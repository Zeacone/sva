//
//  MatrixOperate.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatrixOperate : NSObject
+(double)getHLDoubleWithDoubleMatrix:(double*)data WithRow:(int)row  WithCol:(int)col;
+(float)getHLFloatWithFloatMatrix:(float*)data WithRow:(int)row  WithCol:(int)col;
+(int)getHLIntWithIntMatrix:(int*)data WithRow:(int)row  WithCol:(int)col;

+(void)getDYWithDoubleMatrix:(double*)data WithRow:(int)rowOfData WithCol:(int)colOfData
                     withInt:(int)h withInt:(int)v
           WithResultSavedIn:(double*)newData ;
+(void)getDYWithFloatMatrix:(float*)data WithRow:(int)rowOfData WithCol:(int)colOfData
                    withInt:(int)h withInt:(int)v
          WithResultSavedIn:(float*)newData WithRow:(int)rowOfNewData WithCol:(int)colOfNewData;
+(void)getDYWithIntMatrix:(int*)data WithRow:(int)rowOfData WithCol:(int)colOfData
                  withInt:(int)h withInt:(int)v
        WithResultSavedIn:(int*)newData WithRow:(int)rowOfNewData WithCol:(int)colOfNewData;

+(void)getNWithDoubleMatrix:(double*)data WithRow: (int)rowOfData WithCol: (int)colOfData
WithResultSavedInMatrix:(double*)newData WithRow:(int)rowOfNewData WithCol: (int)colOfNewData;

+(void)getNWithFloatMatrix:(float*)data WithRow: (int)rowOfData WithCol: (int)colOfData
   WithResultSavedInMatrix:(float*)newData WithRow:(int)rowOfNewData WithCol: (int)colOfNewData;

+(void)getNWithIntMatrix:(int*)data WithRow: (int)rowOfData WithCol: (int)colOfData
 WithResultSavedInMatrix:(float*)newData WithRow:(int)rowOfNewData WithCol: (int)colOfNewData;

+(void)getA_TWithDoubleMatrix:(double*)A WithRow:(int)rowOfA WithCol: (int)colOfA
      withResultSavedInMatrix:(double*)A_T WithRow:(int)rowOfA_T WithCol: (int)colOfA_T;
+(void)getA_TWithFloatMatrix:(float*)A WithRow:(int)rowOfA WithCol: (int)colOfA
     withResultSavedInMatrix:(float*)A_T WithRow:(int)rowOfA_T WithCol: (int)colOfA_T;

+(void)getA_TWithIntMatrix:(int*)A WithRow:(int)rowOfA WithCol: (int)colOfA
   withResultSavedInMatrix:(int*)A_T WithRow:(int)rowOfA_T WithCol: (int)colOfA_T;
+(NSMutableArray*)multiplyAbDoubleWithDouble:(double)a
                             withDoubleArray:(NSMutableArray*)A;

+(void)multiplyAbWithFloat:(float)a
           withFloatMatrix:(float*)A
                   withRow: (int)row
                   withCol: (int)col
             savedInMatrix: (float*) aA;
+(NSMutableArray*)multiplyAbFloatWithInt:(int)a
                          withFloatArray:(NSMutableArray*)A;
+(NSMutableArray*)multiplyAbIntWithInt:(int)a
                          withIntArray:(NSMutableArray*)A;

+(NSMutableArray*)multiplyAbFloatWithFloat:(float)a
                              withIntArray:(NSMutableArray*)A;

+(NSMutableArray*)multiplyAbDoubleWithDouble:(double)a
                            withDouble1Array:(NSMutableArray*)Arry;

+(void)multiplyAbWithFloat:(float)a
           withFloat1Array:(float*)Arry WithElemCount: (int)nOfArry
         WithResultSavedIn:(float*) aA;

+(NSMutableArray*)addABIntWIthIntArray:(NSMutableArray*)A
                          withIntArray:(NSMutableArray*)B;

+(void)addABWithFloatMatrix:(float*)A WithRow: (int)rowOfA WithCol: (int)colOfA
            withFloatMatrix:(float*)B WithRow: (int)rowOfB WithCol: (int)colOfB
    WithResultSavedInMatrix:(float*) ABadd WithRow:(int)rowOfABadd WithCol: (int)colOfABadd;


+(NSMutableArray*)addABDoubleWithDoubleArray:(NSMutableArray*)A
                             withDoubleArray:(NSMutableArray*)B;

+(void)addABwithIntMatrix:(int*)A withRowOfA: (int)arow withColOfA: (int)acol
          withFloatMatrix:(float*)B withRowOfB:(int)brow withColOfB:(int)bcol
            savedInMatrix:(float*)ABadd;
+(NSMutableArray*)addABDoublewithIntArray:(NSMutableArray*)A
                          withDoubleArray:(NSMutableArray*)B;

+(NSMutableArray*)addABFloatwithFloatArray:(NSMutableArray*)A
                              withIntArray:(NSMutableArray*)B;

+(NSMutableArray*)addArryDoublewithDoubleArray:(NSMutableArray*)arryA
                               withDoubleArray:(NSMutableArray*)arryB;

+(void)addArrywithFloatArray:(float*)arryA WithElemCount:(int)lenOfArryA
              withFloatArray:(float*)arryB withElemCount:(int)lenOfArryB
        WithResultSaveInArry: (float*) addArry;

+(void)normArryWithDoubleArray:(double*)arryA WithElemCount:(int)nOfArryA SavedIn:(double*)normArry WithElemCount: (int)nOfNormArry;
+(void)normArryWithFloatArray:(float*)arryA WithElemCount:(int)nOfArryA SavedIn:(float*)normArry WithElemCount: (int)nOfNormArry;


+(NSMutableArray*)minusABIntWithIntArray:(NSMutableArray*)A
                            withIntArray:(NSMutableArray*)B;

+(NSMutableArray*)minusABFloatWithFloatArray:(NSMutableArray*)A
                                withIntArray:(NSMutableArray*)B;

+(NSMutableArray*)minusABFloatWithFloatArray:(NSArray*)A
                              withFloatArray:(NSMutableArray*)B;

+(NSMutableArray*)minusABDoubleWithDoubleArray:(NSMutableArray*)A
                               withDoubleArray:(NSMutableArray*)B;

+(NSMutableArray*)minusABFloatWithIntArray:(NSMutableArray*)A
                            withFloatArray:(NSMutableArray*)B;

+(void)minusArrytWithFloatArray:(float*)arryA WithElemCount:(int)lenOfArryA
                 withFloatArray:(float*)arryB WithElemCount:(int)lenOfArryB WIthResultSavedInFloatArray: (float*)arryMinus WithElemCount:(int)lenOfArryMinus;
+(NSMutableArray*)minusArryDoubleWithDoubleArray:(NSMutableArray*)arryA
                                 withDoubleArray:(NSMutableArray*)arryB;


+(void)getDiagIntMatrix: (int*)matrix WithValue:(int)a withDemension:(int)n;
+(void)getDiagfloatMatrix: (float*)matrix withFloatArrayValue:(float*)A withDimension: (int) n;
+(NSMutableArray*)getDiagDoubleWithDoubleArray:(NSMutableArray*)A;

+(NSMutableArray*)getDiagIntWithInt:(int)a
                            withInt:(int)n;

+(NSMutableArray*)getDiagFloatWithFloat:(float)a
                                withInt:(int)n;

+(NSMutableArray*)getDiagDoubleWithDouble:(double)a
                                  withInt:(int)n;

+(NSMutableArray*)copyRowArryIntWithInt:(int)row
                           withIntArray:(NSMutableArray*)A;

+(NSMutableArray*)copyRowArryFloatWithInt:(int)row
                           withFloatArray:(NSMutableArray*)A;

+(NSMutableArray*)copyRowArryDoubleWithInt:(int)row
                           withDoubleArray:(NSMutableArray*)A;

+(NSMutableArray*)copyLineArryIntWithIntArray:(NSMutableArray*)A withInt:(int)line;

+(NSMutableArray*)copyLineArryFloatWithFloatArray:(NSMutableArray*)A withInt:(int)line;

+(NSMutableArray*)copyLineArryDoubleWithDoubleArray:(NSMutableArray*)A withInt:(int)line;





+(void)multiplyABWithFloatMatrix:(float*)matrixA WithRow:(int)row WithCol:(int)col
                  withFloatArray:(float*)arryB WithElemCount:(int)nOfArryB
              ResultSavedInArray: (float*)matrixAB;
+(void)multiplyABWithFloatMatrix:(float*)matrixA WithRow:(int) rowOfA WithCol:(int)colOfA
                   withIntMatrix:(int*)matrixB WithRow:(int) rowOfB WithCol:(int)colOfB
    WithResultSavedInFloatMatrix: (float*) matrixAB WithRow: (int)rowOfMatrixAB WithCol: (int)colOfMatrixAB;
+(NSMutableArray*)multiplyABFloatWithIntMatrix:(NSMutableArray*)matrixA
                               withFloatMatrix:(NSMutableArray*)matrixB;
+(void)multiplyABWithFloatMatrix:(float*)matrixA WithRow:(int) rowOfA WithCol:(int)colOfA
                 withFloatMatrix:(float*)matrixB WithRow:(int) rowOfB WithCol:(int)colOfB
    WithResultSavedInFloatMatrix: (float*) matrixAB WithRow: (int)rowOfMatrixAB WithCol: (int)colOfMatrixAB;
+(void)minusABWithFloatMatrix:(float*)A WithRow:(int)rowOfA WithCol:(int)colOfA
              withFloatMatrix:(float*)B WithRow:(int)rowOfB WithCol:(int)colOfB WithResultSavedInFloatMatrix:(float*)ABminus WithRow:(int)rowOfABminus WithCol:(int)colOfABminus;
+(NSMutableArray*)addABFloatWithFloatMatrix:(NSMutableArray*)A
withFloatMatrix:(NSMutableArray*)B ;

@end
