//
//  Filter.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Filter : NSObject
+(void)medianFilterWithIntArray:(int*)data WithElemCount:(int)lenOfData
                        withInt:(int)windowLength
         WithResultSavedInArray:(float*)medianResult WithElemCount:(int)lenOfMedianResult;
+(void)medianFilterWithFloatArray:(float*)data WithElemCount:(int)lenOfData
                          withInt:(int)windowLength
           WithResultSavedInArray:(float*)medianResult WithElemCount:(int)lenOfMedianResult;
+(void)medianFilterWithDoubleArray:(double*)data WithElemCount:(int)lenOfData
                           withInt:(int)windowLength
            WithResultSavedInArray:(double*)medianResult WithElemCount:(int)lenOfMedianResult;
+(void)meanFilterWithDoubleArray:(double*)data WithElemCount:(int)lenOfData
                         withInt:(int)winLength
               WithResultSavedIn:(double*)meanResult WithElemCount:(int)lenOfMeanResult;
+(void)meanFilterWithFloatArray:(float*)data WithElemCount:(int)lenOfData
                        withInt:(int)winLength
              WithResultSavedIn:(float*)meanResult WithElemCount:(int)lenOfMeanResult;
+ (float)hanningFIRWithFloatArray:(float*)x WithElemCount:(int)lenofX;
+ (double)hanningFIRWithDoubleArray:(double*)x WithElemCount:(int)lenofX;



@end
