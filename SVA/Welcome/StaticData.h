//
//  StaticData.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameplayKit/GameplayKit.h>


@interface StaticData : NSObject
+(NSArray*)arrayDefaultValue;
+(NSArray*)arraydefaultIntValue;
+(void)seAjustfaileCn:(int)AjustfaileCn;
+(void)seAjustSuccFla:(bool)AjustSuccFla;
+(bool)seQ_VectorExis:(bool)Q_VectorExis;
+(void)Q_Vector_tempAddObject: (NSMutableArray*)arry;
+(void)sefi0:(float)fi0;
+(void)sev0:(float)v0;
+(float)sepreV0:(float)preV0;
+(void)seWifiNu:(int)WifiNu;
+(void)sestubCnt0:(int)stubCnt0;
+(void)seMAX_LINE_NUM0:(int)MAX_LINE_NUM0;

+(void)seLineNum0:(int)LineNum0;
+(void)sepaticleNu:(int)paticleNu;
+(void)seInputData:(int)inputData;

+(NSMutableArray*)seLineCn;

+(NSMutableArray*)seQ_Vector_te;


+(void)seUPGATE:(float)UPGATE;

+(void)seLOW_GAT:(float)LOW_GAT;

+(void)seRFPOINTNUM:(int)RFPOINTNUM;

+(void)seAP_NU:(int)AP_NU;
+(void)seDEFAULT_R:(float)DEFAULT_R;
+(void)seUP_INTERV:(int)UP_INTERV;

+(void)seMAX_INDE:(int)MAX_INDE;

+(void)seNO_PEA:(int)NO_PEA;


+(void)seSTEP_LE:(float)STEP_LE;


+(void)seh:(float)h;



+(void)sefirstSeco:(BOOL)firstSeco;
+(void)setwoSeco:(BOOL)twoSeco;

+(void)semoreSeco:(BOOL)moreSeco;

    
+(void)seisFirstHe:(BOOL)isFirstHe;
+(NSMutableArray*)seacc4Lo;
+(void)sestepNu:(int)stepNu;

    
+(void)selast_u:(int)last_u;

+(void)selast_lo:(int)last_lo;

    
+(void)seaMaxLas:(float)aMaxLas;


+(void)seaMinLas:(float)aMinLas;

    


+(NSMutableArray*)seaxFiel;

    
+(NSMutableArray*)seayFiel;

    
    
+(NSMutableArray*)seazFiel;

+(NSMutableArray*)segxFiel;

+(NSMutableArray*)segyFiel;

+(NSMutableArray*)segzFiel;

    
+(NSMutableArray*)semxFiel;

    
+(NSMutableArray*)semyFiel;

+(NSMutableArray*)semzFiel;

+(NSMutableArray*)seax1;
+(NSMutableArray*)seay1;

+(NSMutableArray*)seaz;

+(NSMutableArray*)segx1;
+(NSMutableArray*)segy1;
+(NSMutableArray*)segz1;
+(NSMutableArray*)semx1;
+(NSMutableArray*)semy1;
+(NSMutableArray*)semz1;

+(NSMutableArray*)seaxMean1;

+(NSMutableArray*)seayMean1;

+(NSMutableArray*)seazMean1;
+(NSMutableArray*)segxMean1;
+(NSMutableArray*)segyMean1;

+(NSMutableArray*)segzMean1;

    
+(NSMutableArray*)semxMean1;

+(NSMutableArray*)semyMean1;

+(NSMutableArray*)semzMean1;

+(NSMutableArray*)seX1;

+(NSMutableArray*)seY1;

    
+(NSMutableArray*)seyfielt1;

+(NSMutableArray*)sevelCache1;

+(NSMutableArray*)seheadCache1;

+(NSMutableArray*)seheadPDR1;
+(void)sefla:(int)fla;

+(void)senumberof:(int)numberof;

+(void)secoun:(int)coun;

+(void)seg:(float)g;
+(void)seflagFie:(int)flagFie;

+(void)seT:(int)T;

+(void)seT0:(int)T0;

+(void)seiii_le:(int)iii_le;
+(NSMutableArray*)seRSSI;
+(void)sepreTim:(long)preTim;
+(NSMutableArray*)sepreX;

+(NSMutableArray*)sepreY1;
+(NSMutableArray*)sepreFloorInde;
+(NSMutableArray*)sepreWifiFloorInde;


+ (NSMutableArray*) serollforAttitude;
+ (NSMutableArray*) sepitchforAttitude;
+ (NSMutableArray*) seheadforAttitude;
+(void)rollforAttitude_AddObject:(float)value;
+(void)pitchforAttitude_AddObject:(float)value;
+(void)headforAttitude_AddObject:(float)value;
+(void)rollforAttitude_RemoveObjectAtIndex:(int)index;
+(void)pitchforAttitude_RemoveObjectAtIndex:(int)index;
+(void)headforAttitude_RemoveObjectAtIndex:(int)index;
+(void)MODE_AddObject:(float)value;
+(void)axFielt_AddObject:(float)value;
+(void)ayFielt_AddObject:(float)value;
+(void)azFielt_AddObject:(float)value;
+(void)gxFielt_AddObject:(float)value;
+(void)gyFielt_AddObject:(float)value;
+(void)gzFielt_AddObject:(float)value;
+(void)mxFielt_AddObject:(float)value;
+(void)myFielt_AddObject:(float)value;
+(void)mzFielt_AddObject:(float)value;


+(void)sechangeFloorFla:(int)changeFloorFla;

+ (void)clearField;
+ (void)resetAll;

+ (NSMutableArray*) seMODE;

+ (int) flagforDecide;
+ (BOOL) firstSecond;
+ (BOOL) Q_VectorExist;
+(NSMutableArray*) seMag_Init_Temp;
+(void)Mag_Init_Temp_AddObjectArray:(float*)arry WithElemCount:(int)lenOfArry;
+(int) flagFielt;
+(float) G;
+(NSMutableArray*) acc4Low;
+ (NSMutableArray*) ax1;
+ (NSMutableArray*) ay1;
+ (NSMutableArray*) az1;

+ (NSMutableArray*) gx1;
+ (NSMutableArray*) gy1;
+ (NSMutableArray*) gz1;

+ (NSMutableArray*) mx1;
+ (NSMutableArray*) my1;
+ (NSMutableArray*) mz1;

+(NSMutableArray*)axMean1;
+(NSMutableArray*)ayMean1;
+(NSMutableArray*)azMean1;

+(NSMutableArray*)gxMean1;
+(NSMutableArray*)gyMean1;
+(NSMutableArray*)gzMean1;

+(NSMutableArray*)mxMean1;
+(NSMutableArray*)myMean1;
+(NSMutableArray*)mzMean1;

+(NSMutableArray*)axFielt;
+(float)axFieltAtIndex:(int)index;
+(float)ayFieltAtIndex:(int)index;
+(float)azFieltAtIndex:(int)index;

+(float)gxFieltAtIndex:(int)index;
+(float)gyFieltAtIndex:(int)index;
+(float)gzFieltAtIndex:(int)index;

+(float)mxFieltAtIndex:(int)index;
+(float)myFieltAtIndex:(int)index;
+(float)mzFieltAtIndex:(int)index;

+(NSMutableArray*)ayFielt;
+(NSMutableArray*)azFielt;

+(NSMutableArray*)gxFielt;
+(NSMutableArray*)gyFielt;
+(NSMutableArray*)gzFielt;

+(NSMutableArray*)mxFielt;
+(NSMutableArray*)myFielt;
+(NSMutableArray*)mzFielt;

+(BOOL)isFirstHead;
+(void) seq: (float)value AtIndex: (int)index;
+(void)seq:(float*)newQ WithElemCount:(int)lenOfNewQ;
+(float) qAtIndex:(int)index;
+(int)q_count;

+(void)sedataMemsMean: (float)value AtIndex: (int)index;
+(float) dataMemsMeanAtIndex:(int)index;
+(NSMutableArray*) headforAttitude;
+(NSMutableArray*) pitchforAttitude;
+(NSMutableArray*) rollforAttitude;

+(float) flagofAttitude;
+(void) seflagofAttitude: (float)value;

+(NSMutableArray*) MODE;
+(void) selastMode: (float)value AtIndex: (int)index;
+(float*) lastMode;
+(void) semodePrint:(float) value AtIndex:(int)index;
+(float*) modePrint;
+(void) seP:(float)newvalue AtRow:(int)row AtCol:(int)col;
+(int)P_RowCount;
+(int)P_ColCount;
+(float*) P;
+(int) coun;
+(NSMutableArray*)headCache1;
+(NSMutableArray*)Q_Vector_temp;
+(NSMutableArray*) seQ_Vector;
+(NSMutableArray*) Q_Vector;
+(NSMutableArray*) Mag_Init_Temp;
+(void)seMag_Init_TempAddObject:newObject;
+(NSMutableArray*) seMag_Init;
+(NSMutableArray*) Mag_Init;
+(BOOL)twoSecond;
+(int)Xopt_count;
+(float*)xopt;
+(void) seXopt: (float)value AtIndex: (int)index;
+(float) XoptAIndex:(int)index;
+(void)setXopt:(float*)newArray withElemCount:(int)lenOfNewArray;
+(float)fi;
+(int) NO_PEAK;
+(NSMutableArray*) seyfielt;
+(NSMutableArray*) yfielt;
+(float)UP_GATE;
+(void) seRATE: (int) newRate;
+(int) RATE;
+(int) stepNum;
+(int) UP_INTERVAL;
+(float)H;
+(float)aMinLast;
+(void) seflagEkf: (int)newValue;
+(int)flagEkf;
+(int)last_up;
+(int) MAX_INDEX;
+(BOOL)moreSecond;
+(float)aMaxLast;
+(void)sequiescentCnt: (int) newValue;
+(int)quiescentCnt;
+(void)setPopt:(float*)newMatrix withRow:(int)row WithCol:(int)col;
+(float*) sePopt;
+(float*) Popt;
+(float) PoptAtRow: (int)row AtCol:(int)col;
+(void) selocationWifi:(float)value AtIndex: (int)index;
+(float) locationWifiAtIndex:(int)index;
+(NSMutableArray*) preX;
+(float) preXLastObject;
+(NSMutableArray*)preY;
+(float)preYLastObject;
+(NSMutableArray*)preFloorIndex;
+(NSMutableArray*)preWifiFloorIndex;
+(int)changeFloorFlag;
+(void) senoReadingModeCount: (int)newValue;
+(int) noReadingModeCount;
+(void)selocationWifi_global: (float)value AtIndex:(int)index;
+(float)locationWifi_globalAtIndex:(int)index;
+(int)paticleNum;
+(NSMutableArray*) paticle;
+(NSMutableArray*)sepaticle;
+(float)preV;
+(void)sedeltaFi: (float)newdeltaFi;
+(float)deltaFi;
+(NSMutableArray*)sehistory_fi;
+(NSMutableArray*)history_fi;
+(void) sehistory_fi_index: (int)newValue;
+(int)history_fi_index;
+(void) sewifi_wifi_DistanceCnt: (int)newValue;
+(int)wifi_wifi_DistanceCnt;
+(void) seresetWifiTimeCnt: (int)newValue;
+(int)resetWifiTimeCnt;
+(void) seWifiLocation_eKnn;
+(float)WifiLocation_eKnnAtIndex:(int)index;
+(void) seWifiAvailableNum: (int)newValue;
+(int)WifiAvailableNum;
+(void)seWifiTotalNum: (int)newvalue;
+(int)WifiTotalNum;
+(void)sewifi_pdr_DistanceCnt: (int)newValue;
+(int)wifi_pdr_DistanceCnt;
+(GKRandomDistribution*)ser;
+ (void) seflagforDecide: (int)n;

+(NSArray*)arrayDefaultValue;
+(float)v;
+(void)seInputData;
+ (void)setDefaultValue;
+(void)seInputDataIndex:(int)inputDataIndex;

+(int)floorNum;

+(int)distanceThreshold;

+(int)ErrorLocThreshold;

+(int)ErrorLocCount;

+(void)setErrorLocCount:(int)newValue;
+(void)setStayCount:(int)newValue;
+(int)StayCount;
+(void)setIntialLocX:(int)newValue;
+(int)intialLocX;
+(void)setIntialLocY:(int)newValue;
+(int)intialLocY;
+(int)StayThreshold;
+(void)setErrorAngle:(float)angle;
+(float)errorAngle;

















@end
