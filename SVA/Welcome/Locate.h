//
//  Locate.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Locate : NSObject

@property (nonatomic, assign) NSInteger filterLength;
@property (nonatomic, assign) BOOL traceFlag;

+ (instancetype)sharedLocate;

-(void) locate: (float*) dataMems WithRow:(int)rowOfDataMems WithCol:(int)colOfDataMems WithResultSavedIn:(float*)xylocation;
-(void) wlanMemsFusion: (int) flagEkf : (float*) dataWiFi : (float*) dataWifiBssid : (float) headPDR WithResultSavedIn: (float*) res;

@end
