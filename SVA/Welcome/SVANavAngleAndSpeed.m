//
//  SVANavAngleAndSpeed.cpp
//

#import "SVANavAngleAndSpeed.h"

static int SAMPLING_ACC_NUM = 50;////////////////
static int SMOOTH_WINDOW = 10;///////////////////
static double ACC_VALUE_TH = 0.1; // change to ajust viration detection strenth;
static int ACC_TIME_TH = 15; // 0.3s speed equal 20ms sampling mult 15 steeps;
static double DEFAULT_STEEP = 0.6; // 0.5 origin

@interface SVANavAngleAndSpeed ()
{
    NSMutableArray *accelerationX;
    NSMutableArray *accelerationY;
    NSMutableArray *accelerationZ;
    
    int axis_array_count;

    BOOL axis_array_ready, axis_cacu_flag;
    
    NSMutableArray * last_acc_array;
    int last_top_acc_index; //contain 3 time top_acc

    NSThread *get_axis_thread;
    
    NSThread *get_velocity_thread;

}

@end

@implementation SVANavAngleAndSpeed

+(instancetype)defaultNav {
    static SVANavAngleAndSpeed *nav = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        nav = [[self alloc] init];
    });
    return nav;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        nav_velocity = 0;
        nav_angle = 0;
        self.motionManager = [[CMMotionManager alloc] init];
        
        if (self.motionManager.accelerometerAvailable) {
            self.motionManager.accelerometerUpdateInterval = 1.0 / 50;
            [self.motionManager startAccelerometerUpdates];
        }

        axis_array_count = 0;
        axis_array_ready = false;
        accelerationX = [NSMutableArray arrayWithCapacity:50];
        accelerationY = [NSMutableArray arrayWithCapacity:50];
        accelerationZ = [NSMutableArray arrayWithCapacity:50];
        
        for (NSInteger i = 0; i < 50; i++) {
            [accelerationX addObject:@(0)];
            [accelerationY addObject:@(0)];
            [accelerationZ addObject:@(0)];
        }

        get_axis_thread = [[NSThread alloc] initWithTarget:self selector:@selector(thread_get_acc_axis) object:nil];
        get_velocity_thread = [[NSThread alloc] initWithTarget:self selector:@selector(thread_cacu_velocity) object:nil];
    }
    return self;
}


- (void)threadStart
{
    
    [get_axis_thread start];
    
    [get_velocity_thread start];
    
}

//-(void)threadStop
//{
//    if (get_axis_thread.executing) {
//        [get_axis_thread cancel];
//    }
//    if (get_velocity_thread.executing) {
//        [get_velocity_thread cancel];
//    }
//}

-(double) nav_angle
{
    return nav_angle;
}

-(double)nav_velocity
{
    return nav_velocity;
}

-(void)thread_get_acc_axis
{
    while (true) {
        [self get_acc_axis];
        [NSThread sleepForTimeInterval:0.02];
    }
}

-(void)get_acc_axis
{
    
    @try {
        accelerationX[axis_array_count] = [NSNumber numberWithDouble:self.motionManager.accelerometerData.acceleration.x];
    }
    @catch (NSException *exception) {
        accelerationX[axis_array_count] = axis_array_count == 0? 0 : accelerationX[axis_array_count];
    }
    
    
    @try {
        accelerationY[axis_array_count] = [NSNumber numberWithDouble:self.motionManager.accelerometerData.acceleration.y];
    }
    @catch (NSException *exception) {
        accelerationY[axis_array_count] = axis_array_count == 0? 0 : accelerationY[axis_array_count];
    }
    
    
    @try {
        accelerationZ[axis_array_count] = [NSNumber numberWithDouble:self.motionManager.accelerometerData.acceleration.z];
    }
    @catch (NSException *exception) {
        accelerationZ[axis_array_count] = axis_array_count == 0? 0 : accelerationZ[axis_array_count];
    }
    
    //NSLog(@"++++%f",self.motionManager.accelerometerData.acceleration.x );
    //NSLog(@"%lu", (unsigned long)(accelerationX.count));
    axis_array_count++;
    if (axis_array_count == SAMPLING_ACC_NUM) {
        axis_array_ready = false;
        _acc_result_array = [NSMutableArray arrayWithObjects:accelerationX, accelerationY, accelerationZ, nil];
        axis_array_count = 0;
        axis_cacu_flag = false;
        axis_array_ready = true;
        
        [self get_nav_angle];
        
        for (NSInteger i = 0; i < SAMPLING_ACC_NUM; i++) {
            [accelerationX addObject:@(0)];
            [accelerationY addObject:@(0)];
            [accelerationZ addObject:@(0)];
        }
    }
    //[NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(getValue) userInfo:nil repeats:YES];
}


-(void)thread_cacu_velocity
{
    
    NSMutableArray * acc_cacu_array = [[NSMutableArray alloc]init];
    
    while (true) {
        if (axis_array_ready && !axis_cacu_flag) {
            //NSLog(@"axis_array_calcal_begin");
            acc_cacu_array = [self get_acc_cacu_array];
            //_acc_cacu_array = _acc_result_array;
            [self cacu_velocity: acc_cacu_array];
            axis_cacu_flag = true;
            [NSThread sleepForTimeInterval:0.05];
            //NSLog(@"axis_array_calcal_end");
        }else{
            //NSLog(@"axis_array_not_ready");
            [NSThread sleepForTimeInterval:0.05];
        }
    }
}

-(NSMutableArray *) get_acc_cacu_array
{
    NSMutableArray * tmp_acc_array = [[NSMutableArray alloc]init];
    NSMutableArray * acc_cacu_array = [[NSMutableArray alloc]init];
    NSMutableArray * tmp_accx_array = [[NSMutableArray alloc]init];
    NSMutableArray * tmp_accy_array = [[NSMutableArray alloc]init];
    NSMutableArray * tmp_accz_array = [[NSMutableArray alloc]init];
    
    NSMutableArray * tmp_last_accx_array = [[NSMutableArray alloc]init];
    NSMutableArray * tmp_last_accy_array = [[NSMutableArray alloc]init];
    NSMutableArray * tmp_last_accz_array = [[NSMutableArray alloc]init];
    
    tmp_acc_array = _acc_result_array;
    if ([last_acc_array count] < 3) {
        for (NSInteger i = 0; i < SMOOTH_WINDOW; i++) {
            [tmp_accx_array addObject:@(0)];
            [tmp_accy_array addObject:@(0)];
            [tmp_accz_array addObject:@(0)];
        }
    }else{
        tmp_accx_array = last_acc_array[0];
        tmp_accy_array = last_acc_array[1];
        tmp_accz_array = last_acc_array[2];
    }
    for (int i = 0; i < SAMPLING_ACC_NUM; i ++) {
        [tmp_accx_array addObject:((NSMutableArray*)tmp_acc_array[0])[i]];
        [tmp_accy_array addObject:((NSMutableArray*)tmp_acc_array[1])[i]];
        [tmp_accz_array addObject:((NSMutableArray*)tmp_acc_array[2])[i]];
        if (i >= SAMPLING_ACC_NUM-SMOOTH_WINDOW) {
            [tmp_last_accx_array addObject:((NSMutableArray*)tmp_acc_array[0])[i]];
            [tmp_last_accy_array addObject:((NSMutableArray*)tmp_acc_array[1])[i]];
            [tmp_last_accz_array addObject:((NSMutableArray*)tmp_acc_array[2])[i]];
        }
    }
    acc_cacu_array = [NSMutableArray arrayWithObjects:tmp_accx_array, tmp_accy_array, tmp_accz_array, nil];
    last_acc_array = [NSMutableArray arrayWithObjects:tmp_last_accx_array, tmp_last_accy_array, tmp_last_accz_array, nil];
    return acc_cacu_array;
}

-(void)cacu_velocity: (NSMutableArray *)acc_cacu_array
{
    NSArray *parameter = ((NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"inherit"]);
    if (parameter && parameter.count != 0) {
        NSString *temp_acc_value = parameter[2];
        NSString *temp_default_step = parameter[3];
        NSString *temp_acc_time = parameter[4];
        
        ACC_VALUE_TH = temp_acc_value ? temp_acc_value.doubleValue : ACC_VALUE_TH;
        DEFAULT_STEEP = temp_default_step ? temp_default_step.doubleValue : DEFAULT_STEEP;
        ACC_TIME_TH = temp_acc_time ? temp_acc_time.doubleValue : ACC_TIME_TH;
    }
    
//    NSLog(@"acc_value_th = %@, step = %@, acc_time = %@", @(ACC_VALUE_TH), @(DEFAULT_STEEP), @(ACC_TIME_TH));

    NSMutableArray * smooth_tmp_array = [[NSMutableArray alloc]init];
    NSMutableArray * find_top_acc_array = [[NSMutableArray alloc]init];
    
    double tmp_velocity = 0;
    
    smooth_tmp_array = [self smooth_acc_array: acc_cacu_array];
    //NSLog(@"smooth_tmp_array:%@",smooth_tmp_array);
    
    find_top_acc_array = [self find_top_accs: smooth_tmp_array];
    //NSLog(@"find_top_acc_array:%@",find_top_acc_array);
    tmp_velocity = [self cacu_velocity_by_frequence: find_top_acc_array];
//    NSLog(@"tmp_velocity:%f",tmp_velocity);
    nav_velocity = tmp_velocity;
}

-(NSMutableArray *)smooth_acc_array: (NSMutableArray *)acc_cacu_array
{
    NSMutableArray *smooth_tmp_array = [NSMutableArray array];
    double smooth_accx, smooth_accy, smooth_accz, acc_sum;
    smooth_accx = 0;
    smooth_accy = 0;
    smooth_accz = 0;
    for (int i=(SMOOTH_WINDOW/2-1); i < (SAMPLING_ACC_NUM + SMOOTH_WINDOW/2); i++) {
        for (int j=0; j<SMOOTH_WINDOW; j++) {
            smooth_accx += [((NSMutableArray*)acc_cacu_array[0])[i + SMOOTH_WINDOW/2 - j] doubleValue];
            smooth_accy += [((NSMutableArray*)acc_cacu_array[1])[i + SMOOTH_WINDOW/2 - j] doubleValue];
            smooth_accz += [((NSMutableArray*)acc_cacu_array[2])[i + SMOOTH_WINDOW/2 - j] doubleValue];
        }
        acc_sum = (smooth_accx/SMOOTH_WINDOW)*(smooth_accx/SMOOTH_WINDOW) + (smooth_accy/SMOOTH_WINDOW)*(smooth_accy/SMOOTH_WINDOW) + (smooth_accz/SMOOTH_WINDOW)*(smooth_accz/SMOOTH_WINDOW);
        
        [smooth_tmp_array addObject:@(acc_sum)];
        
        smooth_accx = 0;
        smooth_accy = 0;
        smooth_accz = 0;
    }
    //    NSRange range;
    //    range.location = [smooth_tmp_array count] - SMOOTH_WINDOW;
    //    range.length = SMOOTH_WINDOW;
    //    last_top_acc_index = [smooth_tmp_array subarrayWithRange:range];
    //    smooth_tmp_array = last_top_acc_index;
    //
    
    return smooth_tmp_array;
    
}

-(NSMutableArray *)find_top_accs:(NSMutableArray*)smooth_tmp_array
{
    NSMutableArray *find_top_acc_array = [NSMutableArray array];
    [find_top_acc_array addObject:@(last_top_acc_index)];
    for (int i = 1; i < smooth_tmp_array.count - 1; i ++) {
        if (([smooth_tmp_array[i] doubleValue] - [smooth_tmp_array[i-1] doubleValue] > ACC_VALUE_TH * ACC_VALUE_TH && [smooth_tmp_array[i] doubleValue] - [smooth_tmp_array[i+1] doubleValue] > 0) || ([smooth_tmp_array[i] doubleValue] - [smooth_tmp_array[i-1] doubleValue] > 0 && [smooth_tmp_array[i] doubleValue] - [smooth_tmp_array[i+1] doubleValue] > ACC_VALUE_TH * ACC_VALUE_TH)) {
            [find_top_acc_array addObject:@(i)];
        }
    }
    
    if (find_top_acc_array.count > 0) {
        last_top_acc_index = [[find_top_acc_array lastObject] intValue];
    }
    else if (last_top_acc_index > -(SAMPLING_ACC_NUM * 10)){ // if 10s not move, dont care any more.
        last_top_acc_index -= SAMPLING_ACC_NUM;
    }
    return find_top_acc_array;
}

-(double)cacu_velocity_by_frequence:(NSMutableArray*)find_top_acc_array
{
    double velocity = 0;
    int steeps = 0;
    
    for (int i=0; i < find_top_acc_array.count; i++) {
        for (int j=i; j< find_top_acc_array.count; j++) {
            if (j == i) {
                continue;
            }
            if ([find_top_acc_array[j] integerValue] - [find_top_acc_array[i] integerValue] > ACC_TIME_TH) {
                if (steeps == 0 || steeps > [find_top_acc_array[j] intValue]-[find_top_acc_array[i] intValue]) {
                    steeps = [find_top_acc_array[j] intValue]-[find_top_acc_array[i] intValue];
                }
            }
        }
    }
    if (steeps == 0) {
        return 0;
    }
    return DEFAULT_STEEP * (double)SAMPLING_ACC_NUM/(double)steeps;
}


-(void)get_nav_angle
{
    nav_angle = 0; //get angle
}

@end
