//
//  Gesture.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//



#import "Gesture.h"
#import "StaticData.h"
#import "QandRPY.h"


@implementation Gesture{
    float rad2deg;
}


- (instancetype) init
{
    self = [super init];
    
    if (self)
    {
        rad2deg = 57.29578;
    }
    
    return self;
    
}

- (void) attitudeDecide:(float*)RPY  Of:(float*) dataMems
                WithRow:(int)rowOfDataMems WithCol:(int)colofDataMems
{
    //float RPY[3] = {0.0f, 0.0f, 0.0f};
    float RPYtemp[3] = {0.0f, 0.0f, 0.0f};
    float roll4Attitude[50];
    float pitch4Attitude[50];
    float head4Attitude[50];
    float roll4AttitudeSort[50];
    float pitch4AttitudeSort[50];
    float head4AttitudeSort[50];

    for(int i = 0; i <  50; i++)
    {
        roll4Attitude[i] = 0.0f;
        pitch4Attitude[i] = 0.0f;
        head4Attitude[i] = 0.0f;
    }
    
    for (int i = 0; i < 50; i++)
    {
        [QandRPY getRPY0:RPYtemp
               WithFloat:*(dataMems + i)
               withFloat:*(dataMems + colofDataMems*1 +i)
               withFloat:*(dataMems + colofDataMems*2 +i)
               withFloat:*(dataMems + colofDataMems*6 +i)
               withFloat:*(dataMems + colofDataMems*7 +i)
               withFloat:*(dataMems + colofDataMems*8 +i)];
        roll4Attitude[i] = (RPYtemp[0] *  self->rad2deg);
        pitch4Attitude[i] = (RPYtemp[1] * self->rad2deg);
        head4Attitude[i] = (RPYtemp[2] * self->rad2deg);
    }

    [self sortFloat:roll4Attitude WithElemCount:50
        WithResultSavedInArray:roll4AttitudeSort WithElemCount:50];
    [self sortFloat:pitch4Attitude WithElemCount:50
        WithResultSavedInArray:pitch4AttitudeSort WithElemCount:50];
    [self sortFloat:head4Attitude WithElemCount:50
        WithResultSavedInArray:head4AttitudeSort WithElemCount:50];

    float roll4AttitudeforMean[20];
    float pitch4AttitudeforMean[20];
    float head4AttitudeforMean[20];
    for(int i = 0; i < 20; i++)
    {
        roll4AttitudeforMean[i] = 0.0f;
        pitch4AttitudeforMean[i] = 0.0f;
        head4AttitudeforMean[i] = 0.0f;
    }
    for (int i = 0; i < 10; i++)
    {
        roll4AttitudeforMean[i] = roll4AttitudeSort[i];
        pitch4AttitudeforMean[i] = pitch4AttitudeSort[i];
        head4AttitudeforMean[i] = head4AttitudeSort[i];

        roll4AttitudeforMean[(19 - i)] = roll4AttitudeSort[(50 - i - 1)];
        pitch4AttitudeforMean[(19 - i)] = pitch4AttitudeSort[(50 - i - 1)];
        head4AttitudeforMean[(19 - i)] = head4AttitudeSort[(50 - i - 1)];
    }

    RPY[0] = [self meanFloat:roll4AttitudeforMean WithElemCount:20];
    RPY[1] = [self meanFloat:pitch4AttitudeforMean WithElemCount:20];
    RPY[2] = [self meanFloat:head4AttitudeforMean WithElemCount:20];
    
    return ;
}


-(void) sortFloat: (float*) data WithElemCount:(int)lenOfData
WithResultSavedInArray:(float*)dataNew WithElemCount:(int)lenOfDataNew
{
    if (lenOfData != lenOfDataNew)
    {
        return;
    }
    for(int i = 0; i < lenOfData; i++)
    {
        dataNew[i] = data[i];
    }
    for (int i = 0; i < lenOfData; i++)
    {
        for (int j = 1; j < lenOfData - i; j++)
        {
            if ( dataNew[j] <= dataNew[(j - 1)])
            {
                float temp = dataNew[(j - 1)];
                dataNew[(j - 1)] = dataNew[j];
                dataNew[j] = temp;
            }
        }
    }
    return ;
}


-(float) meanFloat: (float*) data WithElemCount:(int)lenOfData
{
    float dataMean = 0.0;
    for (int i = 0; i < lenOfData; i++)
    {
        dataMean += data[i];
    }
    dataMean /= lenOfData;
    
    return dataMean;
}

-(BOOL) findInt: (NSArray*) dataSource : (NSInteger) target
{
    NSInteger len = [dataSource count];
    for (int i = 1; i < len; i++)
    {
        if ([dataSource[i] integerValue] == target)
        {
            return true;
        }
    }
    return false;
}

-(BOOL) findArrayList: (NSMutableArray*) dataSource : (float) target
{
    NSInteger len = [dataSource count];
    for (NSInteger i = 0; i < len; i++)
    {
        if ([dataSource[i] floatValue] == target)
        {
            return true;
        }
    }
    return false;
}

-(NSMutableArray*) modeChange: (NSMutableArray*) dataMems
{
    int turnFlag = 1;
    int fdata = 50;
    NSRange range50;
    range50.location = 0;
    range50.length = 50;
    NSMutableArray *change = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:range50]];
    float mode = 0.0;
    
    NSRange range9;
    range9.location = 0;
    range9.length = 9;
    NSMutableArray *dataMemsNew = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:range9]]; //[9][50]
    for ( NSInteger i = 9; i < 9; i++ )
    {
        dataMemsNew[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:range50]];
    }
    
    float axNew = 0.0;
    float ayNew = 0.0;
    float azNew = 0.0;
    float gxNew = 0.0;
    float gyNew = 0.0;
    float gzNew = 0.0;
    float mxNew = 0.0;
    float myNew = 0.0;
    float mzNew = 0.0;

    float meanAx = [StaticData dataMemsMeanAtIndex:0 ];
    float meanAy = [StaticData dataMemsMeanAtIndex:1];
    float meanGy = [StaticData dataMemsMeanAtIndex:4];
    float meanGz = [StaticData dataMemsMeanAtIndex:5];
    
    NSArray *ax = [NSArray arrayWithArray:dataMems[0]];
    NSArray *ay = [NSArray arrayWithArray:dataMems[1]];
    NSArray *az = [NSArray arrayWithArray:dataMems[2]];
    NSArray *gx = [NSArray arrayWithArray:dataMems[3]];
    NSArray *gy = [NSArray arrayWithArray:dataMems[4]];
    NSArray *gz = [NSArray arrayWithArray:dataMems[5]];
    NSArray *mx = [NSArray arrayWithArray:dataMems[6]];
    NSArray *my = [NSArray arrayWithArray:dataMems[7]];
    NSArray *mz = [NSArray arrayWithArray:dataMems[8]];
    int mSum = 0;

    for (int i = 0; i < fdata; i++)
    {
        if ( ( fabs( [ [StaticData headforAttitude][1] floatValue ] - [ [StaticData headforAttitude][0] floatValue ]) > 90.0 ) &&
        ( fabs( [ [StaticData headforAttitude][1] floatValue ] - [ [StaticData headforAttitude][0] floatValue ] ) < 360.0 ) &&
        ( fabs( [ [StaticData pitchforAttitude][1] floatValue ] ) < 20.0 ) &&
        ( fabs( [ [StaticData rollforAttitude][1] floatValue ] ) < 20.0 ) )
        {
            if ([ gz[i] floatValue ] - meanGz < -1.5 )
            {
                [StaticData seflagofAttitude:1.1];
            }
            else if ( [ gz[i] floatValue ] - meanGz > 1.2 )
           {
               [StaticData seflagofAttitude:1.2 ];
           }
        }
        else if (( [ ax[i] floatValue ] - meanAx > 5.0 ) && ([ StaticData.rollforAttitude[1] floatValue ] > 40.0))
        {
            [StaticData seflagofAttitude: 2.0];
            if ([ gy[i] floatValue ] - meanGy < -2.0)
            {
                [StaticData seflagofAttitude:2.1];
            }
            else if ( [ gy[i] floatValue ] - meanGy > 2.0)
            {
                [StaticData seflagofAttitude:2.2];
            }
        }
        else if ( ( [ ay[i] floatValue ] - meanAy > 3.0 ) && ( [ StaticData.pitchforAttitude[1] floatValue ] <= -25.0 ) )
        {
            [StaticData seflagofAttitude:3.0];
            if ( [ gz[i] floatValue ] - meanGz < -1.0 )
            {
                [StaticData seflagofAttitude:3.1];
            }
            else if ( [ gz[i] floatValue ] - meanGz > 1.2)
            {
                [StaticData seflagofAttitude:3.2];
            }
        }
        else if ( [ StaticData.pitchforAttitude[1] floatValue ] > 20.0 )
        {
            [StaticData seflagofAttitude:4.0];
            if ( [ StaticData.pitchforAttitude[1] floatValue ] - [ StaticData.pitchforAttitude[0] floatValue ] > 20.0 )
            {
                [StaticData seflagofAttitude:4.1];
            }
            else if ([ gz[i] floatValue ] - meanGz < -1.0 )
            {
                [StaticData seflagofAttitude:4.2];
            }
        }
        else
        {
            [StaticData seflagofAttitude:0.0];
        }
        
        
        if ( ( [StaticData flagofAttitude] == 1.1 ) || ( [StaticData flagofAttitude] == 1.2 ) )
        {
            turnFlag++;
            mode = [StaticData flagofAttitude];
        }
        else if ( ( [StaticData flagofAttitude] == 2.1) || ([ StaticData flagofAttitude] == 2.2 )||
                 ( [StaticData flagofAttitude ] == 3.1 ) || ( [StaticData flagofAttitude ] == 3.2 ) ||
                 ( [StaticData flagofAttitude ] == 4.1 ) || ( [StaticData flagofAttitude ] == 4.2 ) ||
                 ( [StaticData flagofAttitude ] == 0.0 ) )
        {
            mode = [StaticData flagofAttitude];
        }
        else if ( ( [StaticData flagofAttitude ]== 2.0 ) ||
                 ( [StaticData flagofAttitude ] == 3.0 ) ||
                 ( [StaticData flagofAttitude ] == 4.0 ))
        {
            mode = [StaticData flagofAttitude] ;
        }
        else
        {
            mode = [[ StaticData MODE][fdata + i - 1] floatValue ];
        }
        
        if (turnFlag <= 2)
        {
            [ [StaticData MODE] addObject: [ NSNumber numberWithFloat:mode ] ];
        }
        else
        {
            [ [StaticData MODE] addObject: @0.0f ];
        }
        
        if ([[StaticData MODE][fdata + i] floatValue ] == 0.0 )
        {
            axNew = [ax[i] floatValue];
            ayNew = [ay[i] floatValue];
            azNew = [az[i] floatValue];
            
            gxNew = [gx[i] floatValue];
            gyNew = [gy[i] floatValue];
            gzNew = [gz[i] floatValue];
            
            mxNew = [mx[i] floatValue];
            myNew = [my[i] floatValue];
            mzNew = [mz[i] floatValue];
            
            if ( [ [StaticData MODE][fdata + i] floatValue] == [ [StaticData MODE][fdata + i - 1] floatValue] )
            {
                change[i] = [NSNumber numberWithInt:1];
            }
            else
            {
                change[i] = [NSNumber numberWithInt:0];
            }
        }
        else if (( [[StaticData MODE][fdata + i] floatValue] == 1.1) || ([[StaticData MODE][fdata + i] floatValue] == 1.2F))
        {
            if (( [[StaticData MODE][fdata + i] floatValue]== [[StaticData MODE][fdata + i - 1] floatValue] ) && (i != 0))
            {
                change[i] = [NSNumber numberWithInt:1];
            }
            else
            {
                change[i] = [NSNumber numberWithInt:0];
            }
            axNew = [ax[i] floatValue];
            ayNew = [ay[i] floatValue];
            azNew = [az[i] floatValue];
                                                  
            gxNew = [gx[i] floatValue];
            gyNew = [gy[i] floatValue];
            gzNew = [gz[i] floatValue];
                                                  
            mxNew = [mx[i] floatValue];
            myNew = [my[i] floatValue];
            mzNew = [mz[i] floatValue];
                                                  
        }
        else if (([[StaticData MODE][fdata + i] floatValue] == 2.1) || ([[StaticData MODE][fdata + i] floatValue] == 2.2) ||
                 ([[StaticData MODE][fdata + i] floatValue] == 2.0))
        {
            if (![StaticData.MODE[fdata + i] floatValue] == [StaticData.MODE[fdata + i - 1] floatValue])
            {
                change[i] = [NSNumber numberWithInt:1];
            }
            else
            {
                change[i] = [NSNumber numberWithInt:0];
            }
            axNew = -[az[i] floatValue];
            ayNew = [ay[i] floatValue];
            azNew = [ax[i] floatValue];
            
            gxNew = -[gz[i] floatValue];
            gyNew = [gy[i] floatValue];
            gzNew = [gx[i] floatValue];
            
            mxNew = -[mz[i] floatValue];
            myNew = [my[i] floatValue];
            mzNew = [mx[i] floatValue];
        }
        else if (([[StaticData MODE][fdata + i] floatValue] == 3.1) || ([[StaticData MODE][fdata + i] floatValue] == 3.2) ||
                 ([[StaticData MODE][fdata + i] floatValue ] == 3.0))
        {
            if (![[StaticData MODE][fdata + i] floatValue]== [[StaticData MODE][fdata + i - 1] floatValue])
            {
                change[i] = [NSNumber numberWithInt:1];
            }
            else
            {
                change[i] = [NSNumber numberWithInt:0];
            }
            axNew = -[az[i] floatValue];
            ayNew = -[ax[i] floatValue];
            azNew = [ay[i] floatValue];
            
            gxNew = -[gz[i] floatValue];
            gyNew = -[gx[i] floatValue];
            gzNew = [gy[i] floatValue];
            
            mxNew = -[mz[i] floatValue];
            myNew = -[mx[i] floatValue];
            mzNew = [my[i] floatValue];
        }
        else if (([[StaticData MODE][fdata + i] floatValue] == 4.1) || ([[StaticData MODE][fdata + i] floatValue] == 4.2) ||
                 ([[StaticData MODE][fdata + i] floatValue] == 4.0))
        {
            if (![[StaticData MODE][fdata + i] floatValue] == [[StaticData MODE][fdata + i - 1] floatValue])
            {
                change[i] = [NSNumber numberWithInt:1];
            }
            else
            {
                change[i] = [NSNumber numberWithInt:0];
            }
            axNew = -[ax[i] floatValue];
            ayNew = -[az[i] floatValue];
            azNew = -[ay[i] floatValue];
            
            gxNew = -[gx[i] floatValue];
            gyNew = -[gz[i] floatValue];
            gzNew = -[gy[i] floatValue];
            
            mxNew = -[mx[i] floatValue];
            myNew = -[mz[i] floatValue];
            mzNew = -[my[i] floatValue];
        }
        dataMemsNew[0][i] = [NSNumber numberWithFloat: axNew];
        dataMemsNew[1][i] = [NSNumber numberWithFloat: ayNew];
        dataMemsNew[2][i] = [NSNumber numberWithFloat: azNew];
        dataMemsNew[3][i] = [NSNumber numberWithFloat: gxNew];
        dataMemsNew[4][i] = [NSNumber numberWithFloat: gyNew];
        dataMemsNew[5][i] = [NSNumber numberWithFloat: gzNew];
        dataMemsNew[6][i] = [NSNumber numberWithFloat: mxNew];
        dataMemsNew[7][i] = [NSNumber numberWithFloat: myNew];
        dataMemsNew[8][i] = [NSNumber numberWithFloat: mzNew];
                   
        if ((fabs([StaticData.pitchforAttitude[1] floatValue]) < 30.0) &&
            (fabs([StaticData.rollforAttitude[1] floatValue]) < 30.0) &&
            (fabs([ay[i] floatValue] - meanAy) < 5.0) && (fabs([ax[i] floatValue] - meanAx) < 5.0))
        {
            mSum++;
        }
    }
                   
    for (int i = 0; i < fdata; i++)
    {
        [StaticData.MODE removeObjectAtIndex: 0];
    }
    [StaticData selastMode:[StaticData lastMode][1] AtIndex:(0)];
                   
    [StaticData semodePrint:[StaticData modePrint][1] AtIndex:0];
    if (mSum > 25)
        [StaticData semodePrint:0.0 AtIndex:1];
    else
    {
        [StaticData semodePrint:7.0 AtIndex:1];
    }
    
    if ([self findArrayList: [StaticData MODE] : 1.1 ]|| [self findArrayList: [StaticData MODE] : 1.2])
    {
        if (( [self findArrayList: [StaticData MODE] : 1.1]) && ([StaticData lastMode][0] != 1.1) &&
            ([StaticData lastMode][0] != 1.2) && ([StaticData lastMode][0] != 0.0) )
        {
            [StaticData selastMode:5.0  AtIndex:1];
        }
        else
        {
            if ( [StaticData lastMode][0]  != 1.1)
            {
                [StaticData selastMode:1.1 AtIndex:1];
            }
            
            if ([StaticData lastMode][0] != 1.2)
            {
                [StaticData selastMode:1.2 AtIndex:1];
            }
        }
    }
    else if ([self findInt: change : 1])
    {
        if ([self findArrayList: StaticData.MODE : 4.2F])
        {
            [StaticData selastMode:4.2 AtIndex:1];
        }
        else
        {
            [StaticData selastMode:5.0 AtIndex:1];
        }
    }
    else
    {
        [StaticData selastMode:mode AtIndex:1];
    }
    
    return dataMemsNew;
}
    

@end
