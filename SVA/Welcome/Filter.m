//
//  Filter.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import "Filter.h"
#import "StaticData.h"

@implementation Filter

+(void)medianFilterWithIntArray:(int*)data WithElemCount:(int)lenOfData
                           withInt:(int)windowLength
            WithResultSavedInArray:(float*)medianResult WithElemCount:(int)lenOfMedianResult
{
    int med = (int)(floor(windowLength / 2) + 1.0);
    int tempData[windowLength];
    for(int i = 0; i < windowLength; i++)
    {
        tempData[i] = 0;
    }
    int temp = 0;
    int index = 0;

    for (int i = 0; i < lenOfData; i++)
    {
        if ((i < med - 1) || (i > lenOfData - med))
        {
            medianResult[i] = data[i];
        }
        else
        {
            for (int j = 0; j < windowLength; j++)
            {
                tempData[j] = data[(i - med + 1 + j)];
            }

            for (int j = 0; j < med; j++)
            {
                int min = tempData[j];
                index = j;
                for (int k = j; k < windowLength; k++)
                {
                    if (tempData[k] < min)
                    {
                        min = tempData[k];
                        index = k;
                    }
                }
                temp = tempData[j];
                tempData[j] = tempData[index];
                tempData[index] = temp;
            }

            if (windowLength % 2 != 0)
            {
                medianResult[i] = tempData[(med - 1)];
            }
            else
            {
                medianResult[i] = ((tempData[(med - 2)] + tempData[(med - 1)]) / 2);
            }
        }
    }
    return ;
}


+(void)medianFilterWithFloatArray:(float*)data WithElemCount:(int)lenOfData
                          withInt:(int)windowLength
           WithResultSavedInArray:(float*)medianResult WithElemCount:(int)lenOfMedianResult
{
    if(lenOfData != lenOfMedianResult)
    {
        return;
    }
    int med = (int)(floor(windowLength / 2) + 1.0);
    float tempData[windowLength];
    for(int i = 0; i < windowLength; i++)
    {
        tempData[i] = 0.0f;
    }
    float temp = 0.0f;
    for (int i = 0; i < lenOfData; i++)
    {
        if ((i < med - 1) || (i > lenOfData - med))
        {
            medianResult[i] = data[i];
        }
        else
        {
            for (int j = 0; j < windowLength; j++)
            {
                tempData[j] = data[(i - med + 1 + j)];
            }

            for (int j = 0; j < med; j++)
            {
                for (int k = j; k < windowLength; k++)
                {
                    if (tempData[j] >= tempData[k])
                    {
                        temp = tempData[j];
                        tempData[j] = tempData[k];
                        tempData[k] = temp;
                    }
                }
            }
            
            if (windowLength % 2 != 0)
            {
                medianResult[i] = tempData[(med - 1)];
            }
            else
            {
                medianResult[i] = ((tempData[(med - 2)] + tempData[(med - 1)]) / 2.0f);
            }
        }
    }
    return ;
}

+(void)medianFilterWithDoubleArray:(double*)data WithElemCount:(int)lenOfData
                          withInt:(int)windowLength
           WithResultSavedInArray:(double*)medianResult WithElemCount:(int)lenOfMedianResult
{
    if(lenOfData != lenOfMedianResult)
    {
        return;
    }
    int med = (int)(floor(windowLength / 2) + 1.0);
    double tempData[windowLength];
    for(int i = 0; i < windowLength; i++)
    {
        tempData[i] = 0.0f;
    }
    double temp = 0.0;
    int index = 0;

    for (int i = 0; i < lenOfData; i++)
    {
        if ((i < med - 1) || (i > lenOfData - med))
        {
            medianResult[i] = data[i];
        }
        else
        {
            for (int j = 0; j < windowLength; j++)
            {
                tempData[j] = data[(i - med + 1 + j)];
            }

            for (int j = 0; j < med; j++)
            {
                double min = tempData[j];
                index = j;
                for (int k = j; k < windowLength; k++)
                {
                    if (tempData[k] < min)
                    {
                        min = tempData[k];
                        index = k;
                    }
                }
                temp = tempData[j];
                tempData[j] = tempData[index];
                tempData[index] = temp;
            }

            if (windowLength % 2 != 0)
            {
                medianResult[i] = tempData[(med - 1)];
            }
            else
            {
                medianResult[i] = ((tempData[(med - 2)] + tempData[(med - 1)]) / 2.0);
            }
        }
    }
    return ;
}


+(void)meanFilterWithFloatArray:(float*)data WithElemCount:(int)lenOfData
                                   withInt:(int)winLength
                         WithResultSavedIn:(float*)meanResult WithElemCount:(int)lenOfMeanResult
{
    if(lenOfData != lenOfMeanResult)
    {
        return;
    }
    int med = (int)floor(winLength / 2) + 1;
    float sum = 0.0f;
    //float[] meanResult = new float[data.length];
    for (int i = 0; i <= med - 2; i++)
    {
        meanResult[i] = data[i];
    }

    for (int i = lenOfData - med + 1; i < lenOfData; i++)
    {
        meanResult[i] = data[i];
    }

    for (int i = 0; i < lenOfData; i++)
    {
        if ((i > med - 2) && (i <= lenOfData - med)) {
            for (int j = 0; j < winLength; j++)
            {
                sum += data[(i - med + 1 + j)];
            }
            meanResult[i] = (sum / winLength);

            sum = 0.0f;
        }

    }

    return ;
}

+(void)meanFilterWithDoubleArray:(double*)data WithElemCount:(int)lenOfData
                        withInt:(int)winLength
              WithResultSavedIn:(double*)meanResult WithElemCount:(int)lenOfMeanResult
{
    if(lenOfData != lenOfMeanResult)
    {
        return;
    }
    int med = (int)floor(winLength / 2) + 1;
    double sum = 0.0;
    //double meanResult[] = new double[data.length];
    for (int i = 0; i <= med - 2; i++)
    {
        meanResult[i] = data[i];
    }

    for (int i = lenOfData - med - 1; i < lenOfData; i++)
    {
        meanResult[i] = data[i];
    }

    for (int i = 0; i < lenOfData; i++)
    {
        if ((i > med - 2) && (i < lenOfData - med - 1))
        {
            for (int j = 0; j < winLength; j++)
            {
                sum += data[(i - med + 1 + j)];
            }
            meanResult[i] = (sum / winLength);
            data[i] = meanResult[i];
            sum = 0.0;
        }
    }

    return ;
}


+ (float)hanningFIRWithFloatArray:(float*)x WithElemCount:(int)lenofX
{
    double bn[] = { -0.0004092840516965646, -0.001781815004437, -0.004202347799477,
        -0.007487304165184, -0.009759128954685999, -0.007602823582934,
        0.002775820847495, 0.023771392907347, 0.054748653738444,
        0.091377432595837, 0.126401078911696, 0.151701566774857,
        0.160933515565476, 0.151701566774857, 0.126401078911696,
        0.091377432595837, 0.054748653738444, 0.023771392907347,
        0.002775820847495, -0.007602823582934, -0.009759128954685999,
        -0.007487304165184, -0.004202347799477, -0.001781815004437,
        -0.0004092840516965646 };
    int N = sizeof(bn)/sizeof(bn[0]);
 
    float sum = 0.0;
 
    for (int j = 0; j < N; j++) {
        sum += (float)(x[(N - j - 1)] * bn[j]);
    }
    float y = sum;
    sum = 0.0f;
   
    return y;

}


+ (double)hanningFIRWithDoubleArray:(double*)x WithElemCount:(int)lenofX
{

    double bn[] = { -0.0004092840516965646, -0.001781815004437, -0.004202347799477,
        -0.007487304165184, -0.009759128954685999, -0.007602823582934,
        0.002775820847495, 0.023771392907347, 0.054748653738444,
        0.091377432595837, 0.126401078911696, 0.151701566774857,
        0.160933515565476, 0.151701566774857, 0.126401078911696,
        0.091377432595837, 0.054748653738444, 0.023771392907347,
        0.002775820847495, -0.007602823582934, -0.009759128954685999,
        -0.007487304165184, -0.004202347799477, -0.001781815004437,
        -0.0004092840516965646 };
    int N = sizeof(bn)/sizeof(bn[0]);
    double sum = 0.0;
    for (int j = 0; j < N; j++) {
        sum += x[(N - j - 1)] * bn[j];
    }
    double y = sum;
    sum = 0.0;
    return y;
}








@end
