 //
//  Statistics.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import "Statistics.h"

@implementation Statistics

+(float)meanIntArray:(NSMutableArray*)data
{
    float sum = 0.0;
    for (int i=0; i<data.count; i++) {
        sum +=[data[i] integerValue];
        
    }

    float Mean = sum/data.count;
    return  Mean;

}
+(float)meanFloatArray:(float*)data WithElemCount:(int)lenOfData
{
    float sum = 0.0;
    for (int i=0; i < lenOfData; i++) {
        sum += data[i];
        
    }
    
    float Mean = sum/lenOfData;
    return  Mean;
    
}
+(float)meanDoubleArray:(NSMutableArray*)data
{
    float sum = 0.0;
    for (int i=0; i<data.count; i++) {
        sum +=[data[i] doubleValue] ;
        
    }
    
    float Mean = sum/data.count;
    return  Mean;
    
}
+(float)varIntArray:(NSMutableArray*)data
{
    float Variance = 0.0;
    float dataMean =[Statistics meanIntArray:data];
   
    
    for (int i=0; i<data.count; i++) {
        Variance = (float)(Variance + pow([data[i] integerValue]-dataMean,2.0));
        
    }
    Variance /=(data.count-1);
    return Variance;
    
}
+(float)varFloatArray:(float*)data WithElemCount:(int)lenOfData
{
    float Variance = 0.0;
    float dataMean = [Statistics meanFloatArray:data WithElemCount:lenOfData ];
    
    for (int i=0; i< lenOfData; i++) {
        Variance = (float)(Variance + pow(data[i]-dataMean,2.0));
        
    }
    Variance /=(lenOfData-1);
    return Variance;
    
}
+(float)varDoubleArray:(NSMutableArray*)data
{
    double Variance = 0.0;
    double dataMean = [Statistics meanDoubleArray:data];
    
    for (int i=0; i<data.count; i++) {
        Variance += (Variance + pow([data[i] integerValue]-dataMean,2.0));
        
    }
    Variance /=(data.count-1);
    return Variance;
    
}
+(float)stdIntArray:(NSMutableArray*)data
{
    float stdData = 0.0;
    stdData = (float)sqrt([Statistics varIntArray:data]);
    return stdData;
    
}
+(float)stdFloatArray:(float*)data WithElemCount:(int)lenOfData
{
    float stdData = 0.0;
    stdData = (float)sqrt([Statistics varFloatArray:data WithElemCount:lenOfData]);
    //stdData = (float)sqrt([Statistics varFloatArray:data]);
    return stdData;
    
}
+(float)stdDoubleArray:(NSMutableArray *)data
{
    double stdData = 0.0;
    stdData = sqrt([Statistics varDoubleArray:data]);
    return stdData;
    
}
+(double)sumOfSquareIntArray:(NSMutableArray*)data
{
    long sumSquare = 0;
    for (int i=0; i<[data count]; i++) {
        sumSquare = (double)(sumSquare+pow([data[i] intValue], 2.0));
    }

    return sumSquare;

}
+(double)sumOfSquareLongArray:(NSMutableArray*)data
{
    long sumSquare = 0;
    for (int i=0; i<[data count]; i++) {
        sumSquare = (double)(sumSquare+pow([data[i] longValue], 2.0));
    }
    
    return sumSquare;
    
}
+(float)sumOfSquareFloatArray:(float*)data WithElemCount:(int)n
{
    float sumSquare = 0.0f;
    for (int i = 0; i < n; i++) {
        sumSquare = (float)(sumSquare + pow(*(data+i), 2.0));
    }
    return sumSquare;
}

+(double)sumOfSquareDoubleArray:(double*)data WithElemCount:(int)n
{
    double sumSquare = 0.0;
    for (int i=0; i < n; i++) {
        sumSquare += pow(*(data +i), 2.0);
    }
    return sumSquare;
    
}
@end
