//
//  Statistics.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Statistics : NSObject

+(float)meanIntArray:(NSMutableArray*)data;

+(float)meanFloatArray:(float*)data WithElemCount:(int)lenOfData;
+(float)meanDoubleArray:(NSMutableArray*)data;
+(float)varIntArray:(NSMutableArray*)data;
+(float)varFloatArray:(float*)data WithElemCount:(int)lenOfData;
+(float)varDoubleArray:(NSMutableArray*)data;
+(float)stdIntArray:(NSMutableArray*)data;
+(float)stdFloatArray:(float*)data WithElemCount:(int)lenOfData;
+(float)stdDoubleArray:(NSMutableArray*)data;
+(double)sumOfSquareIntArray:(NSMutableArray*)data;
+(double)sumOfSquareLongArray:(NSMutableArray*)data;
+(float)sumOfSquareFloatArray:(float*)data WithElemCount:(int)n;
+(double)sumOfSquareDoubleArray:(double*)data WithElemCount:(int)n;










@end
