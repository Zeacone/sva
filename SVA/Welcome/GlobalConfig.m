//
//  GlobalConfig.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import "GlobalConfig.h"

@implementation GlobalConfig

static NSString * server_ip=@"192.168.1.100";
static bool autoPush = true;
static bool bAnimation = true;
static NSString * port = @"8082";
static NSString * identification = @"IP";
static NSString * insapi = @"API";
static NSString * threshold = @"0.2";//静止门限
static NSString * addsecound = @"0.4";//添加速度
static NSString * readius = @"3.0";//抛洒粒子半径
static NSString * weights = @"32";//权重
static NSString * maxDeviate = @"8";//最大偏离
static NSString * moreMaxDeviate = @"5";//超过最大偏离
static NSString * direction = @"0";//矫正地图方向
static double proportion = 26.5;//地图比例
static double xSport = 0.0;//矫正地图方向
static double ySport = 0.0;
+(double)sexSp:(double)xSp
{
    xSport = xSp;
    return xSport;
}
+(double)xSport
{
    return xSport;
}

+(double)seySp:(double)ySp
{
    ySport = ySp;
     return xSport;
}

+(double)ySport
{
    return ySport;
}

+(NSString*)seThre
{
    if(!threshold)
    {
        threshold = [[NSString alloc]init];
    }
    return threshold;
}

+(NSString*)threshold
{
    return threshold;
}


+(NSString*)seServer
{
    if(!server_ip)
    {
        server_ip = [[NSString alloc]init];
    }
    return server_ip;
}

+(NSString*)seIns
{
    if(!insapi)
    {
        insapi = [[NSString alloc]init];
    }
    return insapi;
}
+(NSString*)seRead
{
    if(!readius)
    {
        readius = [[NSString alloc]init];
    }
    return readius;
}
+(NSString*)seAddsec
{
    if(!addsecound)
    {
        addsecound = [[NSString alloc]init];
    }
    return addsecound;
}

+(double)sePropor:(double)propor
{
    proportion = propor;
     return proportion;
}

+(double)proportion
{
    return proportion;
}

+(NSString*)weights
{
    return weights;
}
+(NSString*)seMaxDev
{
    if(!maxDeviate)
    {
        maxDeviate = [[NSString alloc]init];
    }
    return maxDeviate;
}

+(NSString*)seMoreMax
{
    if(!moreMaxDeviate)
    {
        moreMaxDeviate = [[NSString alloc]init];
    }
    return moreMaxDeviate;
}

+(NSString*)moreMaxDeviate
{
    if(!moreMaxDeviate)
    {
        moreMaxDeviate = [[NSString alloc]init];
    }
    return moreMaxDeviate;
}

+(NSString*)seDirec
{
    if(!direction)
    {
        direction = [[NSString alloc]init];
    }
    return direction;
}

+(NSString*)seIdentifi
{
    if(!identification)
    {
        identification = [[NSString alloc]init];
    }
    return identification;
}

+(NSString*)sePor
{
    if(!port)
    {
        port = [[NSString alloc]init];
    }
    return port;
}

+(BOOL)seAutop:(BOOL)autop
{
    autoPush =autop;
     return autoPush;
}

+(BOOL)seAnimation:(BOOL)animation
{
    bAnimation =animation;
     return bAnimation;
}


@end
