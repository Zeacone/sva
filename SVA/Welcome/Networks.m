//
//  Networks.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//


#import "Networks.h"

@implementation Networks
{
    NSArray  *hDatabase;
    NSArray *fDatabase;
    NSArray *cDatabase ;
    NSArray *IN;
    NSArray *OUT ;
    NSArray *MAP_IN;
    NSArray *MAP_OUT;
    NSArray *W1;
    NSArray *B1;
    NSArray *W2;
    float B2 ;
}

- (instancetype) init
{
    self = [super init];
    
    if (self)
    {
        hDatabase = @[ @1.5f, @1.52f, @1.54f, @1.56f, @1.58f, @1.6f, @1.62f, @1.64f, @1.66f, @1.68f, @1.7f, @1.72f, @1.74f, @1.76f,@1.78f, @1.8f, @1.82f, @1.84f];
        fDatabase =@[ @1.2f, @1.3f, @1.4f, @1.5f, @1.6f, @1.7f, @1.8f, @1.9f, @2.0f, @2.1f, @2.2f, @2.3f, @2.4f];
        cDatabase = @[ @[ @0.5048348f, @0.5035394f, @0.499785f, @0.4893157f, @0.4751124f, @0.4690559f, @0.4718823f, @0.4814222f, @0.4929605f, @0.4975387f, @0.5004575f, @0.5251008f, @0.5668203f ], @[ @0.4856326f, @0.4815763f, @0.473089f, @0.4565314f, @0.4433914f, @0.4434536f, @0.451766f, @0.4642662f, @0.4733757f, @0.474008f, @0.477086f, @0.50525f, @0.5453317f ], @[ @0.481094f, @0.4740735f, @0.4597194f, @0.4370796f, @0.4246519f, @0.429295f, @0.4404365f, @0.4498749f, @0.452627f, @0.4487134f, @0.4516882f, @0.4814356f, @0.5165204f ], @[ @0.4894649f, @0.4834538f, @0.4670474f, @0.4441595f, @0.4364776f, @0.4462617f, @0.4573661f, @0.460812f, @0.4570691f, @0.4484814f, @0.4501308f, @0.4778056f, @0.5030537f ], @[ @0.4926285f, @0.4886171f, @0.4728099f, @0.4556465f, @0.4566843f, @0.4711664f, @0.4822899f, @0.4837874f, @0.4780569f, @0.4677495f, @0.4691171f, @0.4924068f, @0.5049526f ], @[ @0.4900621f, @0.4864356f, @0.472359f, @0.4632666f, @0.4702492f, @0.4835364f, @0.493139f, @0.4941509f, @0.487511f, @0.4762434f, @0.4781484f, @0.4979383f, @0.503446f ], @[ @0.4872516f, @0.4819093f, @0.4711025f, @0.4697594f, @0.4770198f, @0.484717f, @0.4901935f, @0.4880746f, @0.477032f, @0.4624177f, @0.4656133f, @0.4862729f, @0.4938118f ], @[ @0.4897402f, @0.4816802f, @0.473286f, @0.4746766f, @0.4777442f, @0.477885f, @0.4760368f, @0.4683397f, @0.4533244f, @0.4402483f, @0.4516855f, @0.4777454f, @0.4889986f ], @[ @0.5045216f, @0.4934157f, @0.482753f, @0.4788145f, @0.474267f, @0.4659499f, @0.456938f, @0.447955f, @0.4372573f, @0.4334602f, @0.4554245f, @0.4842219f, @0.4957401f ], @[ @0.5356028f, @0.518675f, @0.5002724f, @0.4859952f, @0.472924f, @0.4610204f, @0.453207f, @0.450616f, @0.4488883f, @0.4546176f, @0.4831731f, @0.5103474f, @0.5194637f ], @[ @0.5629749f, @0.5364299f, @0.5106124f, @0.4911902f, @0.4774991f, @0.4702624f, @0.4695532f, @0.4744425f, @0.480728f, @0.4944587f, @0.5265751f, @0.5502883f, @0.5563318f ], @[ @0.5601f, @0.5270423f, @0.502886f, @0.4885899f, @0.4808269f, @0.4786121f, @0.4821536f, @0.4915252f, @0.5043775f, @0.5256795f, @0.5594384f, @0.5788384f, @0.5811504f ], @[ @0.5363298f, @0.5073f, @0.4916146f, @0.484276f, @0.4809486f, @0.4805133f, @0.4839114f, @0.4935184f, @0.510502f, @0.5380877f, @0.570541f, @0.5833862f, @0.5797221f ], @[ @0.5146069f, @0.493822f, @0.4849632f, @0.4813905f, @0.479714f, @0.4791315f, @0.4804476f, @0.486808f, @0.5035908f, @0.5341448f, @0.5621207f, @0.567466f, @0.5593154f ], @[ @0.5012404f, @0.4867399f, @0.481326f, @0.4788818f, @0.4769172f, @0.4746885f, @0.4725643f, @0.4734735f, @0.4855668f, @0.5147271f, @0.5388352f, @0.5427077f, @0.5376515f ], @[ @0.4928536f, @0.4821295f, @0.4776493f, @0.4742262f, @0.4698066f, @0.4635949f, @0.456091f, @0.45128f, @0.4600439f, @0.4892378f, @0.514531f, @0.5234364f, @0.5246928f ], @[ @0.4856769f, @0.4762622f, @0.4701299f, @0.4630269f, @0.4535376f, @0.4419886f, @0.4308876f, @0.4264263f, @0.439781f, @0.4725164f, @0.4992488f, @0.51163f, @0.517293f ], @[ @0.4755903f, @0.4642788f, @0.4535966f, @0.4412724f, @0.4280902f, @0.4162714f, @0.4088409f, @0.4112624f, @0.4321891f, @0.4663225f, @0.491567f, @0.504052f, @0.5112103f ] ];
        IN = @[ @[ @1.2055f, @2.3963f ], @[ @1.5f, @1.81f ] ];
        OUT = @[ @0.4325f, @0.5822f] ;
        MAP_IN = @[@(-0.95f), @0.95f];
        MAP_OUT = @[ @0.05f, @0.95f] ;
        W1 = @[ @[ @(-4.958726f), @10.5908f ], @[ @(-0.277062f), @(-8.307787f) ], @[ @(-0.2081934f), @4.990155f ],
            @[ @0.5214378f, @(-9.700461f) ], @[ @(-8.813279f), @2.01615f ], @[ @1.692567f, @1.613987f ],
            @[ @22.59395f, @3.667872f ], @[ @(-0.7376698f), @2.368554f ], @[ @(-5.327563f), @(-4.063536f) ],
            @[ @3.085122f, @0.683227f ], @[ @(-4.933975f), @(-4.672351f) ] ];
        B1 =@[ @(-3.54918f), @4.236363f, @(-1.872206f), @(-5.380845f), @6.439789f, @1.403608f, @(-14.476072f), @1.475497f, @(-0.3763568f), @0.9561023f, @(-0.5417511f)] ;
        W2 =@[ @(-0.9070764f), @2.374942f, @5.143374f, @(-1.799813f), @0.9106352f, @2.599414f, @0.5643216f, @(-5.643482f), @(-4.367612f), @(-3.189569f), @4.452132f ] ;
        B2 = 1.135526;
    }
    
    return self;

}


- (float) getC: (NSArray*) p
{
    NSMutableArray *dH = [NSMutableArray arrayWithCapacity:[hDatabase count]];

    for (NSInteger i = 0; i < [hDatabase count]; i++)
    {
        [dH replaceObjectAtIndex:i withObject: [NSNumber numberWithFloat: fabs([hDatabase[i] floatValue] - [p[1] floatValue])]];
    }
    
    float min = [[dH objectAtIndex:0] floatValue] ;
    int m = 0;
    for( int i = 0; i < [dH count]; i++)
    {
        if ([dH[i] floatValue] < min){
            min = [dH[i] floatValue];
            m = i;
        }
    }
    
    NSMutableArray *dF = [NSMutableArray arrayWithCapacity:[fDatabase count]];
    for (NSInteger i = 0;  i < [fDatabase count]; i++)
    {
        dF[i] = [NSNumber numberWithFloat:fabs([fDatabase[i] floatValue] - [p[0] floatValue])];
    }
    min = [dF[0] floatValue];
    int n = 0;
    for (int i = 0; i < [dF count]; i++)
    {
        if ([dF[i] floatValue] < min)
        {
            min = [dF[i] floatValue];
            n =i;
        }
    }
    
    
    return [ cDatabase[m][n] floatValue ];
}


- (float) bpNet: (NSArray*) p
{
    NSInteger node = [B1 count];
    
    NSMutableArray *normIn = [NSMutableArray arrayWithCapacity:[p count]];
    normIn[0] = [NSNumber numberWithFloat: (([MAP_IN[1] floatValue] - [MAP_IN[0] floatValue]) * ([p[0] floatValue] - [IN[0][0] floatValue]) /
                 ([IN[0][1] floatValue] - [IN[0][0] floatValue]) + [MAP_IN[0] floatValue]) ];
    normIn[1] = [NSNumber numberWithFloat: ( ([MAP_IN[1] floatValue] - [MAP_IN[0] floatValue]) * ([p[1] floatValue] - [IN[1][0] floatValue] ) /
                 ( [IN[1][1] floatValue] - [IN[1][0] floatValue ]) + [MAP_IN[0] floatValue] ) ];
    
    float bpOut = 0.0;
    float inHide = 0.0;
    float outHide = 0.0;
    for (NSInteger i = 0; i < node; i++)
    {
        inHide = [ W1[i][0] floatValue ] * [ normIn[0] floatValue ] + [ W1[i][1] floatValue ] *
                [ normIn[1] floatValue ] + [ B1[i] floatValue ];
        outHide = (float)(1.0 / (1.0 + expf(-inHide)));
        bpOut += [ W2[i] floatValue ] * outHide;
    }
    bpOut += 1.135526F;
    
    float c = (bpOut - [ MAP_OUT[0] floatValue ]) * ( [ OUT[1] floatValue ] - [ OUT[0] floatValue ] ) /
            ([ MAP_OUT[1] floatValue ] - [ MAP_OUT[0] floatValue ]) + [ OUT[0] floatValue ];
    return c;
}


@end
