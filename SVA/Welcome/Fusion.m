//
//  Fusion.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import "Fusion.h"
#import "Configuration.h"
#import "MatrixOperate.h"
#import "StaticData.h"
#import "QandRPY.h"

@implementation Fusion
  static  float detaT = 0.02;
  static  float nativeMag = 43.700001;
  static  int sampleFreq = 400;

+(void)fusionEKFFloatArray:(float*)data WithResultSavedInFloatArray: (float*)result
{
    int numStationary = [StaticData coun];
    float q[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    for(int i = 0; i < 4; i++)
    {
        q[i] = [StaticData qAtIndex:i];
    }
    
    float P[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4;  j++)
        {
            P[i][j] = *([StaticData P] + i*4 + j);
        }
    }

    
    //float result[6][4] = {{0.0f, 0.0f, 0.0f, 0.0f},{0.0f, //0.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f, 0.0f},
      //  {0.0f, 0.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f, 0.0f}};
    int II[4][4] = {{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
    [MatrixOperate getDiagIntMatrix:&II[0][0] WithValue:1 withDemension:4];
    float twoKp = 0.0f;
    float twoKi = 0.0f;
    float integralFB[3] = {0.0f, 0.0f, 0.0f};
    float accLiner = 0.0f;
    float Accelerometer[3] = {0.0f, 0.0f, 0.0f};
    float Magnetometer[3] = {0.0f, 0.0f, 0.0f};
    float h[3] = {0.0f, 0.0f, 0.0f};
    float b[3] = {0.0f, 0.0f, 0.0f};
    float halfv[3] = {0.0f, 0.0f, 0.0f};
    float halfw[3] = {0.0f, 0.0f, 0.0f};
    float halfe[3] = {0.0f, 0.0f, 0.0f};
    float W[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f} };
    float T[4][3] = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f} };
    float F[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f} };
    float FP[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f} };
    float FPF[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float H[6][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float Ha[4][6] = {{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}};
    float P1Ha[4][6] = {{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}};
    float HP1Ha[6][6] = {{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}};
    float HP1HaR[6][6] = {{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}};
    float HP1HaRni[6][6] = {{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}};
    float Q[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}} ;
    float TQ[4][3] = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}} ;
    float TQT[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float P1[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float K[4][6] = {{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}} ;
    float KH[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float KHP1[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float Tbn[3][3] = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}};
    float Tnb[3][3] = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}};
    float Y1[3] = {0.0f, 0.0f, 0.0f};
    float aY1[3] = { 0.0f, 0.0f, 1.0f };
    float Y2[3] = {0.0f, 0.0f, 0.0f};
    float Y[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
    float zy[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
    float X[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    float X1[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    float Z[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
    float RPY[3] = {0.0f, 0.0f, 0.0f};
    float ar = 0.5f;
    float mr = 5.0f;
    float rr[6] = { ar, ar, ar, mr, mr, mr };
    float rr0[6] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
    int qq[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    [MatrixOperate getDiagIntMatrix:&qq[0][0] WithValue:1 withDemension:3];
    float R[6][6] = {{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f}};
    
    
    if (0.0 == *(data +9))
    {
        
        return ;
    }else
    {
        Accelerometer[0] = *data/ *(data + 9);
        Accelerometer[1] = *(data + 1)/ *(data + 9);
        Accelerometer[2] = *(data + 2)/ *(data + 9);
    }
    if (0.0 == *(data + 11)) {
        return ;
    }else
    {
        Magnetometer[0] = *(data + 6)/ *(data + 11);
        Magnetometer[1] = *(data + 7)/ *(data + 11);
        Magnetometer[2] = *(data + 8)/ *(data + 11);
    }
    rr0[0] = rr[0];
    rr0[1] = rr[1];
    rr0[2] = rr[2];
    rr0[3] = (float)(0.2 * rr[3]);
    rr0[4] = (float)(0.2 * rr[4]);
    rr0[5] = (float)(0.2 * rr[5]);
    if (*(data + 14) >=1.0 || fabs(*(data + 11) -43.700001)>25.0)
    {
      rr0[3] = 10000.0;
      rr0[4] = 10000.0;
      rr0[5] = 10000.0;
     
    }
    else if ((data[14] < 1.0) && (data[14] > 0.4) &&
      (fabs( data[11] - 43.700001F) < 25.0))
     {
         rr0[3] = data[14] * 2500.0;
         rr0[4] = data[14] * 2500.0;
         rr0[5] = data[14] * 2500.0;
      
     }
     else if ((data[14] < 0.4) && (data[14] > 0.25) &&
       (fabs(data[11] - 43.700001f) < 25.0))
     {
         rr0[3] = data[14] * 500.0;
         rr0[4] = data[14] * 500.0;
         rr0[5] = data[14] * 500.0;
      
     }
     else if ((data[14]< 0.25) && (fabs(data[11] - 43.700001) < 25.0))
     {
         rr0[3] = data[14] * 200.0;
         rr0[4] = data[14] * 200.0;
         rr0[5] = data[14] * 200.0;
       
     }
     accLiner = (float)fabs(data[9] - 9.2);
     if ((accLiner < 0.5) && (data[10] < 0.1))
     {
       numStationary++;
       twoKp = 0.05;
       if (numStationary > 50)
       {
         if ((data[14] < 1.0) && (fabs(data[11] - 43.700001) < 25.0))
         {
             rr0[0] = 0.1;
             rr0[1] = 0.1;
             rr0[2] = 0.1;
             rr0[3] = 1.0;
             rr0[4] = 1.0;
             rr0[5] = 1.0;
           
         }
         else
         {
             rr0[0] = 10.0;
             rr0[1] = 10.0;
             rr0[2] = 10.0;
             rr0[3] = 100.0;
             rr0[4] = 100.0;
             rr0[5] = 100.0;
           
         }
       }
       else if (numStationary > 20)
       {
         if ((data[14] < 1.0) && (fabs(data[11] - 43.700001) < 25.0))
         {
             rr0[0] = (float)(rr[0] * 0.05);
             rr0[1] = (float)(rr[0] * 0.05);
             rr0[2] = (float)(rr[0] * 0.05);
             rr0[3] = (float)(rr[0] * 0.5);
             rr0[4] = (float)(rr[0] * 0.5);
             rr0[5] = (float)(rr[0] * 0.5);
           
         }
         else
         {
             
             rr0[3] = rr[3]*10.0;
             rr0[4] = rr[4]*10.0;
             rr0[5] = rr[5]*10.0;
          
         }
       }
     }
     else
     {
       numStationary = 0;
       if ((accLiner >= 0.5) && (accLiner < 10.0) && (data[12] < 20.0))
       {
           rr0[0] = (float)( data[12] + accLiner * 0.5) ;
           rr0[1] = (float)( data[12] + accLiner * 0.5) ;
           rr0[2] = (float)( data[12] + accLiner * 0.5) ;
       }
       else
       {
           rr0[0] = 10000.0;
           rr0[1] = 10000.0;
           rr0[2] = 10000.0;
           rr0[3] = 100000.0;
           rr0[4] = 100000.0;
           rr0[5] = 100000.0;
       }
      
       if ((data[13] > 0.05) || (data[7] > 8.0) || (data[7] < -15.0))
       {
         twoKp = 0.0;
       }
       else
      {
         twoKp = 2.0;
          data[3] = 0.0;
          data[4] = 0.0;
          data[5] = 0.0;
        
       }
    
      }
    //R =[MatrixOperate getDiagFloatWithFloatArray:rr0];
    [MatrixOperate getDiagfloatMatrix:&R[0][0] withFloatArrayValue:rr0 withDimension:6];
    h[0] = ((float)(2.0 * (Magnetometer[0] * (0.5 - pow(q[2], 2.0) - pow(q[3], 2.0)) +
                            Magnetometer[1] * (q[1] * q[2] - q[0] * q[3]) + Magnetometer[2] * (q[1] * q[3] + q[0] * q[2]))));
    h[1] = ((float)(2.0 * (Magnetometer[0] * (q[1] * q[2] + q[0] * q[3]) +
                            Magnetometer[1] * (0.5 - q[1] * q[1] - q[3] * q[3]) + Magnetometer[2] * (q[1] * q[3] - q[0] * q[1]))));
    h[2] = ((float)(2.0 * (Magnetometer[0] * (q[1] * q[3] - q[0] * q[2]) +
                            Magnetometer[1] * (q[2] * q[3] + q[0] * q[1]) + Magnetometer[2] * (0.5 - q[1] * q[1] - q[2] * q[2]))));
    
    
    
    
    b[0] = 0.0f;
    b[1] = ((float)sqrt(pow(h[0], 2.0) + pow(h[1], 2.0)));
    b[2] = h[2];

    halfv[0] = (2.0f * (q[1] * q[3] - q[0] * q[2]));
    halfv[1] = (2.0f * (q[0] * q[1] + q[2] * q[3]));
    halfv[2] = ((float)(2.0 * (q[0] * q[0] - 0.5 + q[3] * q[3])));
    
    halfw[0] = (2.0f * b[1] * (q[1] * q[2] + q[0] * q[3]) +
                2.0f * b[2] * (q[1] * q[3] - q[0] * q[2]));
    halfw[1] = (2.0f * b[1] * (q[0] * q[0] - q[1] * q[1] + q[2] * q[2] - q[3] * q[3]) +
                2.0f * b[2] * (q[0] * q[1] + q[2] * q[3]));
    halfw[2] =((float)(2.0f * b[1] * (q[2] * q[3] - q[0] * q[1]) +
                       2.0f * b[2] * (0.5 - q[1] * q[1] - q[2] * q[2])));

    halfe[0] = (Accelerometer[1] * halfv[2] - Accelerometer[2] * halfv[1] + (
                Magnetometer[1] * halfw[2] - Magnetometer[2] * halfw[1]));
    halfe[1] = (Accelerometer[2] * halfv[0] - Accelerometer[0] * halfv[2] + (
                Magnetometer[2] * halfw[0] - Magnetometer[0] * halfw[2]));
    halfe[2] = (Accelerometer[0] * halfv[1] - Accelerometer[1] * halfv[0] + (
                Magnetometer[0] * halfw[1] - Magnetometer[1] * halfw[0]));

    if (twoKi > 0.0)
    {
        integralFB[0] += twoKi * halfe[0] * 0.0f;
        integralFB[1] += twoKi * halfe[1] * 0.0f;
        integralFB[2] += twoKi * halfe[2] * 0.0f;
        data[3] += integralFB[0];
        data[4] += integralFB[1];
        data[5] += integralFB[2];
        
    }
     else
     {
       integralFB[0] = 0.0;
       integralFB[1] = 0.0;
       integralFB[2] = 0.0;
     }
    
    
    float da3 = data[3];
    da3 += twoKp * halfe[0];
    data[3]= da3;
    
    float da4 = data[4];
    da4 += twoKp * halfe[1];
    data[4]= da4;
    
    float da5 = data[5];
    da5 += twoKp * halfe[2];
    data[5]= da5;
    
    [Fusion getWmatrix: &W[0][0] withFloat:data[3] withFloat:data[4] withFloat:data[5]];
    [Fusion getTmatrix: &T[0][0] withArray: q];
    
    float W_001[4][4]={{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    [MatrixOperate multiplyAbWithFloat:0.01 withFloatMatrix:&W[0][0] withRow:4 withCol:4 savedInMatrix:&W_001[0][0]];
    [MatrixOperate addABwithIntMatrix:&II[0][0] withRowOfA:4 withColOfA:4 withFloatMatrix:&W_001[0][0] withRowOfB:4 withColOfB:4 savedInMatrix: &F[0][0]];
    
    [MatrixOperate normArryWithFloatArray:b WithElemCount:3 SavedIn:b WithElemCount:3];
    
    [Fusion getFloatMatrixH:&H[0][0] withFloatArray:q withFloatArray:b ];
   
    
    float Wq[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    [MatrixOperate  multiplyABWithFloatMatrix:&W[0][0] WithRow:4 WithCol:4 withFloatArray:q WithElemCount:4 ResultSavedInArray:Wq];
    
    float Wq001[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    [MatrixOperate multiplyAbWithFloat:0.01 withFloat1Array:Wq WithElemCount:4 WithResultSavedIn:Wq001 ];
    [MatrixOperate addArrywithFloatArray:q WithElemCount:4
                          withFloatArray:Wq001 withElemCount:4
                    WithResultSaveInArry:q];
    [MatrixOperate normArryWithFloatArray:q WithElemCount:4 SavedIn:q WithElemCount:4];

    float temp = (float)pow(0.009999999776482582, 2.0);
    [MatrixOperate multiplyABWithFloatMatrix:&T[0][0] WithRow:4 WithCol:3 withIntMatrix:&qq[0][0] WithRow:3 WithCol:3
                WithResultSavedInFloatMatrix:&TQ[0][0] WithRow:4 WithCol:3];
    float T2[3][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    [MatrixOperate getA_TWithFloatMatrix:&T[0][0] WithRow:4 WithCol:3 withResultSavedInMatrix:&T2[0][0] WithRow:3 WithCol:4];
    
    //T = [MatrixOperate getA_TFloatWithFloatArray:T]; //zhuanzhi
    
    [MatrixOperate multiplyABWithFloatMatrix:&TQ[0][0] WithRow:4 WithCol:3
                             withFloatMatrix:&T2[0][0] WithRow:3 WithCol:4
                      WithResultSavedInFloatMatrix:&TQT[0][0] WithRow:4 WithCol:4];
    [MatrixOperate multiplyAbWithFloat:temp withFloatMatrix:&TQT[0][0] withRow:4 withCol:4 savedInMatrix:&Q[0][0]];
    
   // Q = [MatrixOperate multiplyAbFloatWithFloat:temp withFloatArray:TQT];
    [MatrixOperate multiplyABWithFloatMatrix:&F[0][0] WithRow:4 WithCol:4
                             withFloatMatrix:&P[0][0]  WithRow:4 WithCol:4
                WithResultSavedInFloatMatrix:&FP[0][0] WithRow:4 WithCol:4];
    
    //FP = [MatrixOperate multiplyABFloatWithFloatMatrix:F withFloatMatrix:P ];
  
    float F_T[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    [MatrixOperate getA_TWithFloatMatrix:&F[0][0] WithRow:4 WithCol:4 withResultSavedInMatrix:&F_T[0][0] WithRow:4 WithCol:4];
    //F = [MatrixOperate getA_TFloatWithFloatArray:F];
    [MatrixOperate multiplyABWithFloatMatrix:&FP[0][0] WithRow:4 WithCol:4
                             withFloatMatrix:&F_T[0][0] WithRow:4 WithCol:4
                WithResultSavedInFloatMatrix:&FPF[0][0] WithRow:4 WithCol:4];
   // FPF = [MatrixOperate multiplyABFloatWithFloatMatrix:FP withFloatMatrix:F ];
    [MatrixOperate addABWithFloatMatrix:&FPF[0][0] WithRow:4 WithCol:4
                       withFloatMatrix:&Q[0][0] WithRow:4 WithCol:4
               WithResultSavedInMatrix:&P1[0][0] WithRow:4 WithCol:4];
    [MatrixOperate getA_TWithFloatMatrix:&H[0][0] WithRow:6 WithCol:4 withResultSavedInMatrix:&Ha[0][0] WithRow:4 WithCol:6];
    [MatrixOperate multiplyABWithFloatMatrix:&P1[0][0] WithRow:4 WithCol:4 withFloatMatrix:&Ha[0][0] WithRow:4 WithCol:6
                WithResultSavedInFloatMatrix:&P1Ha[0][0] WithRow:4 WithCol:6];
    [MatrixOperate multiplyABWithFloatMatrix:&H[0][0] WithRow:6 WithCol:4 withFloatMatrix:&P1Ha[0][0] WithRow:4 WithCol:6 WithResultSavedInFloatMatrix:&HP1Ha[0][0] WithRow:6 WithCol:6];
    [MatrixOperate addABWithFloatMatrix:&HP1Ha[0][0] WithRow:6 WithCol:6 withFloatMatrix:&R[0][0] WithRow:6 WithCol:6
                WithResultSavedInMatrix:&HP1HaR[0][0] WithRow:6 WithCol:6];
    
    [MatrixOperate getNWithFloatMatrix:&HP1HaR[0][0] WithRow:6 WithCol:6
               WithResultSavedInMatrix:&HP1HaRni[0][0] WithRow:6 WithCol:6];
    //HP1HaRni = [MatrixOperate getNFloatWithFloatArray:HP1HaR];
  
    [MatrixOperate multiplyABWithFloatMatrix:&P1Ha[0][0] WithRow:4 WithCol:6
                             withFloatMatrix:&HP1HaRni[0][0] WithRow:6 WithCol:6
                WithResultSavedInFloatMatrix:&K[0][0] WithRow:4 WithCol:6];
    //K = [MatrixOperate multiplyABFloatWithFloatMatrix:P1Ha withFloatMatrix:HP1HaRni ];

    [Fusion getTbnWithFloatArray:q WithResultSavedInMatrix:&Tbn[0][0] WithRow:3 WithCol:3];
    [MatrixOperate getA_TWithFloatMatrix:&Tbn[0][0] WithRow:3 WithCol:3
                 withResultSavedInMatrix:&Tnb[0][0] WithRow:3 WithCol:3];
    //Tnb = [MatrixOperate getA_TFloatWithFloatArray:Tbn];
    [MatrixOperate multiplyABWithFloatMatrix:&Tnb[0][0] WithRow:3 WithCol:3
                              withFloatArray:aY1 WithElemCount:3
                          ResultSavedInArray:Y1];
    [MatrixOperate multiplyABWithFloatMatrix:&Tnb[0][0] WithRow:3 WithCol:3
                              withFloatArray:b WithElemCount:3
                          ResultSavedInArray:Y2];
    
    float m[3] = {0.0f, 0.0f, 0.0f};
    m[0] = data[6];
    m[1] = data[7];
    m[2] = data[8];
    
    [StaticData Q_Vector_tempAddObject:[NSMutableArray arrayWithObjects:
                                            [NSNumber numberWithFloat:q[0]],
                                            [NSNumber numberWithFloat:q[1]],
                                            [NSNumber numberWithFloat:q[2]],
                                            [NSNumber numberWithFloat:q[3]], nil] ];
    [Fusion getYArray:Y withFloatArray:Y1 withFloatArray:Y2];
    [Fusion getZArray:Z withFloat:Accelerometer[0] withFloat:Accelerometer[1] withFloat:Accelerometer[2] withFloat:Magnetometer[0] withFloat:Magnetometer[1] withFloat:Magnetometer[2]];
    
    X1[0] = q[0];
    X1[1] = q[1];
    X1[2] = q[2];
    X1[3] = q[3];
    
    [MatrixOperate minusArrytWithFloatArray:Z WithElemCount:6
                             withFloatArray:Y WithElemCount:6
                WIthResultSavedInFloatArray:zy WithElemCount:6];
    
    
    float tempArry[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    [MatrixOperate multiplyABWithFloatMatrix:&K[0][0] WithRow:4 WithCol:6
                              withFloatArray:zy WithElemCount:6
                          ResultSavedInArray:tempArry];
    [MatrixOperate addArrywithFloatArray:X1  WithElemCount:4
                          withFloatArray:tempArry withElemCount:4
                    WithResultSaveInArry:X];
    
    [MatrixOperate normArryWithFloatArray:X WithElemCount:4
                                  SavedIn:X WithElemCount:4];
    [MatrixOperate multiplyABWithFloatMatrix:&K[0][0] WithRow:4 WithCol:6
                             withFloatMatrix:&H[0][0] WithRow:6 WithCol:4
                WithResultSavedInFloatMatrix:&KH[0][0] WithRow:4 WithCol:4];
    [MatrixOperate multiplyABWithFloatMatrix:&KH[0][0] WithRow:4 WithCol:4
                             withFloatMatrix:&P1[0][0] WithRow:4 WithCol:4
                WithResultSavedInFloatMatrix:&KHP1[0][0] WithRow:4 WithCol:4];
    [MatrixOperate minusABWithFloatMatrix:&P1[0][0] WithRow:4 WithCol:4
                          withFloatMatrix:&KHP1[0][0] WithRow:4 WithCol:4
             WithResultSavedInFloatMatrix:&P[0][0] WithRow:4 WithCol:4];
    [QandRPY quaterToRPYWithFloat:X[0] withFloat:X[1] withFloat:X[2] withFloat:X[3] WithResultSavedInFloatArray:RPY WithElemCount:3 ];
    
    [Fusion getResultMatrix:result WithRow:6 WithCol:4
                     WithInt:numStationary
              withFloatArray:RPY
              withFloatArray:X
             withFloatMatrix:&P[0][0]];
    
    *(result + 2) = (float) (*(result + 2) + 3.141592653589793 * [Configuration angleOfYAxis] / 180.0);
    return ;
}

+(void)getWmatrix: (float*)w withFloat:(float)gx
                      withFloat:(float)gy
                      withFloat:(float)gz
{
    //w[4][4]
    *w = 0.0f;
    *(w + 1) = (-gx);
    *(w + 2) = (-gy);
    *(w + 3) = (-gz);
    *(w + 1 * 4) = gx;
    *(w + 1 * 4 + 1) = 0.0f;
    *(w + 1 * 4 + 2) = gz;
    *(w + 1 * 4 + 3) = (-gy);
    *(w + 2 * 4) = gy;
    *(w + 2 * 4 + 1) = (-gz);
    *(w + 2 * 4 + 2) = 0.0f;
    *(w + 2 * 4 + 3) = gx;
    *(w + 3 * 4) = gz;
    *(w + 3 * 4 + 1) = gy;
    *(w + 3 * 4 + 2) = (-gx);
    *(w + 3 * 4 + 3) = 0.0f;
    
    return ;
}
+(void)getTmatrix: (float*)t withArray:(float*)q0
{   //t[4][3]
    *t = q0[1];
    *(t + 1) = q0[2];
    *(t + 2) = q0[3];
    *(t + 1 * 4) = (-q0[0]);
    *(t + 1 * 4 + 1) = q0[3];
    *(t + 1 * 4 + 2) = (-q0[2]);
    *(t + 2 * 4) = (-q0[3]);
    *(t + 2 * 4 + 1) = (-q0[0]);
    *(t + 2 * 4 + 2) = q0[1];
    *(t + 3 * 4) = q0[2];
    *(t + 3 * 4 + 1)= (-q0[1]);
    *(t + 3 * 4 + 2) = (-q0[0]);
    return ;
}


+(void)getFloatMatrixH:(float*) h withFloatArray:(float*)q0
                      withFloatArray:(float*)b0
{
    //h[6][4]
    *(h + 0 * 4 + 0) = (-2.0f * q0[2]);
    *(h + 0 * 4 + 1) = (2.0f * q0[3]);
    *(h + 0 * 4 + 2) = (-2.0f * q0[0]);
    *(h + 0 * 4 + 3) = (2.0f * q0[1]);
    *(h + 1 * 4 + 0) = (2.0f * q0[1]);
    *(h + 1 * 4 + 1) = (2.0f * q0[0]);
    *(h + 1 * 4 + 2) = (2.0f * q0[3]);
    *(h + 1 * 4 + 3) = (2.0f * q0[2]);
    *(h + 2 * 4 + 0) = (2.0f * q0[0]);
    *(h + 2 * 4 + 1) = (-2.0f * q0[1]);
    *(h + 2 * 4 + 2) = (-2.0f * q0[2]);
    *(h + 2 * 4 + 3) = (2.0f * q0[3]);
    *(h + 3 * 4 + 0) = (2.0f * (q0[3] * b0[1] - q0[2] * b0[2]));
    *(h + 3 * 4 + 1) = (2.0f * (q0[2] * b0[1] + q0[3] * b0[2]));
    *(h + 3 * 4 + 2) = (2.0f * (q0[1] * b0[1] - q0[0] * b0[2]));
    *(h + 3 * 4 + 3) = (2.0f * (q0[0] * b0[1] + q0[1] * b0[2]));
    *(h + 4 * 4 + 0) = (2.0f * (q0[0] * b0[1] + q0[1] * b0[2]));
    *(h + 4 * 4 + 1) = (-2.0f * (q0[1] * b0[1] - q0[0] * b0[2]));
    *(h + 4 * 4 + 2) = (2.0f * (q0[2] * b0[1] + q0[3] * b0[2]));
    *(h + 4 * 4 + 3) = (-2.0f * (q0[3] * b0[1] - q0[2] * b0[2]));
    *(h + 5 * 4 + 0) = (-2.0f * (q0[1] * b0[1] - q0[0] * b0[2]));
    *(h + 5 * 4 + 1) = (-2.0f * (q0[0] * b0[1] + q0[1] * b0[2]));
    *(h + 5 * 4 + 2) = (2.0f * (q0[3] * b0[1] - q0[2] * b0[2]));
    *(h + 5 * 4 + 3) = (2.0f * (q0[2] * b0[1] + q0[3] * b0[2]));

    return ;
}
+(void)getTbnWithFloatArray:(float*)q0 WithResultSavedInMatrix:(float*)tbn WithRow:(int)row WithCol:(int)col

{
    
    //float[][] tbn = new float[3][3];
    if((row != 3)||(col != 3))
    {
        return;
    }
    *tbn = ((float)(2.0 * pow(q0[0], 2.0) - 1.0 + 2.0 * pow(q0[1], 2.0)));
    *(tbn + 1) = (2.0f * (q0[1] * q0[2] - q0[0] * q0[3]));
    *(tbn + 2) = (2.0f * (q0[1] * q0[3] + q0[0] * q0[2]));
    *(tbn + 1 * col ) = (2.0f * (q0[1] * q0[2] + q0[0] * q0[3]));
    *(tbn + 1 * col + 1) = ((float)(2.0 * pow(q0[0], 2.0) - 1.0 + 2.0 * pow(q0[2], 2.0)));
    *(tbn + 1 * col + 2) = (2.0f * (q0[2] * q0[3] - q0[0] * q0[1]));
    *(tbn + 2 * col) = (2.0f * (q0[1] * q0[3] - q0[0] * q0[2]));
    *(tbn + 2 * col +1) = (2.0f * (q0[2] * q0[3] + q0[0] * q0[1]));
    *(tbn + 2 * col +2) = ((float)(2.0 * pow(q0[0], 2.0) - 1.0 + 2.0 * pow(q0[3], 2.0)));

    return;
}

+(void)getYArray:(float*)y withFloatArray:(float*)y1 withFloatArray:(float*)y2
{
    y[0] = y1[0];
    y[1] = y1[1];
    y[2] = y1[2];
    y[3] = y2[0];
    y[4] = y2[1];
    y[5] = y2[2];
}

+(void)getZArray: (float*)z withFloat:(float)z1
                      withFloat:(float)z2
                      withFloat:(float)z3
                      withFloat:(float)z4
                      withFloat:(float)z5
                      withFloat:(float)z6
{
    z[0] = z1;
    z[1] = z2;
    z[2] = z3;
    z[3] = z4;
    z[4] = z5;
    z[5] = z6;
    return ;
    
}
+(void)getResultMatrix:(float*)all WithRow:(int)row WithCol:(int)col
                          WithInt:(int)n
                    withFloatArray:(float*)rpy
                    withFloatArray:(float*)x
                    withFloatMatrix:(float*)p
{
    //float[][] all = new float[6][4];
    if((row != 6)||(col != 4))
    {
        return ;
    }
    for (int i = 0; i < 3; i++)
    {
        *(all + i) = rpy[i];
    }
    *(all + 3) = n;
    for (int i = 0; i < 4; i++)
    {
        *(all + col + i) = x[i];
    }

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            *(all + (i + 2) * col +j) = *(p + i * 4 + j);
        }
    }
    return;
}










@end
