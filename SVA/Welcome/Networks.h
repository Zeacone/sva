//
//  Networks.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Networks : NSObject

- (instancetype) init;
- (float) getC: (NSArray*) p;
- (float) bpNet: (NSArray*) p;

@end
