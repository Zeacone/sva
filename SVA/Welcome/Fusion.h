//
//  Fusion.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fusion : NSObject
+(void)fusionEKFFloatArray:(float*)data WithResultSavedInFloatArray: (float*)result;


+(void)getWmatrix: (float*)w withFloat:(float)gx
        withFloat:(float)gy
        withFloat:(float)gz;
+(void)getTmatrix: (float*)t withArray:(float*)q0;




+(void)getFloatMatrixH:(float*) h withFloatArray:(float*)q0
        withFloatArray:(float*)b0;
+(void)getTbnWithFloatArray:(float*)q0
    WithResultSavedInMatrix:(float*)tbn WithRow:(int)row WithCol:(int)col;

+(void)getYArray:(float*)y withFloatArray:(float*)y1 withFloatArray:(float*)y2;

+(void)getZArray: (float*)z withFloat:(float)z1
                      withFloat:(float)z2
                      withFloat:(float)z3
                      withFloat:(float)z4
                      withFloat:(float)z5
                      withFloat:(float)z6;

+(void)getResultMatrix:(float*)all WithRow:(int)row WithCol:(int)col
               WithInt:(int)n
        withFloatArray:(float*)rpy
        withFloatArray:(float*)x
       withFloatMatrix:(float*)p;


@end
