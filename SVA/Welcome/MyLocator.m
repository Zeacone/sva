//
//  MyLocator.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//


#import <GameplayKit/GameplayKit.h>
#import "MyLocator.h"
#import "GlobalConfig.h"
#import "StaticData.h"
#import "Configuration.h"
#import "Paticle.h"

#define NAV_ANGLE_VDF 120
#define NAV_ANGLE_HW 240


@implementation MyLocator

static double DIRECTION_LOC_WEIGHT = 0.04; //when 0.06, 10m precicition only take 0.6m error, SVA data error weight on the direction of speed;
static double CROSS_LOC_WEIGHT = 0.02; //when 0.03, 10m precicition only take 0.3m error, SVA data error weight on the cross direciton of speed;
static double DEFAULT_NAV_ANGLE = 270;
static double STAY_SPEED_TH = 0.3;
static int STAY_SPEED_TH_COUNT = 1;
static double LOC_ERROR_TH = 8.0; //LOC ERROR THRESHOLD
static int LOC_ERROR_TH_COUNT = 5;//


static float last_loc_x = 0.0F;
static float last_loc_y = 0.0F;

+(void) init_last_loc
{
    last_loc_x = 0.0F;
    last_loc_y = 0.0F;
}

+(void) wifi_convertwithFloatPhy: (float)phy withFloatInputWifiX: (float) input_wifi_x withFloatInputWifiY: (float) input_wifi_y  withFloatCurrentLocationX: (float)current_loc_x
       withFloatCurrentLocationY: (float)current_loc_y UpdateArray: (float*)newloc
{
    
    float x2 = input_wifi_x;
    float y2 = input_wifi_y;
    float x1 = current_loc_x;
    float y1 = current_loc_y;
    double theta = 0.0;
    if (x2 == x1)
    {
        if (y2 > y1)
        {
            theta = (double)1.570796326794897;
        }
        else
        {
            theta = (double)4.71238898038469;
        }
    }
    else
    {
        theta = atanf((y2 - y1) / (x2 - x1));
    }
    if (x2 < x1)
    {
        theta += (double)3.141592653589793;
    }
    float phy_tmp = (float)((double)7.853981633974483 - phy);
    double alpha = fabs(phy_tmp - theta);
    double dis = cos(alpha) * sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    
    float ret[2];
    ret[0] = x1 + (float)cos(phy_tmp) * (float)dis;
    ret[1] = y1 + (float)sin(phy_tmp) * (float)dis;
    ret[0] = (ret[0] * 2.0 + x2) / 3.0;
    ret[1] = (ret[1] * 2.0 + y2) / 3.0;

    newloc[0] = ret[0];
    newloc[1] = ret[1];
    return;

}

+(void) wifi_convert_1withFloat: (float)phy withFloatArray:(float*) input_wifi
                      withFloat: (float)current_loc_x withFloat: (float)current_loc_y
          WithResultSavedInArry:(float*) ret WithElemCount:(int)lenOfRet
{
    if(lenOfRet != 0)
    {
        return;
    }
    
    float x2 = input_wifi[0];
    float y2 = input_wifi[1];
    float x1 = current_loc_x;
    float y1 = current_loc_y;
    double theta = 0.0;

    if (x2 == x1)
    {
        if (y2 > y1)
        {
            theta = (double)1.570796326794897;
        }
        else
        {
            theta = (double)4.71238898038469;
        }
    }
    else
    {
        theta = atan((y2 - y1) / (x2 - x1));
    }
    if (x2 < x1)
    {
        theta += (double)3.141592653589793;
    }
    float phy_tmp = (float)((double)7.853981633974483 - phy);
    double alpha = fabs(phy_tmp - theta);
    double dis = cos(alpha) * sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
//    float ret[2] = {0.0f, 0.0f};
    ret[0] = (x1 + (float)cos(phy_tmp) * (float)dis);
    ret[1] = (y1 + (float)sin(phy_tmp) * (float)dis);
    
    return;
}

+(void) meterToPixelwithFloat: (float)fx withFloat: (float)fy WithResultSavedInArray:(int*)ixy WithElemCount:(int)lenOfIxy
{
    //if (lenOfIxy != 3)
    //{
     //   return ;
    //}
    if ([Configuration changeXY] == 0) {
        ixy[0] = ((int)([GlobalConfig proportion] * fx + (float) [GlobalConfig xSport] ));
        ixy[1] = ((int)([GlobalConfig proportion] * fy + (float) [GlobalConfig ySport]));
      //  NSLog(@"%f %f %f", [GlobalConfig proportion], fx, [GlobalConfig xSport]);
    }
    else {
        ixy[0] = ((int)([GlobalConfig proportion] * fy + (float) [GlobalConfig xSport]));
        ixy[1] = ((int)([GlobalConfig proportion] * fx + (float) [GlobalConfig ySport]));
    }
    return;
}


+(void) invalidPaticleProcesswithPaticleArray:(NSMutableArray*)paticleTemp
                                               withFloat: (float)pdrX withFloat:(float)pdrY
                                    withFloat: (float)fi withInt: (int)pdrFloorIndex
                       WithResultSavedInArray: (int*)ixy WithElemCount:(int)lenOfIxy
{
    if(lenOfIxy != 2)
    {
        return;
    }
    
    float fxy[3] = { 0.0f, 0.0f, 0.0f };

    int invalidProcessFlag = 0;
    //int ixy[2] = {0,0};
    
    if ((invalidProcessFlag == 0) && ([StaticData locationWifiAtIndex:0] != (0.0 / 0.0))
        && ([StaticData locationWifiAtIndex:1] != (0.0 / 0.0))) {
        [self reset2WifiwithFloat:pdrX withFloat:pdrY withInt:pdrFloorIndex
                   withFloatArray:fxy WithElemCout:sizeof(fxy)/sizeof(fxy[0])
           WithResultSavedInArray:ixy WithElemCount:lenOfIxy];
      //   NSLog(@"enter invalidPaticleProcess!");
    }
   // [StaticData paticleNum];

    [[StaticData sepreX] addObject: [NSNumber numberWithFloat:fxy[0]]];
    [[StaticData sepreY1] addObject:[NSNumber numberWithFloat:fxy[1]]];
    [[StaticData sepreFloorInde] addObject: [NSNumber numberWithInt:((int)fxy[2])]];

    if ([[StaticData preX] count] > 2) {
        [[StaticData sepreX] removeObjectAtIndex:0];
        [[StaticData sepreY1] removeObjectAtIndex:0];
        [[StaticData sepreFloorInde] removeObjectAtIndex:0];
    }
    return;
}

+(void) reset2WifiwithFloat:(float)pdrX  withFloat:(float)pdrY withInt:(int)pdrFloorIndex withFloatArray:(float*)fxy WithElemCout:(int)lenOfFxy
                WithResultSavedInArray:(int*)ixy WithElemCount:(int)lenOfIxy
{
    if(lenOfIxy != 2)
    {
        return;
    }
    if (([Configuration resetWifiFlag] == 1) || ([StaticData changeFloorFlag] == 1)) {
        fxy[0] = [StaticData locationWifiAtIndex:0];
        fxy[1] = [StaticData locationWifiAtIndex:1];
        fxy[2] = [StaticData locationWifiAtIndex:2];
    }
    else if ((pdrFloorIndex == (int)[StaticData locationWifiAtIndex:2]) && ([StaticData noReadingModeCount] < 3))
    {
        fxy[0] = (([StaticData locationWifi_globalAtIndex:0] + pdrX * 2.0) / 3.0);
        fxy[1] = (([StaticData locationWifi_globalAtIndex:1] + pdrY * 2.0) / 3.0);
        fxy[2] = pdrFloorIndex;
    }
    else {
        fxy[0] = [StaticData locationWifi_globalAtIndex:0];
        fxy[1] = [StaticData locationWifi_globalAtIndex:1];
        fxy[2] = [StaticData locationWifi_globalAtIndex:2];
    }

    [self meterToPixelwithFloat:fxy[0] withFloat:fxy[1]
         WithResultSavedInArray:ixy WithElemCount:lenOfIxy];
    ixy[2] = ((int)fxy[2]);
    int cnt = 0;
    float fx = 0.0;
    float fy = 0.0;
    int tempixy[2] = {0, 0};
    //Random r = new Random();
    GKRandomDistribution *r ;
    
   // arc4random() % 100
    if ([Configuration randomFlag] == 1) {
        r = [StaticData ser];
    }
    double radius = [Configuration resetPaticleRadius];

    for (int i = 0; i < [StaticData paticleNum]; i++) {
        cnt = 0;
        radius = [Configuration resetPaticleRadius];
        while (true) {
            if (cnt > 20) {
                radius *= (double)2.0;
                if (radius > 20.0)
                {
                    radius = 20.0;
                }
                cnt = 0;
            }

            fx = (float)(fxy[0] + ([r nextUniform ] - 0.5) * 2.0 * radius);
            fy = (float)(fxy[1] + ([r nextUniform] - 0.5) * 2.0 * radius);
            
            [self meterToPixelwithFloat:fx withFloat:fy
                 WithResultSavedInArray:tempixy WithElemCount:sizeof(tempixy)/sizeof(tempixy[0])];
            if ([self isValidLocationwithInt:tempixy[0] withInt:tempixy[1]] ) {
                break;
            }
            cnt++;
        }
        Paticle *paticle = [StaticData paticle][i];
        [paticle setPaticleV:0.0 ];
        [paticle setPaticleFi:0.0];
        [paticle setPaticleX:fx];
        [paticle setPaticleY:fy];
        [paticle setPaticleW:((float)(1.0 / [StaticData paticleNum]))];
    }
    return;
}


+(NSMutableArray*) pixelToMeterwithInt: (int)ix withInt: (int)iy
{
    NSRange range3;
    range3.location = 0;
    range3.length = 3;
    NSMutableArray *fxy = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:range3]]; //float[3]
    if ([Configuration changeXY] == 0) {
        fxy[0] = [NSNumber numberWithFloat:((int)(ix - (float) [GlobalConfig xSport]) / (float)[GlobalConfig proportion])];
        fxy[1] = [NSNumber numberWithFloat:((int)(iy -(float) [GlobalConfig ySport]) / (float)[GlobalConfig proportion]) ];
    }
    else {
        fxy[0] = [NSNumber numberWithFloat:((int)(iy - (float) [GlobalConfig xSport]) / (float)[GlobalConfig proportion]) ];
        fxy[1] = [NSNumber numberWithFloat:((int)(ix - (float) [GlobalConfig ySport]) / (float)[GlobalConfig proportion]) ];
    }
    return fxy;
}



//////////////////////////////////////changed by zm///////////////////////////////////
//
+(void) paticleFilterwithFloat:(float)x withFloat:(float)y withInt: (NSString *)floor withFloat: (float)fi withFloat: (float)v WithResultSavedInArray:(int*)ixy WithElemCount:(int)lenOfIxy
{
    NSArray *parameter = ((NSArray *)[[NSUserDefaults standardUserDefaults] objectForKey:@"inherit"]);
    if (parameter && parameter.count != 0) {
        NSString *temp_hw_angle = parameter[0];
        NSString *temp_vf_angle = parameter[1];
        NSString *temp_direction_loc_weight = parameter[5];
        NSString *temp_cross_loc_weight = parameter[6];
        NSString *temp_static_speed = parameter[7];
        NSString *temp_static_speed_count = parameter[8];
        NSString *temp_loc_error = parameter[9];
        NSString *temp_loc_error_count = parameter[10];
        
        if ([floor isEqualToString:@"VF"]) {
            DEFAULT_NAV_ANGLE = temp_vf_angle ? temp_vf_angle.doubleValue : NAV_ANGLE_VDF;
        } else {
            DEFAULT_NAV_ANGLE = temp_hw_angle ? temp_hw_angle.doubleValue : NAV_ANGLE_HW;;
        }
        
        DIRECTION_LOC_WEIGHT = temp_direction_loc_weight ? temp_direction_loc_weight.doubleValue : DIRECTION_LOC_WEIGHT;
        CROSS_LOC_WEIGHT = temp_cross_loc_weight ? temp_cross_loc_weight.doubleValue : CROSS_LOC_WEIGHT;
        STAY_SPEED_TH = temp_static_speed ? temp_static_speed.doubleValue : STAY_SPEED_TH;
        STAY_SPEED_TH_COUNT = temp_static_speed_count ? temp_static_speed_count.doubleValue : STAY_SPEED_TH_COUNT;
        LOC_ERROR_TH = temp_loc_error ? temp_loc_error.doubleValue : LOC_ERROR_TH;
        LOC_ERROR_TH_COUNT = temp_loc_error_count ? temp_loc_error_count.doubleValue : LOC_ERROR_TH_COUNT;
    }
    
    [StaticData selocationWifi_global:x AtIndex:0];
    [StaticData selocationWifi_global:y AtIndex:1];
    
    [StaticData selocationWifi:x AtIndex:0];
    [StaticData selocationWifi:y AtIndex:1];
    
    float fxy[2] = { 0.0f, 0.0f };
    
    //deltaFi = (float)-210/180*3.1415926;
    
    fi += (float)DEFAULT_NAV_ANGLE/(float)180*3.1415926; // TMP CODE
    
//    if ([Configuration deltaFiFlag] == 1) {
//        //fi += [StaticData deltaFi];
//        
//    }
    
    if (fi > 6.283185307179586) {
        fi = (float)fmodf(fi , 6.283185307179586);
    }
    
    fi = 1.5707963267949 - fi; // change nav angle to axis angle; nav angle use north 0 and lefthand, axis angle use x axi and use righthand.
    
    if(last_loc_x != 0.0F)
    {
        fxy[0] = last_loc_x;
    }
    else
    {
        fxy[0] = x;
    }
    if(last_loc_y != 0.0F)
    {
        fxy[1] = last_loc_y;
    }
    else
    {
        fxy[1] = y;
    }
    
    //fxy[0] += v*cos(fi*180/3.1415926);
    //fxy[1] += v*sin(fi*180/3.1415926);
    
    NSLog(@"last x = %f, last y = %f, x = %f, y = %f", last_loc_x, last_loc_y, x, y);
    
    fxy[0] += v*cos(fi);  // use speed and nav angle to calcue move point;
    fxy[1] += v*sin(fi);  // use speed and nav angle to calcue move point;
    
    float relate_distance = (float)sqrt(pow(fxy[0] - x, 2.0) + pow(fxy[1] - y, 2.0));
//
    float loc_dx = x - fxy[0];
    float loc_dy = y - fxy[1];
    
    // add loc error on direction
    fxy[0] = fxy[0] + DIRECTION_LOC_WEIGHT * (loc_dx*cos(fi) + loc_dy*sin(fi))*cos(fi);
    fxy[1] = fxy[1] + DIRECTION_LOC_WEIGHT * (loc_dx*cos(fi) + loc_dy*sin(fi))*sin(fi);
    
    // add loc error on cross direction
    fxy[0] = fxy[0] + DIRECTION_LOC_WEIGHT * (loc_dx*sin(fi) + loc_dy*cos(fi))*cos(fi);
    fxy[1] = fxy[1] + DIRECTION_LOC_WEIGHT * (loc_dx*sin(fi) + loc_dy*cos(fi))*sin(fi);
    
    
//    NSLog(@"dir=%f, cros=%f, de_nav=%f, stay_th=%f, stay_c=%d, loc_th=%f, loc_c=%d", DIRECTION_LOC_WEIGHT, CROSS_LOC_WEIGHT, DEFAULT_NAV_ANGLE, STAY_SPEED_TH, STAY_SPEED_TH_COUNT, LOC_ERROR_TH, LOC_ERROR_TH_COUNT);
//    NSLog(@"nav=%f, error=%f, steep=%f, loc_diff=%f", fi, relate_distance, (float)sqrt(pow(fxy[0] - last_loc_x, 2.0) + pow(fxy[1] - last_loc_y, 2.0)), DIRECTION_LOC_WEIGHT*(loc_dx*cos(fi) + loc_dy*sin(fi)));
    
    
    NSLog(@"V=%f", v);
    if(v < STAY_SPEED_TH)
    {
        [StaticData setStayCount: ([StaticData StayCount] + 1)];
        if([StaticData StayCount] >= STAY_SPEED_TH_COUNT)
        {
            if(last_loc_x != 0.0F)
            {
                fxy[0] = last_loc_x;
            }
            if(last_loc_y != 0.0F)
            {
                fxy[1] = last_loc_y;
            }
        }
        
    }
    else if(relate_distance > LOC_ERROR_TH)
    {
        //NSLog(@"x=%f, y=%f  fx= %f, fy=%f , erroLogcount = %d", x, y, fxy[0], fxy[1], [StaticData ErrorLocCount]);
        [StaticData setStayCount:0];
        [StaticData setErrorLocCount: ([StaticData ErrorLocCount] + 1)];
        if([StaticData ErrorLocCount] >= LOC_ERROR_TH_COUNT)
        {
            fxy[0] = x;
            fxy[1] = y;
        }
        
    }
    else
    {
        [StaticData setErrorLocCount:0];
    }
    
    last_loc_x = fxy[0];
    last_loc_y = fxy[1];
    
    
   [self isValidLocationwithInt:ixy[0] withInt:ixy[1]];
    
    [[StaticData sepreX] addObject: [NSNumber numberWithFloat:fxy[0]]];
    [[StaticData sepreY1] addObject: [NSNumber numberWithFloat:fxy[1]]];
    
//    if ((int)[[StaticData preX] count] > 2) {
//        [[StaticData sepreX] removeObjectAtIndex:0];
//        [[StaticData sepreY1] removeObjectAtIndex:0];
//    }
//
//    [self meterToPixelwithFloat:fxy[0] withFloat:fxy[1]
//         WithResultSavedInArray:ixy WithElemCount:2];
    ixy[2] = [[[StaticData preFloorIndex] lastObject] intValue];
    
    ixy[0] = fxy[0];
    ixy[1] = fxy[1];
    
    return ;
}


//////////////////////////////////////changed by zm///////////////////////////////////
////////////////////////////////changed end/////////////////////
         

+(void) resamplingwithRandom: r withFloatArray: (float*)fxy WithElemCount:(int)lenOfFxy
                     withInt: (int)replaceNum
{
    if( lenOfFxy != 2 )
    {
        return;
    }
    
    double radius = 2.0;
    int cnt = 0;
    int random;
    for (int i = replaceNum; i < [StaticData paticleNum]; i++) {
        cnt = 0;
        radius = 2.0;
        Paticle *paticle = [StaticData paticle][i];
        while (true) {
            if (cnt > 20) {
                radius *= 2.0;
                if (radius > 10.0) {
                    int tmp = MIN(replaceNum, 100);
                    if ([Configuration randomFlag]  == 1)
                        random = (int)[[StaticData ser] nextUniform] * tmp;    //nextGaussian
                    else {
                        random = (int)[r nextUniform] * tmp;
                    }
                    [paticle setPaticleX: [[StaticData paticle][random] paticleX]];
                    [paticle setPaticleY: [[StaticData paticle][random] paticleY]];
                    break;
                }
                cnt = 0;
            }
            if ([Configuration randomFlag] == 1)
                random = (int)[[StaticData ser] nextUniform]; //nextGaussian();
            else
            {
                random = (int)[r nextUniform];   //nextGaussian();
            }

            [paticle setPaticleX: ((float)(fxy[0] + random * radius))];

            if ([Configuration randomFlag] == 1)
                random = (int)[[StaticData ser] nextUniform];     //nextGaussian();
            else {
                random = (int)[r nextUniform];     //nextGaussian();
            }
            [paticle setPaticleY: ((float)(fxy[1] + random * radius))];
            
            int tempXY[2] = {0, 0};
            [self meterToPixelwithFloat:[paticle paticleX] withFloat:[paticle paticleY]
                 WithResultSavedInArray:tempXY WithElemCount:sizeof(tempXY)/sizeof(tempXY[0])];

            if ([self isValidLocationwithInt:tempXY[0] withInt:tempXY[1]]) {
                break;
            }
            cnt++;
        }

    }
    for (int i = 0; i < [StaticData paticleNum]; i++) {
        Paticle *paticle = [StaticData paticle][i];
        [paticle setPaticleW: (1.0 / [StaticData paticleNum]) ];
    }
}


+(void) enhancedResamplingwithFloatArray: (float*)fxy WithElemCount:(int)lenOfFxy
                                 withInt: (int)ratio
{
    if( lenOfFxy != 2)
    {
        return;
    }
    
    int i = 0;
    GKRandomDistribution *r =
    [[GKRandomDistribution alloc] initWithRandomSource: [[GKARC4RandomSource alloc] init] lowestValue:0 highestValue:1];  //Random r = new Random();
    
    if ([Configuration randomFlag] == 1) {
        r = [StaticData ser];
    }
    int index = 0;
    int radius = 2;

    int count = 0;
    while (i < [StaticData paticleNum] / ratio) {
        if (count > 999)
        {
            break;
        }

        index = (int) [r nextInt];                   //r.nextInt([StaticData paticleNum] - 1);
        Paticle *paticle = [StaticData paticle][index];

        float fx = (float)(fxy[0] + ([r nextUniform] - 0.5) * 2.0 * radius);
        float fy = (float)(fxy[1] + ([r nextUniform] - 0.5) * 2.0 * radius);

        int ixy[2] = {0, 0};
        [self meterToPixelwithFloat:fx withFloat:fy
             WithResultSavedInArray:ixy WithElemCount:sizeof(ixy)/sizeof(ixy[0])];

        if ([self isValidLocationwithInt:ixy[0] withInt:ixy[1]]) {
            [paticle setPaticleX: fx];
            [paticle setPaticleY: fy];
            i++;
            count = 0;
        }
        else {
            count++;
        }
    }
}



+(BOOL) isValidLocationwithInt: (int)x withInt: (int)y
{
    return true;
}


@end
