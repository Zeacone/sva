//
//  SVANavAngleAndSpeed.h
//
#import <Foundation/Foundation.h>
#import <libkern/OSAtomic.h>

#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>
@interface SVANavAngleAndSpeed : NSObject
{
    @public
    double nav_velocity;
    double nav_angle;
}

+ (instancetype)defaultNav;
-(void)threadStart;
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (atomic, strong) NSMutableArray       *acc_result_array;

@end
