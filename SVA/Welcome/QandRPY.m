//
//  QandRPY.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import "QandRPY.h"
#import "StaticData.h"

@implementation QandRPY
+(void)getRPY0:(double*)rpy
    WithDouble:(double)ax
    withDouble:(double)ay
    withDouble:(double)az
    withDouble:(double)mx
    withDouble:(double)my
    withDouble:(double)mz
{
    double normAcc = 0.0;
    double normMag = 0.0;
    double roll = 0.0;
    double pitch = 0.0;
    double Hx = 0.0;
    double Hy = 0.0;
    double head = 0.0;
    //double rpy = new double[3];

    normAcc = sqrt(pow(ax, 2.0) + pow(ay, 2.0) + pow(az, 2.0));
    normMag = sqrt(pow(mx, 2.0) + pow(my, 2.0) + pow(mz, 2.0));
    ax /= normAcc;
    ay /= normAcc;
    az /= normAcc;
    mx /= normMag;
    my /= normMag;
    mz /= normMag;

    roll = atan2(ax, sqrt(pow(ay, 2.0) + pow(az, 2.0)));
    pitch = atan2(-ay, az);
    Hx = mx * cos(roll) + my * sin(pitch) * sin(roll) - mz * cos(pitch) * sin(roll);
    Hy = my * cos(pitch) + mz * sin(pitch);
    head = -atan2(Hx, Hy);

    if (head < 0.0)
    {
        head += 6.283185307179586;
    }
    rpy[0] = roll;
    rpy[1] = pitch;
    rpy[2] = head;

    return ;
}


+ (void)getRPY0:(float*)rpy
      WithFloat:(float)ax
      withFloat:(float)ay
      withFloat:(float)az
      withFloat:(float)mx
      withFloat:(float)my
      withFloat:(float)mz
{
    float normAcc = 0.0f;
    float normMag = 0.0f;
    float roll = 0.0f;
    float pitch = 0.0f;
    float Hx = 0.0f;
    float Hy = 0.0f;
    float head = 0.0f;
    //float[] rpy = new float[3];

    normAcc = (float)sqrt(pow(ax, 2.0) + pow(ay, 2.0) + pow(az, 2.0));
    normMag = (float)sqrt(pow(mx, 2.0) + pow(my, 2.0) + pow(mz, 2.0));
    ax /= normAcc;
    ay /= normAcc;
    az /= normAcc;
    mx /= normMag;
    my /= normMag;
    mz /= normMag;

    roll = (float)atan2(ax, sqrt(pow(ay, 2.0) + pow(az, 2.0)));
    pitch = (float)atan2(-ay, az);
    Hx = (float)(mx * cos(roll) + my * sin(pitch) * sin(roll) - mz * cos(pitch) * sin(roll));
    Hy = (float)(my * cos(pitch) + mz * sin(pitch));
    head = (float)-atan2(Hx, Hy);

    if (head < 0.0f)
    {
        head = (float)(head + 6.283185307179586);
    }
    rpy[0] = roll;
    rpy[1] = pitch;
    rpy[2] = head;
    return ;
  }

+(void)getQuater0DoubleArray:(double*)Quat WithElemCout:(int)lenOfQuat
   WithFloatArray:(double*)RPY WithElemCount:(int)lenOfRPY
{
    if ((lenOfQuat != 4) || (lenOfRPY != 3))
    {
        return;
    }

    double mPitch = 0.0;
    double mRoll = 0.0;
    double mHead = 0.0;
    double Tbn[3][3] = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
    double q1 = 0.0;
    double q2 = 0.0;
    double q3 = 0.0;
    double q4 = 0.0;
    //double Quat[4] = {0.0, 0.0, 0.0, 0.0};
    double flag = 0.0;

    mRoll = RPY[0];
    mPitch = RPY[1];
    mHead = RPY[2];

    Tbn[0][0] = (cos(mRoll) * cos(mHead));
    Tbn[0][1] = (cos(mPitch) * sin(mHead) +
                 sin(mRoll) * sin(mPitch) * cos(mHead));
    Tbn[0][2] = (sin(mPitch) * sin(mHead) -
                 cos(mPitch) * sin(mRoll) * cos(mHead));
    Tbn[1][0] = (-cos(mRoll) * sin(mHead));
    Tbn[1][1] = (cos(mPitch) * cos(mHead) -
                 sin(mRoll) * sin(mPitch) * sin(mHead));
    Tbn[1][2] = (sin(mPitch) * cos(mHead) +
                 sin(mRoll) * sin(mHead) * cos(mPitch));
    Tbn[2][0] = sin(mRoll);
    Tbn[2][1] = (-sin(mPitch) * cos(mRoll));
    Tbn[2][2] = (cos(mRoll) * cos(mPitch));

    q2 = sqrt(fabs(1.0 + Tbn[0][0] - Tbn[1][1] - Tbn[2][2])) / 2.0;
    q3 = sqrt(fabs(1.0 - Tbn[0][0] + Tbn[1][1] - Tbn[2][2])) / 2.0;
    q4 = sqrt(fabs(1.0 - Tbn[0][0] - Tbn[1][1] + Tbn[2][2])) / 2.0;
    q1 = sqrt(fabs(1.0 - pow(q2, 2.0) - pow(q3, 2.0) - pow(q4, 2.0)));

    flag = cos(mHead / 2.0) * cos(mPitch / 2.0) * cos(mRoll / 2.0) -
            sin(mHead / 2.0) * sin(mPitch / 2.0) * sin(mRoll / 2.0);

    if (flag > 0.0)
    {
        if (Tbn[2][1] < Tbn[1][2])
        {
            q2 = -q2;
        }
        if (Tbn[0][2] < Tbn[2][0])
        {
            q3 = -q3;
        }
        if (Tbn[1][0] < Tbn[0][1])
        {
            q4 = -q4;
        }
    }
    else
    {
        q1 = -q1;
        if (Tbn[2][1] > Tbn[1][2])
        {
            q2 = -q2;
        }
        if (Tbn[0][2] > Tbn[2][0])
        {
            q3 = -q3;
        }
        if (Tbn[1][0] > Tbn[0][1])
        {
            q4 = -q4;
        }
    }

    Quat[0] = q1;
    Quat[1] = q2;
    Quat[2] = q3;
    Quat[3] = q4;
    
    return ;
}

+(void)getQuater0FloatArray:(float*)Quat WithElemCout:(int)lenOfQuat
   WithFloatArray:(float*)RPY WithElemCount:(int)lenOfRPY
{
    if ((lenOfQuat != 4) || (lenOfRPY != 3))
    {
        return;
    }
    float mPitch = 0.0f;
    float mRoll = 0.0f;
    float mHead = 0.0f;
    float Tbn[3][3];
    float q1 = 0.0f;
    float q2 = 0.0f;
    float q3 = 0.0f;
    float q4 = 0.0f;
    //float Quat[4] = new float[4];
    float flag = 0.0f;

    mRoll = RPY[0];
    mPitch = RPY[1];
    mHead = RPY[2];

    Tbn[0][0] = ((float)(cos(mRoll) * cos(mHead)));
    Tbn[0][1] = ((float)(cos(mPitch) * sin(mHead) +
                         sin(mRoll) * sin(mPitch) * cos(mHead)));
    Tbn[0][2] =((float)(sin(mPitch) * sin(mHead) -
                        cos(mPitch) * sin(mRoll) * cos(mHead)));
    Tbn[1][0] = ((float)(-cos(mRoll) * sin(mHead)));
    Tbn[1][1] = ((float)(cos(mPitch) * cos(mHead) -
                         sin(mRoll) * sin(mPitch) * sin(mHead)));
    Tbn[1][2] = ((float)(sin(mPitch) * cos(mHead) +
                         sin(mRoll) * sin(mHead) * cos(mPitch)));
    Tbn[2][0] = ((float)sin(mRoll));
    Tbn[2][1] = ((float)(-sin(mPitch) * cos(mRoll)));
    Tbn[2][2] = ((float)(cos(mRoll) * cos(mPitch)));

    q2 = (float)(sqrt(fabsf(1.0f + Tbn[0][0] - Tbn[1][1] - Tbn[2][2])) / 2.0);
    q3 = (float)(sqrt(fabsf(1.0f - Tbn[0][0] + Tbn[1][1] - Tbn[2][2])) / 2.0);
    q4 = (float)(sqrt(fabsf(1.0f - Tbn[0][0] - Tbn[1][1] + Tbn[2][2])) / 2.0);
    q1 = (float)sqrt(fabs(1.0 - pow(q2, 2.0) - pow(q3, 2.0) - pow(q4, 2.0)));

    flag = (float)(cos(mHead / 2.0) * cos(mPitch / 2.0) * cos(mRoll / 2.0) -
                   sin(mHead / 2.0) * sin(mPitch / 2.0) * sin(mRoll / 2.0));

    if (flag > 0.0f)
    {
        if (Tbn[2][1] < Tbn[1][2])
        {
            q2 = -q2;
        }
        if (Tbn[0][2] < Tbn[2][0])
        {
            q3 = -q3;
        }
        if (Tbn[1][0] < Tbn[0][1])
        {
            q4 = -q4;
        }
    }
    else
    {
        q1 = -q1;
        if (Tbn[2][1] > Tbn[1][2])
        {
            q2 = -q2;
        }
        if (Tbn[0][2] > Tbn[2][0])
        {
            q3 = -q3;
        }
        if (Tbn[1][0] > Tbn[0][1])
        {
            q4 = -q4;
        }
    }

    Quat[0] = q1;
    Quat[1] = q2;
    Quat[2] = q3;
    Quat[3] = q4;

    return;
}


+(void)quaterToRPYWithDouble:(double)q0
                                   withDouble:(double)q1
                                   withDouble:(double)q2
                                   withDouble:(double)q3
       WithResultSavedInFloatArray:(double*)rpy WithElemCount:(int)elemcount

{
    if (elemcount !=3)
        return;
    double Tbn[3][3] = {{0.0, 0.0, 0.0 }, {0.0, 0.0, 0.0 }, {0.0, 0.0, 0.0 }};
    double head = 0.0;
    double pitch = 0.0;
    double roll = 0.0;
    //double[] rpy = new double[3];

    Tbn[0][0] = (q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3);
    Tbn[0][1] = (2.0 * (q1 * q2 - q0 * q3));
    Tbn[0][2] = (2.0 * (q1 * q3 + q0 * q2));
    Tbn[1][0] = (2.0 * (q1 * q2 + q0 * q3));
    Tbn[1][1] = (q0 * q0 + q2 * q2 - q1 * q1 - q3 * q3);
    Tbn[1][2] = (2.0 * (q2 * q3 - q0 * q1));
    Tbn[2][0] = (2.0 * (q1 * q3 - q0 * q2));
    Tbn[2][1] = (2.0 * (q2 * q3 + q0 * q1));
    Tbn[2][2] = (q0 * q0 + q3 * q3 - q1 * q1 - q2 * q2);

    head = atan2(-Tbn[1][0], Tbn[0][0]);
    pitch = atan2(-Tbn[2][1], Tbn[2][2]);
    roll = asin(Tbn[2][0]);

    if (head < 0.0)
    {
        head += 6.283185307179586;
    }
    rpy[0] = roll;
    rpy[1] = pitch;
    rpy[2] = head;

    return;
}

+(void)quaterToRPYWithFloat:(float)q0
                                   withFloat:(float)q1
                                   withFloat:(float)q2
                                   withFloat:(float)q3
          WithResultSavedInFloatArray:(float*)rpy WithElemCount:(int)elemcount
{
    if (elemcount !=3)
        return;
    float Tbn[3][3] = {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}};
    float head = 0.0f;
    float pitch = 0.0f;
    float roll = 0.0f;
    //float rpy = new float[3];

    Tbn[0][0] = (q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3);
    Tbn[0][1] = (2.0f * (q1 * q2 - q0 * q3));
    Tbn[0][2] = (2.0f * (q1 * q3 + q0 * q2));
    Tbn[1][0] = (2.0f * (q1 * q2 + q0 * q3));
    Tbn[1][1] = (q0 * q0 + q2 * q2 - q1 * q1 - q3 * q3);
    Tbn[1][2] = (2.0f * (q2 * q3 - q0 * q1));
    Tbn[2][0] = (2.0f * (q1 * q3 - q0 * q2));
    Tbn[2][1] = (2.0f * (q2 * q3 + q0 * q1));
    Tbn[2][2] = (q0 * q0 + q3 * q3 - q1 * q1 - q2 * q2);

    head = (float)atan2(-Tbn[1][0], Tbn[0][0]);
    pitch = (float)atan2(-Tbn[2][1], Tbn[2][2]);
    roll = (float)asin(Tbn[2][0]);

    if (head < 0.0f)
    {
        head = (float)(head + 6.283185307179586);
    }
    rpy[0] = roll;
    rpy[1] = pitch;
    rpy[2] = head;

    return;
}























@end
