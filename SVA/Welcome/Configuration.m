//
//  Configuration.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import "Configuration.h"
@implementation Configuration
 static long SVMTrainRate = 60;
 static int scenarioFlag = 2;
 static int WifiAdjFlag = 0;//修改前=3
 static int inertiaFlag = 0;
 static int randomFlag = 0;
 static int angleOfYAxis = 215;
 static int gridFlag = 0;
 static NSMutableArray * apNum;
 static int samplingFrequency = 50;
 static float magVTh = 0.8F;
 static float magFiTh = 0.6108652F;
 static int magAdjFlag = 1;
 static int magAdjPerSecFlag = 1;
 static int paticleAdjFlag = 1;
 static int UT_TEST = 0;
 static float wifiLocDistTh = 10.0F;
 static float minDisOfMagAdj = 0.3F;
 static float maxDisOfMagAdj = 2.0F;
 static float disOfLastPoint = 1.4F;
 static float MeanVauleDistThns = 20.0F;
 static float MeanVauleDistTh1s = 4.0F;
 static int DistAjustFlag = 0;
 static int LargeAreaResDist = 500;
 static int SmallAreaResDist = 5;
 static int SmallAreaResTime = 10;
 static int LargenumOfMagAdjResult = 3;
 static int SmallnumOfMagAdjResult = 2;
 static int LargeminTimeMagAdj = 5;
 static int SmallminTimeMagAdj = 3;
 static int enhancedResamplingFlag = 1;
 static int onlyWifiFlag = 0;
 static int vFlag = 0;
 static int resetWifiFlag = 1;
 static int resetWifiTimeCntLow = 3;
 static int magInsteadOfWifiFlag = 1;
 static int KNNNum = 3;
 static float magDistanceOfWifi = 3.5F;
 static int resetPdrByVflag = 1;
 static int frozenFlag = 1;
 static int frozenTime = 8;
 static int wifiAlphaFilterFlag = 1;
 static int deltaFiFlag = 1;
 static int eKNNFlag = 0;
 static int LteWifiMode = 1;
 static int wifi_KNN_num = 3;
 static double resetPaticleRadius = 5.0;//修改前2.5
 static int valid_ap_num = 8;
 static int logFlag = 1;
 static double knn_dis_th = 10.0;
 static int floorJudgeThreshold = 3;
 static int floorJudgeByApNumFlag = 1;
 static int trianglelocateFlag = 1;
 static int enhanceBarycentreFlag = 1;
 static int fixPointFlag = 1;
 static int filterFlag = 1;
 static int filter2Flag = 1;
 static int changeXY = 0;
 static NSString *mapName = @"U3";
 static bool OllaAjustFlag = false;
 static bool MagFilterFlag = false;
 static float resetWifiDistance = 5;//距离
 static int resetWifiTimeCntHigh = 8;//连续超过8米的次数
 static NSMutableArray * fingerprintTotalNum;

   
+(void)seSVMTrain:(long)SVMTrain
{
    SVMTrainRate =SVMTrain;
    return ;

}

+(void)setOllaAjustF:(BOOL)OllaAjustF
{
    OllaAjustFlag = OllaAjustF;
    return ;
}
+(void)setMagFilterF:(BOOL)MagFilterF
{
    MagFilterFlag = MagFilterF;
    return ;
}
+(void)setresetWifiDistan:(float)resetWifiDistan
{
    resetWifiDistance = resetWifiDistan;
    return ;
}

+(float)resetWifiDistance
{
    return resetWifiDistance;
}

+(void)setresetWifiTimeCnt:(int)resetWifiTimeCntn
{
    resetWifiTimeCntHigh = resetWifiTimeCntn;
    return ;
}

+(int)resetWifiTimeCntHigh
{
    return resetWifiTimeCntHigh;
}

+(NSMutableArray*)setfingerprintTotal
{
    if(!fingerprintTotalNum)
    {
        fingerprintTotalNum = [[NSMutableArray alloc]init];
    }
    return fingerprintTotalNum;
}


+(void)sescenario:(int)scenario
{
    scenarioFlag = scenario;
    return ;
}


+(void)seWifiAd:(int)WifiAd
{
    WifiAdjFlag = WifiAd;
    return ;
}

+(int)WifiAdjFlag
{
    return WifiAdjFlag;
}

+(void)seinertia:(int)inertia
{
    inertiaFlag = inertia;
    return ;
}
+(int)inertiaFlag
{
    return inertiaFlag;
}
+(void)serandom:(int)random
{
    randomFlag = random;
    return ;
}
+(int)randomFlag
{
    return randomFlag;
}
+(void)seangleOf:(int)angleOf
{
    angleOfYAxis = angleOf;
    return ;
}
+(int)angleOfYAxis
{
    return angleOfYAxis;
}
+(void)segridF:(int)gridF
{
    gridFlag = gridF;
    return ;
}
+(void)sesampling:(int)sampling
{
    samplingFrequency = sampling;
    return ;
}

+(int) samplingFrequency
{
    return samplingFrequency;
}

+(void)semagAdj:(int)magAdj
{
    magAdjFlag = magAdj;
    return ;
}
+(void)semagAdjPer:(int)magAdjPer
{
    magAdjPerSecFlag = magAdjPer;
    return ;
}
+(void)sepaticleAdj:(int)paticleAdj
{
    paticleAdjFlag = paticleAdj;
    return ;
}

+(int)paticleAdjFlag
{
    return paticleAdjFlag;
}

+(void)seUT_TES:(int)UT_TES
{
    UT_TEST = UT_TES;
    return ;
}

+(void)semagVT:(float)magVT
{
    magVTh = magVT;
    return ;
}
+(void)semagFiT:(float)magFiT
{
    magFiTh = magFiT;
    return ;
}
+(void)sewifiLocDi:(float)wifiLocDi
{
    wifiLocDistTh = wifiLocDi;
    return ;
}
+(void)seminDisOfMag:(float)minDisOfMag
{
    minDisOfMagAdj = minDisOfMag;
    return ;
}

+(void)semaxDisOfMag:(float)maxDisOfMag
{
    maxDisOfMagAdj = maxDisOfMag;
    return ;
}

+(void)sedisOfLast:(float)disOfLast
{
    disOfLastPoint = disOfLast;
    return ;
}

+(void)seMeanVauleDistThn:(float)MeanVauleDistThn
{
    MeanVauleDistThns = MeanVauleDistThn;
    return ;
}


+(void)seMeanVauleDistTh1:(float)MeanVauleDistTh1
{
    MeanVauleDistTh1s = MeanVauleDistTh1;
    return ;
}


+(void)seDistAjustF:(int)DistAjustF
{
    DistAjustFlag = DistAjustF;
    return ;
}
+(void)seLargeAreaRes:(int)LargeAreaRes
{
    LargeAreaResDist = LargeAreaRes;
    return ;
}

+(void)seSmallAreaRes:(int)SmallAreaRes
{
    SmallAreaResDist = SmallAreaRes;
    return ;
}

+(void)seSmallAreaRest:(int)SmallAreaRest
{
    SmallAreaResTime = SmallAreaRest;
    return ;
}

+(void)seLargenumOfMagAdjR:(int)LargenumOfMagAdjR
{
    LargenumOfMagAdjResult = LargenumOfMagAdjR;
    return ;
}

+(void)seSmallnumOfMagAdjRes:(int)SmallnumOfMagAdjRes
{
    SmallnumOfMagAdjResult = SmallnumOfMagAdjRes;
    return ;
}

+(void)seLargeminTimeMa:(int)LargeminTimeMa
{
    LargeminTimeMagAdj = LargeminTimeMa;
    return ;
}

+(int)LargeminTimeMagAdj
{
    return LargeminTimeMagAdj;
}

+(void)seSmallminTimeMag:(int) SmallminTimeMag
{
    SmallminTimeMagAdj =  SmallminTimeMag;
    return ;
}

+(void)seenhancedResampl:(int)enhancedResampl
{
    enhancedResamplingFlag = enhancedResampl;
    return ;
}

+(int)enhancedResamplingFlag
{
    return enhancedResamplingFlag;
}

+(void)seonlyWifiF:(int)onlyWifiF
{
    onlyWifiFlag = onlyWifiF;
    return ;
}

+(void)sevFla:(int)vFla
{
    vFlag = vFla;
    return ;
}

+(void)seresetWifiFla:(int)resetWifiFla
{
    resetWifiFlag = resetWifiFla;
    return ;
}

+(int)resetWifiFlag
{
    return resetWifiFlag;
}

+(void)seresetWifiTimeCn:(int)resetWifiTimeCn
{
    resetWifiTimeCntLow = resetWifiTimeCn;
    return ;
}

+(int)resetWifiTimeCntLow
{
    return resetWifiTimeCntLow;
}

+(void)semagInsteadOfWifi:(int)magInsteadOfWifi
{
    magInsteadOfWifiFlag = magInsteadOfWifi;
    return ;
}
+(void)seKNNNu:(int)KNNNu
{
    KNNNum = KNNNu;
    return ;
}

+(void)semagDistanceOfW:(float)magDistanceOfW
{
    magDistanceOfWifi = magDistanceOfW;
    return ;
}

+(void)seresetPdrByVf:(int)resetPdrByVf
{
    resetPdrByVflag = resetPdrByVf;
    return ;
}

+(void)sefrozenFla:(int)frozenFla
{
    frozenFlag = frozenFla;
    return;
}

+(void)sefrozenTim:(int)frozenTim
{
    frozenTime = frozenTim;
    return ;
}

+(int)frozenTime
{
    return frozenTime;
}
+(void)sewifiAlphaFilterF:(int)wifiAlphaFilterF
{
    wifiAlphaFilterFlag = wifiAlphaFilterF;
    return ;
}

+(void)sedeltaFiFla:(int)deltaFiFla
{
    deltaFiFlag = deltaFiFla;
    return ;
}
+(int)deltaFiFlag
{
    return deltaFiFlag;
}

+(void)seeKNNFla:(int)eKNNFla
{
    eKNNFlag = eKNNFla;
    return ;
}

+(int)eKNNFlag
{
    return eKNNFlag;
}

+(void)seLteWifiMod:(int)LteWifiMod
{
    LteWifiMode = LteWifiMod;
    return ;
}

+(void)sewifi_KNN_nu:(int)wifi_KNN_nu
{
    wifi_KNN_num = wifi_KNN_nu;
    return ;
}

+(void)seresetPaticleRadiu:(double)resetPaticleRadiu
{
    resetPaticleRadius = resetPaticleRadiu;
    return ;
}

+(double)resetPaticleRadius
{
    return resetPaticleRadius;
}

+(void)sevalid_ap_nu:(int)valid_ap_nu
{
    valid_ap_num = valid_ap_nu;
    return ;
}

+(void)selogFla:(int)logFla
{
    logFlag = logFla;
    return ;
}

+(void)seknn_dis_t:(double)knn_dis_t
{
    knn_dis_th = knn_dis_t;
    return ;
}

+(void)sefloorJudgeThreshol:(int)floorJudgeThreshol
{
    floorJudgeThreshold = floorJudgeThreshol;
    return ;
}

+(void)sefloorJudgeByApNumFla:(int)floorJudgeByApNumFla
{
    floorJudgeByApNumFlag = floorJudgeByApNumFla;
    return ;
}

+(void)setrianglelocateFla:(int)trianglelocateFla
{
    trianglelocateFlag = trianglelocateFla;
    return ;
}

+(void)seenhanceBarycentreFla:(int)enhanceBarycentreFla
{
    enhanceBarycentreFlag = enhanceBarycentreFla;
    return ;
}

+(void)sefixPointFla:(int)fixPointFla
{
    fixPointFlag = fixPointFla;
    return ;
}

+(void)sefilterFla:(int)filterFla
{
    filterFlag = filterFla;
    return ;
}

+(void)sefilter2Fla:(int)filter2Fla
{
    filter2Flag = filter2Fla;
    return ;
}

+(void)sechangeX:(int)changeX
{
    changeXY = changeX;
    return ;
}

+(int)changeXY
{
    return changeXY;
}

+(NSString*)semapNam
{
    if(mapName)
    {
        mapName = [[NSString alloc]init];
    }
    return mapName;
}

@end
