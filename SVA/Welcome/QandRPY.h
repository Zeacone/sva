//
//  QandRPY.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QandRPY : NSObject
+(void)getRPY0:(double*)rpy
    WithDouble:(double)ax
    withDouble:(double)ay
    withDouble:(double)az
    withDouble:(double)mx
    withDouble:(double)my
    withDouble:(double)mz;

+ (void)getRPY0:(float*)rpy
      WithFloat:(float)ax
      withFloat:(float)ay
      withFloat:(float)az
      withFloat:(float)mx
      withFloat:(float)my
      withFloat:(float)mz;


+(void)getQuater0DoubleArray:(double*)Quat WithElemCout:(int)lenOfQuat
              WithFloatArray:(double*)RPY WithElemCount:(int)lenOfRPY;


+(void)getQuater0FloatArray:(float*)Quat WithElemCout:(int)lenOfQuat
             WithFloatArray:(float*)RPY WithElemCount:(int)lenOfRPY;

+(void)quaterToRPYWithDouble:(double)q0
                  withDouble:(double)q1
                  withDouble:(double)q2
                  withDouble:(double)q3
 WithResultSavedInFloatArray:(double*)rpy WithElemCount:(int)elemcount;
+(void)quaterToRPYWithFloat:(float)q0
                  withFloat:(float)q1
                  withFloat:(float)q2
                  withFloat:(float)q3
WithResultSavedInFloatArray:(float*)rpy WithElemCount:(int)elemcount;















@end
