//
//  LeadViewController.m
//  SVA
//
//  Created by XuZongCheng on 15/12/24.
//  Copyright © 2015年 huawei. All rights reserved.
//

#import "SVAWelcomeViewController.h"
#import "ViewController.h"

#define WIDTH self.view.bounds.size.width
#define HEIGHT self.view.bounds.size.height

@interface SVAWelcomeViewController ()
{
    
    UILabel * label;
}
@property (nonatomic, strong)UIView * contnerView;
@end

@implementation SVAWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createView];
}

-(void)createView
{
    
    _scView =[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];

    _scView.backgroundColor= [UIColor whiteColor];
    _scView.contentSize = CGSizeMake(WIDTH*2,HEIGHT);
    _scView.pagingEnabled =YES;
    _scView.bounces =NO;
    

    [self.view addSubview:_scView];
    [self agree];
   
    
    
}

-(void)agree {
    
       [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
    _contnerView.hidden = YES;
    UIImageView * imageview1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    

    imageview1.image = [UIImage imageNamed:@"guide01"];
    [_scView addSubview:imageview1];
    
    UIImageView * imageview2 = [[UIImageView alloc]initWithFrame:CGRectMake(WIDTH, 0, WIDTH, HEIGHT)];
     
    imageview2.image = [UIImage imageNamed:@"guide002"];
    [_scView addSubview:imageview2];
    
    
     UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(2*WIDTH-WIDTH*3/4, HEIGHT*5/6, WIDTH/2, 40)];
    button.layer.cornerRadius = 5;
    [button addTarget:self action:@selector(gotonext) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor blueColor];
    [button setTitle:@"start" forState:UIControlStateNormal];
    button.tintColor = [UIColor whiteColor];
    [_scView addSubview:button];
}
-(void)disAgree
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"everLaunched"];
        exit(0);
    
}

-(void)gotonext
{
    ViewController * vc = [[ViewController alloc]init];
    UINavigationController * nc = [[UINavigationController alloc]initWithRootViewController:vc];
    self.view.window.rootViewController = nc;
    [self.navigationController pushViewController:vc animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
