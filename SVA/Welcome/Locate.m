//
//  Locate.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//


#import "Locate.h"
#import "Gesture.h"
#import "StaticData.h"
#import "Filter.h"
#import "Statistics.h"
#import "QandRPY.h"
#import "Fusion.h"
#import "Configuration.h"
#import "Networks.h"
#import "GlobalConfig.h"
#import "MatrixOperate.h"


@implementation Locate


//- (instancetype) init
//{
//    self = [super init];
//
//    if (self)
//    {
//        filterLength = 4;
//       traceFlag=true;
//    }
    
//    return self;
//}

+ (instancetype)sharedLocate {
    static Locate *locate = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        locate = [Locate new];
        locate.filterLength = 4;
        locate.traceFlag = YES;
    });
    return locate;
}


-(void) locate: (float*) dataMems WithRow:(int)rowOfDataMems WithCol:(int)colOfDataMems WithResultSavedIn:(float*)xylocation
{
    if (self.traceFlag) {
        NSDate *date = [NSDate date];
        NSLog(@"collect exec time: locate start %@", date);
    }
    int fdata = 50;
    Gesture *gesture = [[Gesture alloc] init];
    float RPYforDecide[3] = {0.0f, 0.0f, 0.0f};
    [gesture attitudeDecide:RPYforDecide Of:dataMems WithRow:rowOfDataMems WithCol:colOfDataMems];
    
    [StaticData rollforAttitude_AddObject:RPYforDecide[0]];
    [StaticData pitchforAttitude_AddObject:RPYforDecide[1]];
    [StaticData headforAttitude_AddObject:RPYforDecide[2]];
    
    if ((int)[ [StaticData serollforAttitude] count] > 2)
    {
        [StaticData rollforAttitude_RemoveObjectAtIndex:0];
        [StaticData pitchforAttitude_RemoveObjectAtIndex:0];
        [StaticData headforAttitude_AddObject:0];
    }
    int ii;
    if ([StaticData flagforDecide] == 1)
    {
        for (int i = 0; i < fdata; i++) {
            [StaticData MODE_AddObject:0.0f];
        }
        for (ii = 0; ii < rowOfDataMems; ii++) {
            [StaticData  sedataMemsMean:[gesture meanFloat:(dataMems+ii*10) WithElemCount:50]
                                AtIndex: ii];
        }
        [StaticData seflagforDecide: [StaticData flagforDecide] + 1];
    }
    else {
    }
    
    //float xylocation[2] = {0.0f, 0.0f};
    float velSecond = 0.0f;
    int len = 50;
    int halfLen = len / 2 - 1;
             
    if ([StaticData firstSecond])
    {
        [StaticData seQ_VectorExis: false];
       // NSLog(@"dataMems[0].length :%i", colOfDataMems);
        @try
        {
            for (int i = 25; i < colOfDataMems; i++)
            {
                float Mag_Temp[3] = {0.0f, 0.0f, 0.0f};
                Mag_Temp[0] = *(dataMems + 6 * colOfDataMems + i);
                Mag_Temp[1] = *(dataMems + 7 * colOfDataMems + i);
                Mag_Temp[2] = *(dataMems + 8 * colOfDataMems + i);
                //NSMutableArray *Mag_Temp_Temp = [NSMutableArray arrayWithCapacity:3];
                //for(i = 0; i < 3; i++)
                //{
                //    [Mag_Temp_Temp addObject:[NSNumber numberWithFloat:Mag_Temp[i]]];
               // }
                //[[StaticData seMag_Init_Temp] addObject: Mag_Temp_Temp] ;
                //Mag_Temp_Temp = nil;
                [StaticData Mag_Init_Temp_AddObjectArray:Mag_Temp WithElemCount:3];
            }
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@ %@", [exception name], [exception reason]);
        }
        
        [StaticData sefirstSeco:false ];
        [StaticData seflagFie:[StaticData flagFielt] +1];

        float dataMemsTemp[rowOfDataMems][colOfDataMems + 2];
        for(int i = 0; i < rowOfDataMems; i++)
        {
            for(int j = 0; j < colOfDataMems + 2; j++)
            {
                dataMemsTemp[i][j] = 0.0f;
            }
        }
    
        
        for (int i = 0; i < rowOfDataMems; i++) {
            for (int j = 2; j < colOfDataMems + 2  ; j++) {
                dataMemsTemp[i][j] = *(dataMems + i*colOfDataMems +(j - 2));
            }
        }

        float axmedianFilter[colOfDataMems];
        float aymedianFilter[colOfDataMems];
        float azmedianFilter[colOfDataMems];
        float gxmedianFilter[colOfDataMems];
        float gymedianFilter[colOfDataMems];
        float gzmedianFilter[colOfDataMems];
        float mxmedianFilter[colOfDataMems];
        float mymedianFilter[colOfDataMems];
        float mzmedianFilter[colOfDataMems];
        for(int i = 0; i < colOfDataMems; i++)
        {
            axmedianFilter[i] = 0.0f;
            aymedianFilter[i] = 0.0f;
            azmedianFilter[i] = 0.0f;
            gxmedianFilter[i] = 0.0f;
            gymedianFilter[i] = 0.0f;
            gzmedianFilter[i] = 0.0f;
            mxmedianFilter[i] = 0.0f;
            mymedianFilter[i] = 0.0f;
            mzmedianFilter[i] = 0.0f;
        }
        
        
        float axmedianFilterTemp[colOfDataMems+2];
        float aymedianFilterTemp[colOfDataMems+2];
        float azmedianFilterTemp[colOfDataMems+2];
        float gxmedianFilterTemp[colOfDataMems+2];
        float gymedianFilterTemp[colOfDataMems+2];
        float gzmedianFilterTemp[colOfDataMems+2];
        float mxmedianFilterTemp[colOfDataMems+2];
        float mymedianFilterTemp[colOfDataMems+2];
        float mzmedianFilterTemp[colOfDataMems+2];
        for(int i = 0; i < colOfDataMems+2; i++)
        {
            axmedianFilterTemp[i] = 0.0f;
            aymedianFilterTemp[i] = 0.0f;
            azmedianFilterTemp[i] = 0.0f;
            gxmedianFilterTemp[i] = 0.0f;
            gymedianFilterTemp[i] = 0.0f;
            gzmedianFilterTemp[i] = 0.0f;
            mxmedianFilterTemp[i] = 0.0f;
            mymedianFilterTemp[i] = 0.0f;
            mzmedianFilterTemp[i] = 0.0f;
        }
            
        
        
        [Filter medianFilterWithFloatArray:dataMemsTemp[0] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:axmedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[1] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:aymedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[2] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:azmedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[3] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:gxmedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[4] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:gymedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[5] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:gzmedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[6] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:mxmedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[7] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:mymedianFilterTemp WithElemCount:(colOfDataMems+2)];
        [Filter medianFilterWithFloatArray:dataMemsTemp[8] WithElemCount:(colOfDataMems+2)
                                   withInt:5
                    WithResultSavedInArray:mzmedianFilterTemp WithElemCount:(colOfDataMems+2)];
        

        for (int i = 0; i < 50; i++) {
            axmedianFilter[i] = axmedianFilterTemp[(2 + i)];
            aymedianFilter[i] = aymedianFilterTemp[(2 + i)];
            azmedianFilter[i] = azmedianFilterTemp[(2 + i)];
            gxmedianFilter[i] = gxmedianFilterTemp[(2 + i)];
            gymedianFilter[i] = gymedianFilterTemp[(2 + i)];
            gzmedianFilter[i] = gzmedianFilterTemp[(2 + i)];
            mxmedianFilter[i] = mxmedianFilterTemp[(2 + i)];
            mymedianFilter[i] = mymedianFilterTemp[(2 + i)];
            mzmedianFilter[i] = mzmedianFilterTemp[(2 + i)];
        }
        float axmeanFilter[colOfDataMems];
        float aymeanFilter[colOfDataMems];
        float azmeanFilter[colOfDataMems];
        float gxmeanFilter[colOfDataMems];
        float gymeanFilter[colOfDataMems];
        float gzmeanFilter[colOfDataMems];
        float mxmeanFilter[colOfDataMems];
        float mymeanFilter[colOfDataMems];
        float mzmeanFilter[colOfDataMems];
        
        [Filter meanFilterWithFloatArray:axmedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:axmeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:aymedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:aymeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:azmedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:azmeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:gxmedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:gxmeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:gymedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:gymeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:gzmedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:gzmeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:mxmedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:mxmeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:mymedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:mymeanFilter WithElemCount:colOfDataMems];
        [Filter meanFilterWithFloatArray:mzmedianFilter WithElemCount:colOfDataMems
                                 withInt:5
                       WithResultSavedIn:mzmeanFilter WithElemCount:colOfDataMems];

        float sumG = 0.0;
        for (int i = 0; i <colOfDataMems - 4; i++)
        {
            sumG = sumG +
                    (float)sqrt(pow(axmeanFilter[i], 2.0) +
                                pow(aymeanFilter[i], 2.0) +
                                pow(azmeanFilter[i], 2.0));
   //         NSLog(@"%f", sumG);
        }
        int g = (int)(sumG / (colOfDataMems - 4) * 100.0f);
        [StaticData seg: (float)(g / 100.0)];
        
        float accTemp = 0.0;
        for (int i = 0; i < 46; i++) {
            [StaticData axFielt_AddObject:axmeanFilter[i]];
            [StaticData ayFielt_AddObject:aymeanFilter[i]];
            [StaticData azFielt_AddObject:azmeanFilter[i]];
            [StaticData gxFielt_AddObject:gxmeanFilter[i]];
            [StaticData gyFielt_AddObject:gymeanFilter[i]];
            [StaticData gzFielt_AddObject:gzmeanFilter[i]];
            [StaticData mxFielt_AddObject:mxmeanFilter[i]];
            [StaticData myFielt_AddObject:mymeanFilter[i]];
            [StaticData mzFielt_AddObject:mzmeanFilter[i]];
            
            accTemp = (float)sqrt(pow(axmeanFilter[i], 2.0) +
                                  pow(aymeanFilter[i], 2.0) +
                                  pow(azmeanFilter[i], 2.0));
            [[StaticData seacc4Lo] addObject: [NSNumber numberWithFloat:accTemp]];
        }
        
        for (int i = 0; i < 4; i++) {
            [[StaticData seax1 ] addObject: [NSNumber numberWithFloat:dataMemsTemp[0][(48 + i)]]];
            [[StaticData seay1] addObject: [NSNumber numberWithFloat:dataMemsTemp[1][(48 + i)]]];
            [[StaticData seaz]  addObject: [NSNumber numberWithFloat:dataMemsTemp[2][(48 + i)]]];
            [[StaticData segx1] addObject: [NSNumber numberWithFloat:dataMemsTemp[3][(48 + i)]]];
            [[StaticData segy1] addObject: [NSNumber numberWithFloat:dataMemsTemp[4][(48 + i)]]];
            [[StaticData segz1] addObject: [NSNumber numberWithFloat:dataMemsTemp[5][(48 + i)]]];
            [[StaticData semx1] addObject: [NSNumber numberWithFloat:dataMemsTemp[6][(48 + i)]]];
            [[StaticData semy1] addObject: [NSNumber numberWithFloat:dataMemsTemp[7][(48 + i)]]];
            [[StaticData semz1] addObject: [NSNumber numberWithFloat:dataMemsTemp[8][(48 + i)]]];
        }
        
        for (int i = 0; i < 4; i++) {
            [[StaticData seaxMean1] addObject:[NSNumber numberWithFloat:axmedianFilter[(44 + i)]]];
            [[StaticData seayMean1] addObject:[NSNumber numberWithFloat:aymedianFilter[(44 + i)]]];
            [[StaticData seazMean1] addObject:[NSNumber numberWithFloat:azmedianFilter[(44 + i)]]];
            [[StaticData segxMean1] addObject:[NSNumber numberWithFloat:gxmedianFilter[(44 + i)]]];
            [[StaticData segyMean1] addObject:[NSNumber numberWithFloat:gymedianFilter[(44 + i)]]];
            [[StaticData segzMean1] addObject:[NSNumber numberWithFloat:gzmedianFilter[(44 + i)]]];
            [[StaticData semxMean1] addObject:[NSNumber numberWithFloat:mxmedianFilter[(44 + i)]]];
            [[StaticData semyMean1] addObject:[NSNumber numberWithFloat:mymedianFilter[(44 + i)]]];
            [[StaticData semzMean1] addObject:[NSNumber numberWithFloat:mzmedianFilter[(44 + i)]]];
        }
        
        float accLowTemp[halfLen + 1];
        for(int i = 0; i < halfLen + 1; i++)
        {
            accLowTemp[i] = 0.0f;
        }
        float accLow = 0.0f;
        while ((int)[[StaticData seacc4Lo] count] > halfLen) {
            for (int i = 0; i < halfLen + 1; i++) {
                accLowTemp[i] = ([[StaticData acc4Low][i] floatValue] - [StaticData G]) ;
            }
            
            accLow = [Filter hanningFIRWithFloatArray:accLowTemp WithElemCount:halfLen + 1];
            [[StaticData seyfielt1] addObject: [NSNumber numberWithDouble: accLow] ];
            [[StaticData seacc4Lo] removeObjectAtIndex:0];
        }
        
    }
    else
    {
        @try
        {
            for (NSInteger i = 0; i < colOfDataMems; i++) {
                float Mag_Temp[3] = {0.0f, 0.0f, 0.0f};
                Mag_Temp[0] = *(dataMems + 6 * colOfDataMems + i);
                Mag_Temp[1] = *(dataMems + 7 * colOfDataMems + i);
                Mag_Temp[2] = *(dataMems + 8 * colOfDataMems + i);
                
                NSMutableArray *Mag_Temp_Temp = [NSMutableArray array];
                [Mag_Temp_Temp addObject:[NSNumber numberWithFloat:Mag_Temp[0]]];
                [Mag_Temp_Temp addObject:[NSNumber numberWithFloat:Mag_Temp[1]]];
                [Mag_Temp_Temp addObject:[NSNumber numberWithFloat:Mag_Temp[2]]];

                [StaticData seMag_Init_TempAddObject:Mag_Temp_Temp];
                //[[StaticData seMag_Init_Temp] addObject: Mag_Temp_Temp];
                Mag_Temp_Temp = nil;
            }
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@ %@", [exception name], [exception reason]);
        }

        if ([StaticData flagFielt] == 1) {
            [StaticData seflagFie: ([StaticData flagFielt]+1)];
            [StaticData setwoSeco:true];
        }
        
        float axmeanFieltAdd[54];
        float aymeanFieltAdd[54];
        float azmeanFieltAdd[54];
        float gxmeanFieltAdd[54];
        float gymeanFieltAdd[54];
        float gzmeanFieltAdd[54];
        float mxmeanFieltAdd[54];
        float mymeanFieltAdd[54];
        float mzmeanFieltAdd[54];
        for(int i = 0; i < 54; i++)
        {
            axmeanFieltAdd[i] = 0.0f;
            aymeanFieltAdd[i] = 0.0f;
            azmeanFieltAdd[i] = 0.0f;
            gxmeanFieltAdd[i] = 0.0f;
            gymeanFieltAdd[i] = 0.0f;
            gzmeanFieltAdd[i] = 0.0f;
            mxmeanFieltAdd[i] = 0.0f;
            mymeanFieltAdd[i] = 0.0f;
            mzmeanFieltAdd[i] = 0.0f;
        }
        
        float dataMemsAdd[10][54];
        for(int i = 0; i < 10; i++)
        {
            for(int j = 0; j < 54; j++)
            {
                dataMemsAdd[i][j] = 0.0f;
            }
        }

        
        for (int j = 0; j < 4; j++) {
            dataMemsAdd[0][j] = [[StaticData ax1][j] floatValue];
            dataMemsAdd[1][j] = [[StaticData ay1][j] floatValue];
            dataMemsAdd[2][j] = [[StaticData az1][j] floatValue];
            dataMemsAdd[3][j] = [[StaticData gx1][j] floatValue];
            dataMemsAdd[4][j] = [[StaticData gy1][j] floatValue];
            dataMemsAdd[5][j] = [[StaticData gz1][j] floatValue];
            dataMemsAdd[6][j] = [[StaticData mx1][j] floatValue];
            dataMemsAdd[7][j] = [[StaticData my1][j] floatValue];
            dataMemsAdd[8][j] = [[StaticData mz1][j] floatValue];
        }
        
        for (int i = 0; i < 4; i++) {
            [[StaticData seax1] removeObjectAtIndex:0];
            [[StaticData seay1] removeObjectAtIndex:0];
            [[StaticData seaz] removeObjectAtIndex:0];
            [[StaticData segx1] removeObjectAtIndex:0];
            [[StaticData segy1] removeObjectAtIndex:0];
            [[StaticData segz1] removeObjectAtIndex:0];
            [[StaticData semx1] removeObjectAtIndex:0];
            [[StaticData semy1] removeObjectAtIndex:0];
            [[StaticData semz1] removeObjectAtIndex:0];
        }
        
        for (int i = 0; i < rowOfDataMems; i++) {
            for (int j = 0; j < colOfDataMems; j++) {
                dataMemsAdd[i][(j + 4)] = *(dataMems + i *colOfDataMems + j);
            }
        }
        
        float axmedianFilter[colOfDataMems];
        float aymedianFilter[colOfDataMems];
        float azmedianFilter[colOfDataMems];
        float gxmedianFilter[colOfDataMems];
        float gymedianFilter[colOfDataMems];
        float gzmedianFilter[colOfDataMems];
        float mxmedianFilter[colOfDataMems];
        float mymedianFilter[colOfDataMems];
        float mzmedianFilter[colOfDataMems];
        
        [Filter medianFilterWithFloatArray:dataMemsAdd[0] WithElemCount:54
                                                     withInt:5
                    WithResultSavedInArray:axmedianFilter WithElemCount:54];
        [Filter medianFilterWithFloatArray:dataMemsAdd[1] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:aymedianFilter WithElemCount:54];
        [Filter medianFilterWithFloatArray:dataMemsAdd[2] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:azmedianFilter WithElemCount:54];
    
        [Filter medianFilterWithFloatArray:dataMemsAdd[3] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:gxmedianFilter WithElemCount:54];
        [Filter medianFilterWithFloatArray:dataMemsAdd[4] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:gymedianFilter WithElemCount:54];
        [Filter medianFilterWithFloatArray:dataMemsAdd[5] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:gzmedianFilter WithElemCount:54];
    
        [Filter medianFilterWithFloatArray:dataMemsAdd[6] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:mxmedianFilter WithElemCount:54];
        [Filter medianFilterWithFloatArray:dataMemsAdd[7] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:mymedianFilter WithElemCount:54];
        [Filter medianFilterWithFloatArray:dataMemsAdd[8] WithElemCount:54
                                   withInt:5
                    WithResultSavedInArray:mzmedianFilter WithElemCount:54];
    
        for (int i = 0; i < 4; i++) {
            axmeanFieltAdd[i] = [[StaticData axMean1][i] floatValue];
            aymeanFieltAdd[i] = [[StaticData ayMean1][i] floatValue];
            azmeanFieltAdd[i] = [[StaticData azMean1][i] floatValue];
            gxmeanFieltAdd[i] = [[StaticData gxMean1][i] floatValue];
            gymeanFieltAdd[i] = [[StaticData gyMean1][i] floatValue];
            gzmeanFieltAdd[i] = [[StaticData gzMean1][i] floatValue];
            mxmeanFieltAdd[i] = [[StaticData mxMean1][i] floatValue];
            mymeanFieltAdd[i] = [[StaticData myMean1][i] floatValue];
            mzmeanFieltAdd[i] = [[StaticData mzMean1][i] floatValue];
        }
        
        for (int i = 0; i < 4; i++) {
            [[StaticData seaxMean1] removeObjectAtIndex:0];
            [[StaticData seayMean1] removeObjectAtIndex:0];
            [[StaticData seazMean1] removeObjectAtIndex:0];
            [[StaticData segxMean1] removeObjectAtIndex:0];
            [[StaticData segyMean1] removeObjectAtIndex:0];
            [[StaticData segzMean1] removeObjectAtIndex:0];
            [[StaticData semxMean1] removeObjectAtIndex:0];
            [[StaticData semyMean1] removeObjectAtIndex:0];
            [[StaticData semzMean1] removeObjectAtIndex:0];
        }
        
        for (int i = 0; i < sizeof(axmedianFilter)/sizeof(axmedianFilter[0]) - 4; i++) {
            axmeanFieltAdd[(4 + i)] = axmedianFilter[(2 + i)];
            aymeanFieltAdd[(4 + i)] = aymedianFilter[(2 + i)];
            azmeanFieltAdd[(4 + i)] = azmedianFilter[(2 + i)];
            gxmeanFieltAdd[(4 + i)] = gxmedianFilter[(2 + i)];
            gymeanFieltAdd[(4 + i)] = gymedianFilter[(2 + i)];
            gzmeanFieltAdd[(4 + i)] = gzmedianFilter[(2 + i)];
            mxmeanFieltAdd[(4 + i)] = mxmedianFilter[(2 + i)];
            mymeanFieltAdd[(4 + i)] = mymedianFilter[(2 + i)];
            mzmeanFieltAdd[(4 + i)] = mzmedianFilter[(2 + i)];
        }
        
        int elemcount = (sizeof(axmeanFieltAdd))/sizeof(axmeanFieltAdd[0]);
        float axmeanFilter[elemcount] ;
        float aymeanFilter[elemcount] ;
        float azmeanFilter[elemcount] ;
        float gxmeanFilter[elemcount] ;
        float gymeanFilter[elemcount] ;
        float gzmeanFilter[elemcount] ;
        float mxmeanFilter[elemcount] ;
        float mymeanFilter[elemcount] ;
        float mzmeanFilter[elemcount] ;
        
        [Filter meanFilterWithFloatArray:axmeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:axmeanFilter WithElemCount:elemcount];
        
        [Filter meanFilterWithFloatArray:aymeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:aymeanFilter WithElemCount:elemcount];

        
        [Filter meanFilterWithFloatArray:azmeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:azmeanFilter WithElemCount:elemcount];

        
        [Filter meanFilterWithFloatArray:gxmeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:gxmeanFilter WithElemCount:elemcount];

        
        [Filter meanFilterWithFloatArray:gymeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:gymeanFilter WithElemCount:elemcount];

        
        [Filter meanFilterWithFloatArray:gzmeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:gzmeanFilter WithElemCount:elemcount];

        
        [Filter meanFilterWithFloatArray:mxmeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:mxmeanFilter WithElemCount:elemcount];

        
        [Filter meanFilterWithFloatArray:mymeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:mymeanFilter WithElemCount:elemcount];

        
        [Filter meanFilterWithFloatArray:mzmeanFieltAdd WithElemCount:elemcount
                                 withInt:5
                       WithResultSavedIn:mzmeanFilter WithElemCount:elemcount];

        float accTemp = 0.0;
        for (int i = 0; i < 50; i++) {
            [StaticData axFielt_AddObject:axmeanFilter[(2 + i)]];
            [StaticData ayFielt_AddObject:aymeanFilter[(2 + i)]];
            [StaticData azFielt_AddObject:azmeanFilter[(2 + i)]];
            [StaticData gxFielt_AddObject:gxmeanFilter[(2 + i)]];
            [StaticData gyFielt_AddObject:gymeanFilter[(2 + i)]];
            [StaticData gzFielt_AddObject:gzmeanFilter[(2 + i)]];
            //NSLog(@"%f", gzmeanFilter[(2+i)]);
            [StaticData mxFielt_AddObject:mxmeanFilter[(2 + i)]];
            [StaticData myFielt_AddObject:mymeanFilter[(2 + i)]];
            [StaticData mzFielt_AddObject:mzmeanFilter[(2 + i)]];
            
            accTemp = (float)sqrt(pow(axmeanFilter[(2 + i)], 2.0) +
                                  pow(aymeanFilter[(2 + i)], 2.0) +
                                  pow(azmeanFilter[(2 + i)], 2.0));
            
            [[StaticData seacc4Lo] addObject: [NSNumber numberWithFloat:accTemp] ];
        }
        
        for (int i = 0; i < 4; i++) {
            [[StaticData seax1] addObject:[NSNumber numberWithFloat:dataMemsAdd[0][(50 + i)]] ];
            [[StaticData seay1] addObject:[NSNumber numberWithFloat:dataMemsAdd[1][(50+ i)]] ];
            [[StaticData seaz] addObject:[NSNumber numberWithFloat:dataMemsAdd[2][(50 + i)]] ];
            [[StaticData segx1] addObject:[NSNumber numberWithFloat:dataMemsAdd[3][(50 + i)]] ];
            [[StaticData segy1] addObject:[NSNumber numberWithFloat:dataMemsAdd[4][(50 + i)]] ];
            [[StaticData segz1] addObject:[NSNumber numberWithFloat:dataMemsAdd[5][(50 + i)]] ];
            [[StaticData semx1] addObject:[NSNumber numberWithFloat:dataMemsAdd[6][(50 + i)]] ];
            [[StaticData semy1] addObject:[NSNumber numberWithFloat:dataMemsAdd[7][(50 + i)]] ];
            [[StaticData semz1] addObject: [NSNumber numberWithFloat:dataMemsAdd[8][(50 + i)]] ];
        }

        for (int i = 0; i < 4; i++) {
            [[StaticData seaxMean1] addObject:[NSNumber numberWithFloat:axmeanFieltAdd[(50 + i)]]];
            [[StaticData seayMean1] addObject:[NSNumber numberWithFloat:aymeanFieltAdd[(50 + i)]]];
            [[StaticData seazMean1] addObject:[NSNumber numberWithFloat:azmeanFieltAdd[(50 + i)]]];
            [[StaticData segxMean1] addObject:[NSNumber numberWithFloat:gxmeanFieltAdd[(50 + i)]]];
            [[StaticData segyMean1] addObject:[NSNumber numberWithFloat:gymeanFieltAdd[(50 + i)]]];
            [[StaticData segzMean1] addObject:[NSNumber numberWithFloat:gzmeanFieltAdd[(50 + i)]]];
            [[StaticData semxMean1] addObject:[NSNumber numberWithFloat:mxmeanFieltAdd[(50 + i)]]];
            [[StaticData semyMean1] addObject:[NSNumber numberWithFloat:mymeanFieltAdd[(50 + i)]]];
            [[StaticData semzMean1] addObject:[NSNumber numberWithFloat:mzmeanFieltAdd[(50 + i)]]];
        }
        
       
        float accLowTemp[halfLen + 1];
        float accLow = 0.0;
        while ((int)[[StaticData seacc4Lo] count] > halfLen)
        {
            for (int i = 0; i < halfLen + 1; i++) {
                accLowTemp[i] = [[StaticData acc4Low][i] floatValue] - [StaticData G];
            }
            
            accLow = [Filter hanningFIRWithFloatArray:accLowTemp WithElemCount:(halfLen+1)];
            [[StaticData seyfielt1] addObject: [NSNumber numberWithDouble:accLow] ];
            
            [[StaticData seacc4Lo] removeObjectAtIndex:0];
        }
        
    }
    
//    NSLog(@"whilewhilewhilewhile%li", [[StaticData axFielt] count]);
    
    while ((int)[[StaticData axFielt] count] >= 50)
    {
        float data4Ekf[9][50];
        int rowOfData4Ekf = sizeof(data4Ekf)/sizeof(data4Ekf[0]);
        int colOfData4Ekf = sizeof(data4Ekf[0])/sizeof(data4Ekf[0][0]);
       // float data4Ekf[rowOfData4Ekf][colOfData4Ekf];
        
        float resultEKF[6][4]={{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
        float acc[50];
        float gry[50];
        float mag[50];
        float varAcc = 0.0F;
        float varGry = 0.0F;
        float varMag = 0.0F;
        float result[15];
        for(int i = 0; i < rowOfData4Ekf; i++)
        {
            for(int j = 0; j < colOfData4Ekf;  j++)
            {
                data4Ekf[i][j] = 0.0f;
            }
        }
        for(int i = 0; i < 50; i++)
        {
            acc[i] = 0.0f;
            gry[i] = 0.0f;
            mag[i] = 0.0f;
        }
        for(int i = 0; i < 15; i++)
        {
            result[i] = 0.0f;
        }
        

        
        for (int i = 0; i < 50; i++) {
            data4Ekf[0][i] = [StaticData axFieltAtIndex:i];
            data4Ekf[1][i] = [StaticData ayFieltAtIndex:i];
            data4Ekf[2][i] = [StaticData azFieltAtIndex:i];
            data4Ekf[3][i] = [StaticData gxFieltAtIndex:i];
            data4Ekf[4][i] = [StaticData gyFieltAtIndex:i];
            data4Ekf[5][i] = [StaticData gzFieltAtIndex:i];
            data4Ekf[6][i] = [StaticData mxFieltAtIndex:i];
            data4Ekf[7][i] = [StaticData myFieltAtIndex:i];
            data4Ekf[8][i] = [StaticData mzFieltAtIndex:i];
        }
        
        [[StaticData seaxFiel] removeObjectAtIndex:0];
        [[StaticData seayFiel] removeObjectAtIndex:0];
        [[StaticData seazFiel] removeObjectAtIndex:0];
        [[StaticData segxFiel] removeObjectAtIndex:0];
        [[StaticData segyFiel] removeObjectAtIndex:0];
        [[StaticData segzFiel] removeObjectAtIndex:0];
        [[StaticData semxFiel] removeObjectAtIndex:0];
        [[StaticData semyFiel] removeObjectAtIndex:0];
        [[StaticData semzFiel] removeObjectAtIndex:0];
        
        for (int i = 0; i < 50; i++) {
            acc[i] = ((float)sqrt(pow(data4Ekf[0][i], 2.0) +
                                       pow(data4Ekf[1][i], 2.0) +
                                       pow(data4Ekf[2][i], 2.0)));
            gry[i] = ((float)sqrt(pow(data4Ekf[3][i], 2.0) +
                                  pow(data4Ekf[4][i], 2.0) +
                                  pow(data4Ekf[5][i], 2.0)));
           // if(i >= 46)
            //{
            //    NSLog(@"%f %f %f %f", data4Ekf[3][i], data4Ekf[4][i], data4Ekf[5][i],gry[i]);

           // }
            mag[i] = ((float)sqrt(pow(data4Ekf[6][i], 2.0) +
                                  pow(data4Ekf[7][i], 2.0) +
                                  pow(data4Ekf[8][i], 2.0)));
        }
        
        varAcc = [Statistics varFloatArray:acc WithElemCount:50];
        varGry = [Statistics varFloatArray:gry WithElemCount:50];
        varMag = [Statistics varFloatArray:mag WithElemCount:50];
       // NSLog(@"varAcc=%f  VarGry=%f   varMag=%f", varAcc, varGry, varMag);
         
        for (int i = 0; i < rowOfData4Ekf; i++) {
            result[i] = data4Ekf[i][halfLen] ;
        }
        result[9] = acc[halfLen];
        result[10] = gry[halfLen];
        result[11] = mag[halfLen];
        result[12] = varAcc;
        result[13] = varGry;
        result[14] = varMag;
        
        if ([StaticData isFirstHead]){
            [StaticData seisFirstHe:false];
            float RPY0[3] = {0.0f, 0.0f, 0.0f};
            [QandRPY getRPY0: RPY0 WithFloat:result[0] withFloat:result[1] withFloat:result[2]
                   withFloat:result[6] withFloat:result[7] withFloat:result[8]];
            
            float quaterTemp[4] = {0.0f, 0.0f, 0.0f, 0.0f};
            [QandRPY getQuater0FloatArray:quaterTemp WithElemCout:4 WithFloatArray:RPY0 WithElemCount:3];
            [StaticData seq:quaterTemp WithElemCount:4];
        }
        
       // [Fusion fusionEKFFloatArray:result WithResultSavedInFloatArray:&resultEKF[0][0]];
        
        [[StaticData seheadCache1] addObject: [NSNumber numberWithFloat:resultEKF[0][2]]];
        for (int i = 0; i < [StaticData q_count]; i++) {
            [StaticData seq:resultEKF[1][i] AtIndex:i];
        }
        
        [StaticData secoun: (int)resultEKF[0][3]];

        for (int i = 0; i < [StaticData P_RowCount]; i++) {
            for (int j = 0; j < [StaticData P_RowCount]; j++) {
                [StaticData seP:resultEKF[(i + 2)][j] AtRow:i AtCol:j];
            }
        }
        
        [StaticData seQ_VectorExis:true];
    }
    
    if ([StaticData Q_VectorExist]) {
        @try {
            while ((int)[[StaticData  Q_Vector_temp] count] > 0)
            {
                [[StaticData seQ_Vector] addObject:[NSMutableArray arrayWithArray:[StaticData Q_Vector_temp][0]]];
                [[StaticData seMag_Init] addObject:[NSMutableArray arrayWithArray:[StaticData Mag_Init_Temp][0]] ];
                [[StaticData seQ_Vector_te] removeObjectAtIndex:0];
                [[StaticData seMag_Init_Temp] removeObjectAtIndex:0];

                if ((int)[[StaticData Q_Vector] count] > ([Configuration LargeminTimeMagAdj] +3) *
                    [Configuration samplingFrequency]) {
                    [[StaticData seQ_Vector] removeObjectAtIndex:0];
                    [[StaticData seMag_Init] removeObjectAtIndex:0];
                }
            }
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@ %@", [exception name], [exception reason]);
        }
    
    }
    
    if ([StaticData twoSecond])
    {
        [StaticData setwoSeco:false];
        [StaticData semoreSeco:true];
        float headPdr = [[StaticData headCache1][0] floatValue];
        [StaticData seXopt:headPdr AtIndex:3];
        
        [StaticData sefi0: headPdr];
 //       NSLog(@"StaticData.fiStaticData.fi111111=, %f", [StaticData fi]);
        float aMaxTemp = 0.0;

        int temp_up = [StaticData NO_PEAK];
        NSMutableArray *flag_up = [NSMutableArray arrayWithCapacity:5];

        for (int i = 1; i < (int)[[StaticData yfielt] count] - 1; i++)
        {
            if ([[StaticData yfielt][i] floatValue] >= [[StaticData yfielt][i] floatValue])
            {

                if (([[StaticData yfielt][i] floatValue] > [[StaticData yfielt][i + 1] floatValue])
                    && ([[StaticData yfielt][i] floatValue] > [StaticData UP_GATE]))
                {
                    if ((int)[flag_up count] == 0) {
                        [flag_up addObject: [NSNumber numberWithInt:i] ];
    //                    NSLog(@"flag_upflag_upflag_upflag_up1");
                        if (i < [StaticData RATE]){
                            [StaticData sestepNu:([StaticData stepNum] + 1)];
     //                       NSLog(@"step number : %i", [StaticData stepNum]);
                            temp_up = i;
                             aMaxTemp = [[StaticData yfielt][i] floatValue];
                        }
                    }
                    else if (i - [flag_up[((int)[flag_up count] -1)] intValue]  > [StaticData UP_INTERVAL]) {
                        [flag_up addObject: [NSNumber numberWithInt: i]];
        //                NSLog(@"flag_upflag_upflag_upflag_up2");
                        if (i < [StaticData RATE]) {
                            [StaticData sestepNu:([StaticData stepNum] + 1)];
  //                          NSLog(@"step number : %i", [StaticData stepNum]);
                            temp_up = i;
                            aMaxTemp = [[StaticData yfielt][i] floatValue];
                        }
                    }
                }
            }
        }

 //       NSLog(@"[flag_up count]=%li", [flag_up count]);
        if ((int)[flag_up count] == 0 ) {
            velSecond = 0.0;
  //          NSLog(@"111111111111111111");
        }
        else if ((int)[flag_up count] == 1) {
            if ([flag_up[((int)[flag_up count]-1)] intValue] < [StaticData RATE])
            {
                velSecond = ([StaticData RATE] - [flag_up[0] intValue]) * 0.68 /[StaticData RATE];
 //               NSLog(@"22222222222222222");
            }
            else {
                velSecond = 0.0;
  //              NSLog(@"333333333333333333");
            }

        }
        else
        {
            Networks *networks = [[Networks alloc] init];
            
            float dyStepLen = 0.0;
            for (NSInteger j = 0; j < (int)[flag_up count] - 1; j++)
            {
                float SF = 50.0 / ([flag_up[j + 1] intValue] - [flag_up[j] intValue]);
                
                NSArray *p = @[ [NSNumber numberWithFloat: SF], [NSNumber numberWithFloat:[StaticData H]]];
                float c = [networks bpNet: p];

                float aMin = [[StaticData yfielt][[flag_up[j] intValue]] floatValue];

                for (int k = [flag_up[j] intValue]; k < [flag_up[j + 1] intValue]; k++) {
                    if (aMin > [[StaticData yfielt][k] floatValue] ) {
                        aMin = [[StaticData yfielt][k] floatValue];
                    }
                }

                float aMax = [[StaticData yfielt][[flag_up[j] intValue]] floatValue];

                dyStepLen = dyStepLen + (float)(c *  powf(aMax - aMin, 0.25));
            }
            
            dyStepLen /= ( (int)[flag_up count] - 1);

            if (aMaxTemp != 0.0) {
                [StaticData seaMinLas:[[StaticData yfielt][temp_up] floatValue] ];
                for (int i = temp_up; i < [StaticData RATE]; i++) {
                    if ([[StaticData yfielt][i] floatValue] < [StaticData aMinLast]) {
                        [StaticData seaMinLas: [[StaticData yfielt][i] floatValue] ];
                    }
                }
            }
            
            float v0 = 0.0;
            NSRange range80;
            range80.location = 0;
            range80.length = 80;
            NSMutableArray *v = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:range80]];

            for (int j = 0; j < [flag_up[0] intValue]; j++) {
                [v addObject: [NSNumber numberWithFloat: v0] ];
            }
            
            for (NSInteger j = 1; j < (int)[flag_up count]; j++) {
                int interval = [flag_up[j] intValue] - [flag_up[j - 1] intValue];
                v0 = [StaticData RATE] * dyStepLen / interval;

                for (int k = 0; k < interval; k++) {
                    [v addObject:[NSNumber numberWithFloat:v0]];
                }
            }

            v0 = 0.0;
            if ((int)[v count] < [StaticData RATE])
            {
                for (int j = 0; j < (int)[v count]; j++) {
                    v0 += [v[j] floatValue];
                }
                velSecond = (v0 + ([StaticData RATE] - (int)[v count]) *
                             [v [((int)[ v count] -1)] floatValue ]) /[StaticData RATE];
  //              NSLog(@"444444444444444444");
            }
            else {
                for (int j = 0; j < [StaticData RATE]; j++) {
                    v0 += [v[j] floatValue];
                }
                velSecond = v0 / [StaticData RATE];
 //               NSLog(@"55555555555555555555");
            }
            
        }
        
        [StaticData seXopt:velSecond AtIndex:2];
        
        float *res = malloc([StaticData Xopt_count]*sizeof(float));
        [self wlanMemsFusion:StaticData.flagEkf :NULL
                            :NULL :headPdr
           WithResultSavedIn:res];
        [StaticData setXopt:res withElemCount: [StaticData Xopt_count]];

        [StaticData seflagEkf:([StaticData flagEkf] + 1)];

        [StaticData selast_u:temp_up];
        [StaticData seaMaxLas: aMaxTemp];
        for (int j = 0; j < [StaticData MAX_INDEX]; j++)
        {
            [[StaticData yfielt] removeObjectAtIndex:0];
        }
    }
    else if ([StaticData moreSecond])
    {
        float aMaxTemp = 0.0;
        int temp_up = [StaticData NO_PEAK];
        NSRange range5;
        range5.location = 0;
        range5.length = 5;
        NSMutableArray *flag_up = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:range5]];
        
        if ([StaticData last_up] == [StaticData NO_PEAK])
        {
            [flag_up addObject: [NSNumber numberWithInt: -50]];
        }
        else
        {
            [flag_up addObject: [NSNumber numberWithInt: ([StaticData last_up] - [StaticData RATE])] ];
        }

        for (NSInteger i = 1; i < (int)[[StaticData yfielt] count] - 1; i++)
        {
            if ([[StaticData yfielt][i] floatValue] >= [[StaticData yfielt][i-1] floatValue])
            {
                if (([[StaticData yfielt][i] floatValue] > [[StaticData yfielt][i+1] floatValue])
                    && ([[StaticData yfielt][i] floatValue] > [StaticData UP_GATE])){
                    if ([flag_up[((int)[flag_up count] -1)] intValue] == -50)
                    {
                        [flag_up addObject:[NSNumber numberWithInt:(int)(i - 1)] ];
                        
                        if ( (i > 0) && (i <= [StaticData RATE]) )
                        {
                            [StaticData sestepNu: ([StaticData stepNum] + 1) ];
                            temp_up =(int)(i - 1);
                            aMaxTemp = [[StaticData yfielt][i] floatValue];
                        }
                        
                    }
                    else if (i - 1 - [flag_up[((int)[flag_up count] - 1)] intValue] > 20)
                    {
                        [flag_up addObject: [NSNumber numberWithInt: (int)(i - 1)] ];
                        if ((i > 0) && (i <= [StaticData RATE])) {
                            [StaticData  sestepNu: ([StaticData stepNum] + 1) ];
    //                        NSLog(@"step number : %i", [StaticData stepNum]);
                            temp_up = (int)(i - 1);
                            aMaxTemp = [[StaticData yfielt][i] floatValue];
                        }

                    }
                }
            }
        }
        
        if ((int)[flag_up count] == 1)
        {
            if ([StaticData last_up] >= 44){
                velSecond = 0.68;
   //             NSLog(@"6666666666666666");
            }
            else {
                velSecond = 0.0;
  //              NSLog(@"7777777777777777777777");
            }
        }
        else if ((int)[flag_up count] == 2)
        {
            if ([StaticData last_up] == [StaticData NO_PEAK])
            {
                if ([flag_up[((int)[flag_up count] - 1)] intValue] >= 49)
                {
                    velSecond = 0.0;
       //             NSLog(@"88888888888888888888");
                }
                else {
                    velSecond = ([StaticData RATE] - [flag_up[((int)[flag_up count] -1)] intValue]) * 0.68 / [StaticData RATE];
         //           NSLog(@"999999999999999999");
                }
                
            }
            else
            {
                Networks *networks = [[Networks alloc] init];
                
                float dyStepLen = 0.0;
                float SF = 50.0 / ( [flag_up[1] intValue] - [flag_up[0] intValue]);
                
                NSArray *p = @[[NSNumber numberWithFloat: SF], [NSNumber numberWithFloat:[StaticData H ]]];
                float c = [networks bpNet: p];
                
                float aMin = [StaticData aMinLast];
                for (int k = 1; k <=  [flag_up[1] intValue]; k++) {
                    if ([[StaticData yfielt][k] floatValue] < aMin) {
                        aMin = [[StaticData yfielt][k] floatValue];
                    }
                }
                
                float aMax = [StaticData aMaxLast];
                dyStepLen = (float)(c * powf(aMax - aMin, 0.25));

                if (aMaxTemp != 0.0)
                {
                    [StaticData seaMinLas: [[StaticData yfielt][temp_up + 1] floatValue] ];
                    for (int i = temp_up + 1; i <= [StaticData RATE]; i++) {
                        if ([ [StaticData yfielt][i] floatValue ] < [StaticData aMinLast]) {
                            [StaticData seaMinLas: [[StaticData yfielt][i] floatValue] ];
                        }
                    }
                }

                velSecond = [StaticData RATE] * dyStepLen /
                ([flag_up[((int)[flag_up count] - 1)] intValue] - [flag_up[0] intValue ]);
            //    NSLog(@"10000000000000000");
            }
        }
        else {
            float v0 = 0.0;
            NSRange range80;
            range80.location = 0;
            range80.length = 80;
            NSMutableArray *v = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:range80]];

            if ([StaticData last_up] == [StaticData NO_PEAK])
            {
                Networks *networks = [[Networks alloc] init];

                float dyStepLen = 0.0;
                for (NSInteger j = 1; j < (int)[flag_up count] - 1; j++)
                {
                    float SF = 50.0 / ([flag_up[j + 1] intValue] - [flag_up[j] intValue]);

                    NSArray *p = @[ [NSNumber numberWithFloat: SF], [NSNumber numberWithFloat: StaticData.H] ];
                    float c = [networks bpNet: p];

                    float aMin = [[StaticData yfielt][ [flag_up[j] intValue] + 1 ] floatValue];

                    for (int k = [flag_up[j] intValue] + 1;
                         k <=[flag_up[j + 1] intValue]; k++) {
                        if (aMin > [[StaticData yfielt][k] floatValue]) {
                            aMin = [[StaticData yfielt][k] floatValue];
                        }
                    }
                    float aMax = [[StaticData yfielt][[flag_up[j] intValue] + 1 ] floatValue];

                    dyStepLen = dyStepLen + (float)(c * powl(aMax - aMin, 0.25));
                }
                
                dyStepLen /= (int)[flag_up count] - 2;

                if (aMaxTemp != 0.0)
                {
                    [StaticData seaMinLas: [[StaticData yfielt][temp_up + 1] floatValue]];
                    for (int i = temp_up + 1; i <= [StaticData RATE]; i++) {
                        if ([[StaticData yfielt][i] floatValue] < [StaticData aMinLast]) {
                            [StaticData seaMinLas: [[StaticData yfielt][i] floatValue]];
                        }
                    }

                }

                for (int j = 0; j < [flag_up[1] intValue]; j++)
                {
                    [v addObject: [NSNumber numberWithFloat:v0]];
                }

                for (int j = 1; j < (int)[flag_up count] - 1; j++) {
                    int interval = [flag_up[j + 1] intValue] - [flag_up[j] intValue];
                    v0 = [StaticData RATE] * dyStepLen / interval;
                    for (int k = 0; k < interval; k++) {
                        [v addObject: [NSNumber numberWithFloat:v0]];
                    }
                }

                if ((int)[v count] < [StaticData RATE]) {
                    for (int i = 0; i < [StaticData RATE] - [flag_up[(int)[flag_up count] - 1] intValue];
                         i++)
                    {
                        [v addObject: [NSNumber numberWithFloat: v0]];
                    }
                }
                
                v0 = 0.0;
                for (int i = 0; i < [StaticData RATE]; i++) {
                    v0 +=  [v[i] floatValue];
                }
                velSecond = v0 / [StaticData RATE];
          //      NSLog(@"110000000000000000000");
            }
            else
            {
                Networks *networks = [[Networks alloc] init];

                float dyStepLen = 0.0;
                for (NSInteger j = 0; j < (int)[flag_up count] - 1; j++)
                {
                    float SF = 50.0 / ([flag_up[j + 1] intValue] - [flag_up[j] intValue]);
                    NSArray *p = @[ [NSNumber numberWithFloat:SF], [NSNumber numberWithFloat: [StaticData H]] ];
                    float c = [networks bpNet: p];
                    float aMax = 0.0;
                    float aMin = 0.0;
                    if (j == 0) {
                        aMin = [StaticData aMinLast];
                        for (int i = 1; i <= [flag_up[j + 1] intValue]; i++)
                        {
                            if (aMin > [[StaticData yfielt][i] floatValue]) {
                                aMin = [[StaticData yfielt][i] floatValue];
                            }
                        }
                        aMax = [StaticData aMaxLast];
                    }
                    else {
                        aMin = [[StaticData yfielt][[flag_up[j] intValue] + 1 ] floatValue];
                        for (int k = [flag_up[j] intValue] + 1; k <= [flag_up[j + 1] intValue]; k++) {
                            if (aMin > [[StaticData yfielt][k] floatValue]) {
                                aMin = [[StaticData yfielt][k] floatValue];
                            }
                        }
                        aMax = [[StaticData yfielt][[flag_up[j] intValue] + 1] floatValue];
                    }
                    
                    dyStepLen = dyStepLen + (float)(c * powl(aMax - aMin, 0.25));
                }
                dyStepLen /= (int)[flag_up count] - 1;

                if (aMaxTemp != 0.0) {
                    [StaticData seaMinLas: [[StaticData yfielt][temp_up + 1] floatValue]];
                    for (int i = temp_up + 1; i <= [StaticData RATE]; i++) {
                        if ([[StaticData yfielt][i] floatValue] < [StaticData aMinLast]) {
                            [StaticData seaMinLas: [[StaticData yfielt][i] floatValue]];
                        }
                    }
                }
                v0 = [StaticData RATE] * dyStepLen /([flag_up[1] intValue] - [flag_up[0] intValue]);
                for (int i = 0; i < [flag_up[1] intValue]; i++) {
                    [v addObject: [NSNumber numberWithFloat: v0]];
                }
                for (int i = 1; i < (int)[flag_up count] - 1; i++) {
                    int interval = [flag_up[i + 1] intValue] - [flag_up[i] intValue];
                    v0 = [StaticData RATE] * dyStepLen / interval;
                    for (int j = 0; j < interval; j++) {
                        [v addObject: [NSNumber numberWithFloat:v0]];
                    }
                }
                if ((int)[v count] < [StaticData RATE]) {
                    for (int i = 0; i < [StaticData RATE] - [flag_up[(int)[flag_up count] -1 ] intValue];
                         i++)
                    {
                        [v addObject: [NSNumber numberWithFloat: v0]];
                    }
                }
                
                v0 = 0.0;
                for (int i = 0; i < [StaticData RATE]; i++) {
                    v0 += [v[i] floatValue];
                }
                velSecond = v0 / [StaticData RATE];
           //     NSLog(@"120000000000000000000");
            }

        }
        float headPdr = [ [StaticData headCache1][[StaticData MAX_INDEX]] floatValue];

        [StaticData sefi0: headPdr];
     //   NSLog(@"GlobalConfig.getThreshold()=%@", [GlobalConfig threshold]);
        if (velSecond < [[GlobalConfig threshold] doubleValue])
        {
            [StaticData sequiescentCnt: ([StaticData quiescentCnt] + 1)];
            
            float res[[StaticData Xopt_count]];
            [self wlanMemsFusion:[StaticData flagEkf] :NULL
                                :NULL :headPdr
               WithResultSavedIn:res];
            [StaticData setXopt:res withElemCount: (int)sizeof(res)/sizeof(res[0])];
            
    //        NSLog(@"sssssssssssssssssssssss   is stop!");
        }
        else {
            [StaticData seXopt:velSecond AtIndex:2];
            [StaticData sequiescentCnt: 0];
            float res[[StaticData Xopt_count]];
            [self wlanMemsFusion:[StaticData flagEkf] :NULL
                                :NULL :headPdr
               WithResultSavedIn:res];
            [StaticData setXopt:res withElemCount: (int)sizeof(res)/sizeof(res[0])];

  //          NSLog(@"mmmmmmmmmmmmmmmmmmmmmm   is mvoe!");
        }
        [StaticData selast_u: temp_up];
        [StaticData seaMaxLas: aMaxTemp];
        for (int j = 0; j < [StaticData RATE]; j++) {
            [[StaticData seyfielt] removeObjectAtIndex:0 ];
            [[StaticData seheadCache1] removeObjectAtIndex:0];
        }
    }
    [StaticData sev0: velSecond];
  //  NSLog(@"sudu  ---%f",[StaticData v]);
    xylocation[0] = [StaticData XoptAIndex:0];
    xylocation[1] = [StaticData XoptAIndex:1];
    return ;
}

         

-(void) wlanMemsFusion: (int) flagEkf : (float*) dataWiFi : (float*) dataWifiBssid : (float) headPDR WithResultSavedIn: (float*) res

{
    float f[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float F[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float Xpri[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    float Ppri[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float Z[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    float U[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    float V[4] = {0.0f, 0.0f, 0.0f, 0.0f};
    float k0 = 1.0f;
    float k1 = 2.5f;
    float alpha[2] = {0.0f, 0.0f};
    float R0[] = { 60.0f, 1000.0f, 0.0f, 0.0f };
    float R[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float KK[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f},
        {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
    float II[4][4] = { { 1.0f, 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f, 0.0f },
        { 0.0f, 0.0f, 0.0f, 1.0f } };
    
    @try
    {
        if ((flagEkf > 1) && (fabs(headPDR - [StaticData XoptAIndex:3]) > 1.396263401595464)) {
            [StaticData seXopt:headPDR AtIndex:3];
        }
        float f1[4][4] = { { 1.0f, 0.0f, (float)sin([StaticData XoptAIndex:3]), 0.0f },
            { 0.0f, 1.0f, (float)cos([StaticData XoptAIndex:3]), 0.0f },
            { 0.0f, 0.0f, 1.0f, 0.0f },
            { 0.0f, 0.0f, 0.0f, 1.0f } };
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                f[i][j] = f1[i][j];
            }
        }

        float F1[4][4] = {
            {1.0f,
                0.0f,
                (float)sin([StaticData XoptAIndex:3]),
                (float)([StaticData XoptAIndex:2] * cos([StaticData XoptAIndex:3])) },
            {0.0f,
                1.0f,
                (float)cos([StaticData XoptAIndex:3]),
                (float)(-[StaticData XoptAIndex:2] * sin([StaticData XoptAIndex:3])) },
            { 0.0f, 0.0f, 1.0f, 0.0f },
            { 0.0f, 0.0f, 0.0f, 1.0f }
        };
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                F[i][j] = F1[i][j];
            }
        }

        [MatrixOperate multiplyABWithFloatMatrix:&f[0][0] WithRow:4 WithCol:4
                                  withFloatArray:[StaticData xopt] WithElemCount: [StaticData Xopt_count]
                              ResultSavedInArray:Xpri];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %@", [exception name], [exception reason]);
    }
    
    [MatrixOperate minusArrytWithFloatArray:Z WithElemCount:4
                             withFloatArray:Xpri WithElemCount:4
                WIthResultSavedInFloatArray:U WithElemCount:4];
    for (int i = 0; i < 4; i++) {
        V[i] =((float)((U[i] + sqrt(R0[i])) / 2.0 / 0.6745 /
                       sqrt(R0[i])));
    }
    
    if (fabsf(Z[0] - Xpri[0]) < 2.0f) {
        alpha[0] = 10.0f;
    }
    else if (fabsf(V[0]) <= k0)
        alpha[0] = 1.0f;
    else if ((fabsf(V[0]) > k0) && (fabsf(V[0]) <= k1))
        alpha[0] = ((float)(k0 / fabsf(V[0]) *
                            pow((k1 - fabsf(V[0])) / (k1 - k0), 2.0)));
    else if (fabsf(V[0]) > k1) {
        alpha[0] = ((float)pow(10.0, -10.0));
    }
    
    if (fabsf(Z[1] - Xpri[1]) < 2.0f)
    {
        alpha[1] = 10.0f;
    }
    else if (fabsf(V[1]) <= k0)
        alpha[1] = 1.0f;
    else if ((fabsf(V[1]) > k0) && (fabsf(V[1]) <= k1))
        alpha[1] = ((float)(k0 / fabsf(V[1]) *
                            pow((k1 - fabsf(V[1])) / (k1 - k0), 2.0)));
    else if (fabsf(V[1]) > k1) {
        alpha[1] = ((float)pow(10.0, -10.0));
    }

    float R1[4][4] = { { R0[0] / alpha[0], 0.0f, 0.0f, 0.0f },
        { 0.0f, R0[1] / alpha[1], 0.0f, 0.0f },
        { 0.0f, 0.0f, R0[2], 0.0f },
        { 0.0f, 0.0f, 0.0f, R0[3] } };
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            R[i][j] = R1[i][j];
        }
    }
    
    float Ppri1[4][4];
    [MatrixOperate multiplyABWithFloatMatrix:&F[0][0] WithRow:4 WithCol:4
                             withFloatMatrix:[StaticData Popt] WithRow:4 WithCol:4 WithResultSavedInFloatMatrix:&Ppri1[0][0] WithRow:4 WithCol:4];
    
    float Ppri2[4][4];
    [MatrixOperate getA_TWithFloatMatrix:&F[0][0] WithRow:4 WithCol:4
                 withResultSavedInMatrix:&Ppri2[0][0] WithRow:4 WithCol:4];

    float Ppri3[4][4];
    [MatrixOperate multiplyABWithFloatMatrix:&Ppri1[0][0] WithRow:4 WithCol:4
                             withFloatMatrix:&Ppri2[0][0] WithRow:4 WithCol:4
                WithResultSavedInFloatMatrix:&Ppri3[0][0] WithRow:4 WithCol:4];

    [MatrixOperate addABWithFloatMatrix:&Ppri3[0][0] WithRow:4 WithCol:4
                        withFloatMatrix:&II[0][0] WithRow:4 WithCol:4
                WithResultSavedInMatrix:&Ppri[0][0] WithRow:4 WithCol:4];
    
    float K1[4][4];
    [MatrixOperate addABWithFloatMatrix:&Ppri[0][0] WithRow:(4) WithCol:4
                        withFloatMatrix:&R[0][0] WithRow:4 WithCol:4
                WithResultSavedInMatrix:&K1[0][0] WithRow:4 WithCol:4];
    float K2[4][4];
    [MatrixOperate getNWithFloatMatrix:&K1[0][0] WithRow:4 WithCol:4
               WithResultSavedInMatrix:&K2[0][0] WithRow:4 WithCol:4];

    [MatrixOperate multiplyABWithFloatMatrix:&Ppri[0][0] WithRow:4 WithCol:4
                             withFloatMatrix:&K2[0][0] WithRow:4 WithCol:4
                WithResultSavedInFloatMatrix:&KK[0][0] WithRow:4 WithCol:4];
    float Xopt1[4];
    [MatrixOperate minusArrytWithFloatArray:Z WithElemCount:4
                             withFloatArray:Xpri WithElemCount:4
                WIthResultSavedInFloatArray:Xopt1 WithElemCount:4];
    float Xopt2[4];
    [MatrixOperate multiplyABWithFloatMatrix:&KK[0][0] WithRow:4 WithCol:4
                              withFloatArray:Xopt1 WithElemCount:4
                          ResultSavedInArray:Xopt2];
    float xoptTemp[4];
    [MatrixOperate addArrywithFloatArray:Xpri WithElemCount:4
                          withFloatArray:Xopt2 withElemCount:4
                    WithResultSaveInArry:xoptTemp];
    [StaticData setXopt:xoptTemp withElemCount:4];

    float Popt1[4][4];
    [MatrixOperate minusABWithFloatMatrix:&II[0][0] WithRow:4 WithCol:4
                          withFloatMatrix:&KK[0][0] WithRow:4 WithCol:4
             WithResultSavedInFloatMatrix:&Popt1[0][0] WithRow:4 WithCol:4];

    float poptTemp[4][4];
    [MatrixOperate multiplyABWithFloatMatrix:&Popt1[0][0] WithRow:4 WithCol:4
                             withFloatMatrix:&Ppri[0][0] WithRow:4 WithCol:4
                WithResultSavedInFloatMatrix:&poptTemp[0][0] WithRow:4 WithCol:4];
    [StaticData setPopt:&poptTemp[0][0] withRow:4 WithCol:4];
    
    for(int i = 0; i < [StaticData Xopt_count]; i++)
    {
        res[i] = [StaticData XoptAIndex:i];
    }
    return;
}


@end
