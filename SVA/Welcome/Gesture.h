//
//  Gesture.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Gesture : NSObject

- (instancetype) init;
- (void) attitudeDecide:(float*)RPY  Of:(float*) dataMems
                WithRow:(int)rowOfDataMems WithCol:(int)colofDataMems;
-(void) sortFloat: (float*) data WithElemCount:(int)lenOfData
WithResultSavedInArray:(float*)dataNew WithElemCount:(int)lenOfDataNew;

-(float) meanFloat: (float*) data WithElemCount:(int)lenOfData;
//- (BOOL) findInt: (int)target In: (NSArray*) dataSource;
//- (BOOL) find: (float) target InArrayList: (NSArray*) dataSource;
- (NSArray*) modeChange: (NSArray*) dataMems;
-(BOOL) findInt: (NSArray*) dataSource : (NSInteger) target;



@end
