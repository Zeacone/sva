//
//  MatrixOperate.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import "MatrixOperate.h"
#import "Statistics.h"
#import "StaticData.h"

@implementation MatrixOperate

+(double)getHLDoubleWithDoubleMatrix:(double*)data WithRow:(int)row  WithCol:(int)col
{
    double total = 0.0;
    int num = row;
    double nums[num];
    for(int i = 0; i < num; i++)
    {
        nums[i] = 0.0;
    }
    
    if (row != col)
    {
  //      NSLog(@"MatrixOperate1");
        return 0;
    }
    else {
        if (row == 2)
        {
            return  (*data) * (*(data + col + 1)) - (*(data + 1) * (*(data + col)));       //data[0][0] * data[1][1] - data[0][1] * data[1][0];
        }
        for (int i = 0; i < num; i++)
        {
            double newData[row - 1][col - 1];
            [MatrixOperate getDYWithDoubleMatrix:data WithRow:row WithCol:col
                                         withInt:1 withInt:(i+1)
                               WithResultSavedIn:&newData[0][0] WithRow:(row-1) WithCol:(col-1)];
            if (i % 2 == 0)
            {
               
                nums[i] = *(data + i) *  [MatrixOperate getHLDoubleWithDoubleMatrix:&newData[0][0] WithRow:(row-1) WithCol:(col-1)];   //nums[i] = (data[0][i] * getHL(getDY(data, 1, i + 1)));
            }
            else {
                
                nums[i] = -(*(data + i)) * [MatrixOperate getHLDoubleWithDoubleMatrix:&newData[0][0] WithRow:(row-1) WithCol:(col-1)]; // nums[i] = (-data[0][i] * getHL(getDY(data, 1, i + 1)));
            }
        }
        for (int i = 0; i < num; i++)
        {
            total += nums[i];
        }
    }

    return total;
}


+(float)getHLFloatWithFloatMatrix:(float*)data WithRow:(int)row  WithCol:(int)col
{
    float total = 0.0;
    int num = row;
    float nums[num];
    for(int i = 0; i < num; i++)
    {
        nums[i] = 0.0;
    }
    
    if (row != col)
    {
//        NSLog(@"MatrixOperate2");
        return 0;
    }
    else {
        if (row == 2)
        {
            return  (*data) * (*(data + col + 1)) - (*(data + 1) * (*(data + col)));       //data[0][0] * data[1][1] - data[0][1] * data[1][0];
        }
        for (int i = 0; i < num; i++)
        {
            float newData[row - 1][col - 1];
            [MatrixOperate getDYWithFloatMatrix:data WithRow:row WithCol:col
                                        withInt:1 withInt:(i+1)
                              WithResultSavedIn:&newData[0][0] WithRow:(row-1) WithCol:(col-1)];
            if (i % 2 == 0)
            {
                
                nums[i] = *(data + i) * [MatrixOperate getHLFloatWithFloatMatrix:&newData[0][0] WithRow:(row-1) WithCol:(col-1)];   //nums[i] = (data[0][i] * getHL(getDY(data, 1, i + 1)));
            }
            else {
                
                nums[i] = -(*(data + i)) * [MatrixOperate getHLFloatWithFloatMatrix:&newData[0][0] WithRow:(row-1) WithCol:(col-1)]; // nums[i] = (-data[0][i] * getHL(getDY(data, 1, i + 1)));
            }
        }
        for (int i = 0; i < num; i++)
        {
            total += nums[i];
        }
    }
    
    return total;
}


+(int)getHLIntWithIntMatrix:(int*)data WithRow:(int)row  WithCol:(int)col
{
    // public static int getHL(int[][] data)
    int total = 0.0;
    int num = row;
    int nums[num];
    for(int i = 0; i < num; i++)
    {
        nums[i] = 0;
    }
    
    if (row != col)
    {
 //       NSLog(@"MatrixOperate3");
        return 0;
    }
    else {
        if (row == 2)
        {
            return  (*data) * (*(data + col + 1)) - (*(data + 1) * (*(data + col)));       //data[0][0] * data[1][1] - data[0][1] * data[1][0];
        }
        for (int i = 0; i < num; i++)
        {
            int newData[row - 1][col - 1];
            [MatrixOperate getDYWithIntMatrix:data WithRow:row WithCol:col
                                      withInt:1 withInt:(i+1)
                            WithResultSavedIn:&newData[0][0] WithRow:(row-1) WithCol:(col-1)];
            if (i % 2 == 0)
            {
                
                nums[i] = *(data + i) *  [MatrixOperate getHLIntWithIntMatrix:&newData[0][0] WithRow:(row-1) WithCol:(col-1)];   //nums[i] = (data[0][i] * getHL(getDY(data, 1, i + 1)));
            }
            else {
                
                nums[i] = -(*(data + i)) * [MatrixOperate getHLIntWithIntMatrix:&newData[0][0] WithRow:(row-1) WithCol:(col-1)]; // nums[i] = (-data[0][i] * getHL(getDY(data, 1, i + 1)));
            }
        }
        for (int i = 0; i < num; i++)
        {
            total += nums[i];
        }
    }
    
    return total;
    
}


+(void)getDYWithDoubleMatrix:(double*)data WithRow:(int)rowOfData WithCol:(int)colOfData
                    withInt:(int)h withInt:(int)v
          WithResultSavedIn:(double*)newData WithRow:(int)rowOfNewData WithCol:(int)colOfNewData
{
    //  public static double[][] getDY(double[][] data, int h, int v)
    int H = rowOfData;
    int V = colOfData;
    //double[][] newData = new double[H - 1][V - 1];
    if( (H != V) || ((rowOfData - 1) != rowOfNewData) || ((colOfData - 1) != colOfNewData))
    {
  //      NSLog(@"MatrixOperate4");
        return ;
    }
    else {
        for (int i = 0; i < rowOfNewData; i++)
        {
            if (i < h - 1)
            {
                for (int j = 0; j < colOfNewData; j++)
                {
                    if (j < v - 1)
                    {
                        *(newData + i * colOfNewData + j) = *(data + i * colOfData + j); //newData[i][j] = data[i][j];
                    }
                    else {
                        //newData[i][j] = data[i][(j + 1)];
                    }
                }
            }
            else {
                for (int j = 0; j < colOfNewData; j++)
                {
                    if (j < v - 1)
                    {
                        *(newData + i * colOfNewData + j) = *(data + (i + 1) * colOfData + j);//newData[i][j] = data[(i + 1)][j];
                    }
                    else {
                        *(newData + i * colOfNewData + j) = *(data + (i + 1) * colOfData +(j + 1));  //newData[i][j] = data[(i + 1)][(j + 1)];
                    }
                }
            }
        }
    }
    return ;
}


+(void)getDYWithFloatMatrix:(float*)data WithRow:(int)rowOfData WithCol:(int)colOfData
                    withInt:(int)h withInt:(int)v
          WithResultSavedIn:(float*)newData WithRow:(int)rowOfNewData WithCol:(int)colOfNewData
{
    int H = rowOfData;
    int V = colOfData;
    
    if( (H != V) || ((rowOfData - 1) != rowOfNewData) || ((colOfData - 1) != colOfNewData))
    {
  //      NSLog(@"MatrixOperate5");
        return ;
    }
    else {
        for (int i = 0; i < rowOfNewData; i++)
        {
            if (i < h - 1)
            {
                for (int j = 0; j < colOfNewData; j++)
                {
                    if (j < v - 1)
                    {
                        *(newData + i * colOfNewData +j) = *(data + i * colOfData + j) ;        //newData[i][j] = data[i][j];
                    }
                    else
                    {
                        *(newData + i * colOfNewData + j) = *(data + i * colOfData + (j + 1)) ;      //newData[i][j] = data[i][(j + 1)];
                    }
                }
            }
            else {
                for (int j = 0; j < colOfNewData; j++)
                {
                    if (j < v - 1)
                    {
                        *(newData + i * colOfNewData + j) = *(data + (i + 1) * colOfData + j); //newData[i][j] = data[(i + 1)][j];
                    }
                    else {
                        *(newData + i * colOfNewData + j) = *(data + (i + 1) * colOfData + (j + 1));     //newData[i][j] = data[(i + 1)][(j + 1)];
                    }
                }
            }
        }
    }
    return ;
}


+(void)getDYWithIntMatrix:(int*)data WithRow:(int)rowOfData WithCol:(int)colOfData
                    withInt:(int)h withInt:(int)v
          WithResultSavedIn:(int*)newData WithRow:(int)rowOfNewData WithCol:(int)colOfNewData
{
    int H = rowOfData;
    int V = colOfData;
    //int[][] newData = new int[H - 1][V - 1];
    if( (H != V) || ((rowOfData - 1) != rowOfNewData) || ((colOfData - 1) != colOfNewData))
    {
  //      NSLog(@"MatrixOperate6");
        return ;
    }
    else {
        for (int i = 0; i < rowOfNewData; i++)
        {
            if (i < h - 1)
            {
                for (int j = 0; j < colOfNewData; j++)
                {
                    if (j < v - 1)
                    {
                        *(newData + i * colOfNewData + j) = *(data + i * colOfData + j); //newData[i][j] = data[i][j];
                    }
                    else {
                        *(newData + i * colOfNewData + j) = *(data + i * colOfData + (j + 1)); //newData[i][j] = data[i][(j + 1)];
                    }
                }
            }
            else {
                for (int j = 0; j < colOfNewData; j++)
                {
                    if (j < v - 1)
                    {
                        *(newData + i * colOfNewData + j) = *(data + (i + 1) * colOfData + j); //newData[i][j] = data[(i + 1)][j];
                    }
                    else {
                        *(newData + i * colOfNewData + j) = *(data + (i + 1) * colOfData + (j + 1));//newData[i][j] = data[(i + 1)][(j + 1)];
                    }
                }
            }
        }
    }
    return ;
}

//+(NSMutableArray*)getNDoubleWithDoubleArray:(NSMutableArray*)data
+(void)getNWithDoubleMatrix:(double*)data WithRow: (int)rowOfData WithCol: (int)colOfData
   WithResultSavedInMatrix:(double*)newData WithRow:(int)rowOfNewData WithCol: (int)colOfNewData
{
    if (rowOfData != colOfData)
    {
 //       NSLog(@"MatrixOperate7");
        return ;
    }
    double A = [MatrixOperate getHLDoubleWithDoubleMatrix:data WithRow:rowOfData WithCol:colOfData];
    for (int i = 0; i < rowOfData; i++)
    {
        for (int j = 0; j < rowOfData; j++)
        {
            double num;
            double temp[rowOfData -1][colOfData-1];
            [MatrixOperate getDYWithDoubleMatrix:(data ) WithRow:rowOfData WithCol:colOfData
                                         withInt:(i+1) withInt:(j+1)
                               WithResultSavedIn:&temp[0][0]];
            if ((i + j) % 2 == 0)
            {
                num = [MatrixOperate getHLDoubleWithDoubleMatrix:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            }
            else {
                num = -[MatrixOperate getHLDoubleWithDoubleMatrix:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            }

            *(newData + i * colOfNewData + j) = (num / A);
        }
    }
    [MatrixOperate getA_TWithDoubleMatrix:newData WithRow:rowOfNewData  WithCol:colOfNewData withResultSavedInMatrix:newData WithRow:rowOfNewData WithCol:colOfNewData];
    return ;
}


+(void)getNWithFloatMatrix:(float*)data WithRow: (int)rowOfData WithCol: (int)colOfData
   WithResultSavedInMatrix:(float*)newData WithRow:(int)rowOfNewData WithCol: (int)colOfNewData
{
    if (rowOfData != colOfData)
    {
  //      NSLog(@"MatrixOperate8");
        return ;
    }
    float A = [MatrixOperate getHLFloatWithFloatMatrix:data WithRow:rowOfData WithCol:colOfData];
    //float A = [MatrixOperate getHLFloatWithFloatArray:data];
    for (int i = 0; i <  rowOfData; i++)
    {
        for (int j = 0; j < rowOfData; j++)
        {
            float num;
            float temp[rowOfData -1][colOfData-1];
            [MatrixOperate getDYWithFloatMatrix:data WithRow:rowOfData WithCol:colOfData withInt:(i+1) withInt:(j+1) WithResultSavedIn:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            if ((i + j) % 2 == 0)
            {
                num = [MatrixOperate getHLFloatWithFloatMatrix:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            }
            else {
                num = -[MatrixOperate getHLFloatWithFloatMatrix:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            }
            *(newData + i * colOfNewData + j) = num/A;
        }
    }
    [MatrixOperate getA_TWithFloatMatrix:newData WithRow:rowOfNewData WithCol:colOfNewData withResultSavedInMatrix:newData WithRow:rowOfNewData WithCol:colOfNewData];
}


+(void)getNWithIntMatrix:(int*)data WithRow: (int)rowOfData WithCol: (int)colOfData
WithResultSavedInMatrix:(float*)newData WithRow:(int)rowOfNewData WithCol: (int)colOfNewData
{
    if (rowOfData != colOfData)
    {
  //      NSLog(@"MatrixOperate9");
        return ;
    }
    float A = [MatrixOperate getHLIntWithIntMatrix:data WithRow:rowOfData WithCol:colOfData];
    for (int i = 0; i < rowOfData; i++)
    {
        for (int j = 0; j < rowOfData; j++)
        {
            float num;
            int temp[rowOfData - 1][colOfData -1];
            [MatrixOperate getDYWithIntMatrix:data WithRow:rowOfData WithCol:colOfData
                                      withInt:(i+1) withInt:(j+1)
                            WithResultSavedIn:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            
            if ((i + j) % 2 == 0)
            {
                num =  [MatrixOperate getHLIntWithIntMatrix:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            }
            else {
                num = -[MatrixOperate getHLIntWithIntMatrix:&temp[0][0] WithRow:(rowOfData-1) WithCol:(colOfData-1)];
            }

            *(newData + i * colOfNewData + j) = num/A;
        }
    }
    [MatrixOperate getA_TWithFloatMatrix:newData WithRow:rowOfNewData WithCol:colOfNewData withResultSavedInMatrix:newData WithRow:rowOfNewData WithCol:colOfNewData];
    return;
}

+(void)getA_TWithDoubleMatrix:(double*)A WithRow:(int)rowOfA WithCol: (int)colOfA
     withResultSavedInMatrix:(double*)A_T WithRow:(int)rowOfA_T WithCol: (int)colOfA_T
{
    int h = rowOfA;
    int v = colOfA;

    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < v; j++)
        {
            *(A_T + j * h + i)  = *(A + i * colOfA + j);
        }
    }
    return;
}


+(void)getA_TWithFloatMatrix:(float*)A WithRow:(int)rowOfA WithCol: (int)colOfA
                withResultSavedInMatrix:(float*)A_T WithRow:(int)rowOfA_T WithCol: (int)colOfA_T
{
    int h = rowOfA;
    int v = colOfA;
    //float[][] A_T = new float[v][h];
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < v; j++)
        {
            *(A_T + j * h + i)  = *(A + i * colOfA + j);
        }
    }
    return ;
}


//+(NSMutableArray*)getA_TIntWithIntArray:(NSMutableArray*)A
+(void)getA_TWithIntMatrix:(int*)A WithRow:(int)rowOfA WithCol: (int)colOfA
      withResultSavedInMatrix:(int*)A_T WithRow:(int)rowOfA_T WithCol: (int)colOfA_T
{
    int h = rowOfA;
    int v = colOfA;
    
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < v; j++)
        {
            *(A_T + j * h + i)  = *(A + i * colOfA + j);
        }
    }
    return ;
    
}
+(NSMutableArray*)multiplyABDoubleWithDoubleMatrix:(NSMutableArray*)matrixA
                                  withDoubleMatrix:(NSMutableArray*)matrixB
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [matrixA count];
    Col.location = 0;
    Col.length = [matrixB[0] count];

    NSMutableArray* matrixAB = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    for(NSInteger i = 0; i < Row.length; i++)
    {
        matrixAB[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange: Col]];
    }

    if ([matrixA[0] count] != [matrixB count])
    {
   //     NSLog(@"MatrixOperate10");
        return 0;
    }
    else {
        for (NSInteger i = 0; i < [matrixA count]; i++)
        {
            for (NSInteger j = 0; j < [matrixB[0] count]; j++)
            {
                double sum = 0.0;
                for (NSInteger k = 0; k < [matrixB count]; k++)
                {
                    sum += [matrixA[i][k] doubleValue] * [matrixB[k][j] doubleValue];
                }
                matrixAB[i][j] = [NSNumber numberWithDouble:sum];
            }
        }
    }

    return matrixAB;
}



+(void)multiplyABWithFloatMatrix:(float*)matrixA WithRow:(int) rowOfA WithCol:(int)colOfA
                   withFloatMatrix:(float*)matrixB WithRow:(int) rowOfB WithCol:(int)colOfB
    WithResultSavedInFloatMatrix: (float*) matrixAB WithRow: (int)rowOfMatrixAB WithCol: (int)colOfMatrixAB
{
    if (colOfA != rowOfB)
    {
//        NSLog(@"MatrixOperate11");
        return ;
    }
    else {
        for (int i = 0; i < rowOfA; i++)
        {
            for (int j = 0; j < colOfB; j++)
            {
                float sum = 0.0F;
                for (int k = 0; k < rowOfB; k++)
                {
                    sum += (*(matrixA + i * colOfA + k)) * (*(matrixB + k * colOfB + j));
                }
                *(matrixAB + i * colOfMatrixAB + j) = sum;
            }
        }
    }

    return;
}
    

+(NSMutableArray*)multiplyABFloatWithIntMatrix:(NSMutableArray*)matrixA
                                 withFloatMatrix:(NSMutableArray*)matrixB
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [matrixA count];
    Col.location = 0;
    Col.length = [matrixB[0] count];
    
    NSMutableArray* matrixAB = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    for(NSInteger i = 0; i < Row.length; i++)
    {
        matrixAB[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange: Col]];
    }

    if ([matrixA[0] count] != [matrixB count])
    {
//        NSLog(@"MatrixOperate12");
        return 0;
    }
    else {
        for (NSInteger i = 0; i < [matrixA count]; i++)
        {
            for (NSInteger j = 0; j < [matrixB[0] count]; j++)
            {
                float sum = 0.0;
                for (NSInteger k = 0; k < [matrixB count]; k++)
                {
                    sum += [matrixA[i][k] intValue] * [matrixB[k][j] floatValue];
                }
                matrixAB[i][j] = [NSNumber numberWithFloat: (float)sum];
            }
        }
    }
    
    return matrixAB;
}

+(void)multiplyABWithFloatMatrix:(float*)matrixA WithRow:(int) rowOfA WithCol:(int)colOfA
                              withIntMatrix:(int*)matrixB WithRow:(int) rowOfB WithCol:(int)colOfB
    WithResultSavedInFloatMatrix: (float*) matrixAB WithRow: (int)rowOfMatrixAB WithCol: (int)colOfMatrixAB
{
    if (colOfA != rowOfB)
    {
 //       NSLog(@"MatrixOperate13");
        return ;
    }
    else {
        for (int i = 0; i < rowOfA; i++)
        {
            for (int j = 0; j < colOfB; j++)
            {
                float sum = 0.0f;
                for (int k = 0; k < rowOfB; k++)
                {
                    sum += (*(matrixA + i * colOfA + k)) * (*(matrixB + k * colOfB + j));
                }
                *(matrixAB+i*colOfMatrixAB + j) = sum;
            }
        }
    }
    return;
}


+(NSMutableArray*)multiplyABDoubleWithDoubleMatrix:(NSMutableArray*)matrixA
                                  withIntMatrix:(NSMutableArray*)matrixB
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [matrixA count];
    Col.location = 0;
    Col.length = [matrixB[0] count];
    
    NSMutableArray* matrixAB = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    
    for(NSInteger i = 0; i < [matrixB[0] count]; i++)
    {
        matrixAB[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange: Col]];
    }

    if ([matrixA[0] count] != [matrixB count])
    {
 //        NSLog(@"MatrixOperate14");
        return 0;
    }
    else {
        for (NSInteger i = 0; i < [matrixA count]; i++)
        {
            for (NSInteger j = 0; j < [matrixB[0] count]; j++)
            {
                double sum = 0.0;
                for (int k = 0; k < [matrixB count]; k++)
                {
                    sum += [matrixA[i][k] doubleValue] * [matrixB[k][j] intValue];
                }
                matrixAB[i][j] = [NSNumber numberWithDouble:(double)sum];
            }
        }
    }
    return matrixAB;


}
+(NSMutableArray*)multiplyABIntWithIntArray:(NSMutableArray*)matrixA
                               withIntArray:(NSMutableArray*)matrixB
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = matrixA.count;
    Col.location = 0;
    Col.length = (int)((NSMutableArray*)(matrixB[0])).count;
    
    NSMutableArray* matrixAB = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    
    for(NSInteger i = 0; i < ((NSMutableArray*)(matrixB[0])).count; i++)
    {
        matrixAB[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange: Col]];
    }

    if ([matrixA[0] count] != [matrixB count])
    {
    //    NSLog(@"MatrixOperate15");
        return 0;
    }
    else {
        for (NSInteger i = 0; i < [matrixA count]; i++)
        {
            for (NSInteger j = 0; j < [matrixB[0] count]; j++)
            {
                int sum = 0;
                for (int k = 0; k < [matrixB count]; k++)
                {
                    sum += [matrixA[i][k] intValue] * [matrixB[k][j] intValue];
                }
                matrixAB[i][j] = [NSNumber numberWithInt:(int)sum];
            }
        }
    }
    return matrixAB;


}

+(NSMutableArray*)multiplyABDoubleWithDoubleMatrix:(NSMutableArray*)matrixA
                                  withDoubleArray:(NSMutableArray*)arryB
{
    NSRange Row;
    Row.location = 0;
    Row.length = [matrixA count];
    
    
    NSMutableArray* matrixAB = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    
    if ([matrixA[0] count] != [arryB count])
    {
       // NSLog(@"MatrixOperate16");
        return 0;
    }
    else {
        for (NSInteger i = 0; i < [matrixA count]; i++)
        {
            double sum = 0.0;
            for (NSInteger j = 0; j < [matrixA[0] count]; j++)
            {
                sum += [matrixA[i][j] doubleValue] * [arryB[j] doubleValue];
            }
            matrixAB[i] = [NSNumber numberWithDouble:(double)sum];
        }
    }
    return matrixAB;
}


+(void)multiplyABWithFloatMatrix:(float*)matrixA WithRow:(int)row WithCol:(int)col
                             withFloatArray:(float*)arryB WithElemCount:(int)nOfArryB
                         ResultSavedInArray: (float*)matrixAB
{
    
    if(col != nOfArryB)
    {
        //NSLog(@"MatrixOperate17");
        return ;
    }
    else {
        for (int i = 0; i < row; i++)
        {
            float sum = 0.0;
            for (int j = 0; j < col; j++)
            {
                sum += *(matrixA+i*col+j)  * arryB[j] ;
            }
            matrixAB[i] = sum;
        }
    }
    return ;
}

+(NSMutableArray*)multiplyABDoubleWithDoubleMatrix:(NSMutableArray*)matrixA
                        withFloatArray:(NSMutableArray*)arryB
{
    NSRange Row;
    Row.location = 0;
    Row.length = [matrixA count];
    
    
    NSMutableArray* matrixAB = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    if ([matrixA[0] count] != [arryB count])
    {
        //NSLog(@"MatrixOperate18");
        return 0;
    }
    else {
        for (int i = 0; i < [matrixA count]; i++)
        {
            double sum = 0.0;
            for (int j = 0; j < [matrixA[0] count]; j++)
            {
                sum += [matrixA[i][j]doubleValue] * [arryB[j] floatValue];
            }
            matrixAB[i] = [NSNumber numberWithDouble: (double)sum];
        }
    }
    return matrixAB;
}

+(NSMutableArray*)multiplyAbDoubleWithDouble:(double)a
                                  withDoubleArray:(NSMutableArray*)A
{
    
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [ A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray* aA = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    
    for(NSInteger i = 0; i < [ A count]; i++)
    {
        aA[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange: Col]];
    }

    for (NSInteger i = 0; i < [A count]; i++)
    {
        for (NSInteger j = 0; j < [A[0] count]; j++)
       {
         aA[i][j] = [NSNumber numberWithDouble: (a * [A[i][j] doubleValue])];
       }
     }
 
     return aA;

}


+(void)multiplyAbWithFloat:(float)a
                   withFloatMatrix:(float*)A
                              withRow: (int)row
                              withCol: (int)col
                        savedInMatrix: (float*) aA
{
    for (int i = 0; i < row; i++)
    {
       for (int j = 0; j < col; j++)
       {
           *(aA + i * col + j) = a * (*(A+(i*col+j)));
       }
     }
 
     return ;


}

+(NSMutableArray*)multiplyAbFloatWithInt:(int)a
                   withFloatArray:(NSMutableArray*)A
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray * aA = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        aA[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }
    
    for (NSInteger i = 0; i < [A count]; i++)
    {
        for (NSInteger j = 0; j < [A[0] count]; j++)
        {
            aA[i][j] = [NSNumber numberWithFloat:(a * [A[i][j] floatValue])];
        }
    }
    
    return aA;
    
    
}

+(NSMutableArray*)multiplyAbIntWithInt:(int)a
                 withIntArray:(NSMutableArray*)A
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray * aA = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        aA[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }
    
    for (int i = 0; i < [A count]; i++)
    {
        for (int j = 0; j < [A[0] count]; j++)
        {
            aA[i][j] = [NSNumber numberWithInt:(a * [A[i][j] intValue]) ];
        }
    }
    
    return aA;

    
    
}

+(NSMutableArray*)multiplyAbFloatWithFloat:(float)a
                   withIntArray:(NSMutableArray*)A
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray * aA = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        aA[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }
    
    for (int i = 0; i < [A count]; i++)
    {
        for (int j = 0; j < ((NSMutableArray*)A[0]).count; j++)
        {
            aA[i][j] = [NSNumber numberWithInt:(int)(a * [A[i][j] intValue])];
        }
    }
    
    return aA;  //int

    
    
}

+(NSMutableArray*)multiplyAbDoubleWithDouble:(double)a
                     withDouble1Array:(NSMutableArray*)Arry
{
    NSRange leng;
    leng.location = 0;
    leng.length = [Arry count];
    
    NSMutableArray * aA = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    
    for (int i = 0; i < [Arry count]; i++)
    {
        for (int j = 0; j < [Arry[0] count]; j++)
        {
            aA[i] = [NSNumber numberWithDouble: (double)(a * [Arry[i] doubleValue])];
        }
    }
    
    return aA;

    
    
}

+(void)multiplyAbWithFloat:(float)a
           withFloat1Array:(float*)Arry WithElemCount: (int)nOfArry
         WithResultSavedIn:(float*) aA
{
    for (int i = 0; i < nOfArry; i++)
    {
        aA[i] = (a * Arry[i]);
    }

    return ;
}

+(NSMutableArray*)addABIntWIthIntArray:(NSMutableArray*)A
                  withIntArray:(NSMutableArray*)B
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray * ABadd =[NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue]subarrayWithRange:Row]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        ABadd[i] = [NSMutableArray  arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }
    
    
     if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
     {
        // NSLog(@"MatrixOperate19");
         return 0;
     }
     else {
         for (int i = 0; i < [A count]; i++)
         {
             for (int j = 0; j < [A count]; j++)
             {
                 ABadd[i][j] =[NSNumber numberWithInt: (int)([A[i][j] intValue] +[B[i][j] intValue])];
             }
         }
     }
     return ABadd;
}

+(void)addABWithFloatMatrix:(float*)A WithRow: (int)rowOfA WithCol: (int)colOfA
            withFloatMatrix:(float*)B WithRow: (int)rowOfB WithCol: (int)colOfB
    WithResultSavedInMatrix:(float*) ABadd WithRow:(int)rowOfABadd WithCol: (int)colOfABadd

{
    if ((rowOfA != rowOfB) || (colOfA != colOfB))
    {
      //  NSLog(@"MatrixOperate20");
        return ;
    }
    else {
        for (int i = 0; i < rowOfA; i++)
        {
            for (int j = 0; j < colOfA; j++)
            {
                *(ABadd + i * colOfABadd + j)= (*(A + i * colOfA +j)) + (*(B + i * colOfB + j));
            }
        }
    }
    return ;
}

+(NSMutableArray*)addABDoubleWithDoubleArray:(NSMutableArray*)A
                  withDoubleArray:(NSMutableArray*)B
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray * ABadd =[NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue]subarrayWithRange:Row]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        ABadd[i] = [NSMutableArray  arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }

    if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
    {
      //  NSLog(@"MatrixOperate21");
        return 0;
    }
    else {
        for (int i = 0; i < [A count]; i++)
        {
            for (int j = 0; j < [A count]; j++)
            {
                ABadd[i][j] =[NSNumber numberWithDouble: (double)([A[i][j] doubleValue] +[B[i][j] doubleValue])];
            }
        }
    }
    return ABadd;
}

+(void)addABwithIntMatrix:(int*)A withRowOfA: (int)arow withColOfA: (int)acol
                     withFloatMatrix:(float*)B withRowOfB:(int)brow withColOfB:(int)bcol
                       savedInMatrix:(float*)ABadd
{
    if ((arow != brow) || (acol != bcol))
    {
        //NSLog(@"MatrixOperate22");
        return ;
    }
    else {
        for (int i = 0; i < arow; i++)
        {
            for (int j = 0; j < acol; j++)
            {
                *(ABadd+i*acol+j) = (float)( *(A+i*acol+j) + *(B+i*acol+j));
            }
        }
    }
    return;
}

+(NSMutableArray*)addABDoublewithIntArray:(NSMutableArray*)A
                          withDoubleArray:(NSMutableArray*)B
{
    
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray * ABadd =[NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue]subarrayWithRange:Row]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        ABadd[i] = [NSMutableArray  arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }

    if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
    {
      //  NSLog(@"MatrixOperate23");
        return 0;
    }
    else {
        for (int i = 0; i < [A count]; i++)
        {
            for (int j = 0; j < [A count]; j++)
            {
                ABadd[i][j] =[NSNumber numberWithDouble:(double)([A[i][j] intValue] +[B[i][j] doubleValue])];
            }
        }
    }
    return ABadd;
}

+(NSMutableArray*)addABFloatwithFloatArray:(NSMutableArray*)A
                          withIntArray:(NSMutableArray*)B
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray * ABadd =[NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue]subarrayWithRange:Row]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        ABadd[i] = [NSMutableArray  arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }

    if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
    {
     //   NSLog(@"MatrixOperate24");
        return 0;
    }
    else {
        for (int i = 0; i < [A count]; i++)
        {
            for (int j = 0; j < [A count]; j++)
            {
                ABadd[i][j] =[NSNumber numberWithFloat:(float)([A[i][j] floatValue] +[B[i][j] intValue])];
            }
        }
    }
    return ABadd;
}
+(NSMutableArray*)addArryDoublewithDoubleArray:(NSMutableArray*)arryA
                               withDoubleArray:(NSMutableArray*)arryB
{
    NSRange leng;
    leng.location = 0;
    leng.length = [arryA count];
    
    NSMutableArray * addArry = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    
    if ([arryA count] != [arryB count])
     {
       //  NSLog(@"MatrixOperate25");
         return 0;
     }
     else
     {
       for (int i = 0; i < [arryA count]; i++)
       {
           addArry[i]=[NSNumber numberWithDouble :(double)([arryA[i] doubleValue] + [arryB[i] doubleValue])];
       }
     }
     return addArry;
   }

+(void)addArrywithFloatArray:(float*)arryA WithElemCount:(int)lenOfArryA
                         withFloatArray:(float*)arryB withElemCount:(int)lenOfArryB
                   WithResultSaveInArry: (float*) addArry
{
    if (lenOfArryA != lenOfArryB)
    {
        //NSLog(@"MatrixOperate26");
        return ;
    }
    else {
        for (int i = 0; i < lenOfArryA; i++)
        {
            addArry[i]=arryA[i] + arryB[i];
        }
    }
    return;
}


+(void)normArryWithDoubleArray:(double*)arryA WithElemCount:(int)nOfArryA SavedIn:(double*)normArry WithElemCount: (int)nOfNormArry
{
    double norm = (double)sqrt([Statistics sumOfSquareDoubleArray:arryA WithElemCount:nOfArryA]);
    if (norm == 0.0)
    {
       // NSLog(@"MatrixOerate27");
        return ;
    }
    else {
       for (int i = 0; i < nOfArryA; i++)
       {
           normArry[i]=arryA[i] / norm;
       }
     }
     return ;
}

+(void)normArryWithFloatArray:(float*)arryA WithElemCount:(int)nOfArryA SavedIn:(float*)normArry WithElemCount: (int)nOfNormArry
{
    float norm = (float)sqrt([Statistics sumOfSquareFloatArray:arryA WithElemCount:nOfArryA]);
    if (norm == 0.0f)
    {
       // NSLog(@"MatrixOperate28");
        return;
    }
    else {
        for (int i = 0; i < nOfArryA; i++)
        {
            normArry[i]=  arryA[i] / norm;
        }
    }
    return ;
}

+(NSMutableArray*)minusABIntWithIntArray:(NSMutableArray*)A
                            withIntArray:(NSMutableArray*)B
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray *ABminus = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];
    
    for(NSInteger i = 0; i < [A count]; i++)
    {
        ABminus[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Col]];
    }
    
    if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
    {
        //NSLog(@"MatrixOperate29");
        return 0;
    }
    else {
        for (int i = 0; i < [A count]; i++)
        {
            for (int j = 0; j < [A count]; j++)
            {
                ABminus[i][j]= [NSNumber numberWithInt :(int)([A[i][j] intValue] - [B[i][j] intValue])];
            }
        }
    }
    return ABminus;
}

+(NSMutableArray*)minusABFloatWithFloatArray:(NSMutableArray*)A
                            withIntArray:(NSMutableArray*)B
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray *ABminus = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];

    if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
    {
       // NSLog(@"MatrixOperate30");
        return 0;
    }
    else {
        for (int i = 0; i < [A count]; i++)
        {
            for (int j = 0; j < [A count]; j++)
            {
                ABminus[i][j]= [NSNumber numberWithFloat:(float)([A[i][j] floatValue] - [B[i][j] intValue])];
            }
        }
    }
    return ABminus;
}

+(void)minusABWithFloatMatrix:(float*)A WithRow:(int)rowOfA WithCol:(int)colOfA
              withFloatMatrix:(float*)B WithRow:(int)rowOfB WithCol:(int)colOfB WithResultSavedInFloatMatrix:(float*)ABminus WithRow:(int)rowOfABminus WithCol:(int)colOfABminus
{
    if ((rowOfA != rowOfB) || (colOfA != colOfB) || (rowOfABminus != rowOfA) || (colOfABminus != colOfA))
    {
       // NSLog(@"MatrixOperate31");
        return ;
    }
    else {
        for (int i = 0; i < rowOfA; i++)
        {
            for (int j = 0; j < colOfA; j++)
            {
                *(ABminus + i * colOfABminus + j)=  *(A + i * colOfA + j) - *(B + i * colOfB + j);
            }
        }
    }
    return ;
}



+(NSMutableArray*)minusABDoubleWithDoubleArray:(NSMutableArray*)A
                               withDoubleArray:(NSMutableArray*)B
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray *ABminus = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];

    if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
    {
        //NSLog(@"MatrixOperate32");
        return 0;
    }
    else {
        for (int i = 0; i < [A count]; i++)
        {
            for (int j = 0; j < [A count]; j++)
            {
                ABminus[i][j]= [NSNumber numberWithDouble:(double)([A[i][j] doubleValue] - [B[i][j] doubleValue])];
            }
        }
    }
    return ABminus;
}

+(NSMutableArray*)minusABFloatWithIntArray:(NSMutableArray*)A
                               withFloatArray:(NSMutableArray*)B
{
    NSRange Row, Col;
    Row.location = 0;
    Row.length = [A count];
    Col.location = 0;
    Col.length = [A[0] count];
    
    NSMutableArray *ABminus = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:Row]];

    
    if (([A count] != [B count]) || ([A[0] count] != [B[0] count]))
    {
       // NSLog(@"MatrixOperate33");
        return 0;
    }
    else {
        for (int i = 0; i < [A count]; i++)
        {
            for (int j = 0; j < [A count]; j++)
            {
                ABminus[i][j]= [NSNumber numberWithFloat:(float)([A[i][j] intValue] - [B[i][j] floatValue])];
            }
        }
    }
    return ABminus;
}

+(void)minusArrytWithFloatArray:(float*)arryA WithElemCount:(int)lenOfArryA
                 withFloatArray:(float*)arryB WithElemCount:(int)lenOfArryB WIthResultSavedInFloatArray: (float*)arryMinus WithElemCount:(int)lenOfArryMinus
{
    if ((lenOfArryA != lenOfArryB) || (lenOfArryB != lenOfArryMinus))
    {
       // NSLog(@"MatrixOperate34");
        return;
    }
    else {
        for (int i = 0; i < lenOfArryA; i++)
        {
            arryMinus[i] = arryA[i] - arryB[i];
        }
    }
    return ;
}


+(NSMutableArray*)minusArryDoubleWithDoubleArray:(NSMutableArray*)arryA
                                withDoubleArray:(NSMutableArray*)arryB
{
    NSRange leng;
    leng.location = 0;
    leng.length = [arryA count];
    NSMutableArray * arryMinus = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    
    if ([arryA count] != [arryB count])
    {
     //   NSLog(@"MatrixOperate35");
        return 0;
    }
    else
    {
        for (int i=0; i<arryA.count; i++)
        {
            arryMinus[i] = [NSNumber numberWithDouble:(double)([arryA[i] doubleValue]-[arryB[i] doubleValue])];
        }
    }
    return arryMinus;
}

+(NSMutableArray*)getDiagIntWithIntArray:(NSMutableArray*)A
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A count];
    
    NSMutableArray * B = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        B[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    }
    for (int i = 0; i < [A count]; i++)
    {
       B[i][i] = A[i];
    }
    return B;
}

+(void)getDiagfloatMatrix: (float*)matrix withFloatArrayValue:(float*)A withDimension: (int) n
{
    for (int i = 0; i < n; i++)
    {
        *(matrix+i*n+i) = A[i];
    }
    return;
}
+(NSMutableArray*)getDiagDoubleWithDoubleArray:(NSMutableArray*)A
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A count];
    
    NSMutableArray * B = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    for(NSInteger i = 0; i < [A count]; i++)
    {
        B[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    }

    for (int i = 0; i < [A count]; i++)
    {
        B[i][i] = [NSNumber numberWithDouble:[A[i] doubleValue]];
    }
    return B;
}

+(void)getDiagIntMatrix: (int*)matrix WithValue:(int)a
                            withDemension:(int)n
{
    for (int i = 0; i < n; i++)
    {
       *(matrix+i*n+i) = (int)a;
    }
    return ;
}

+(NSMutableArray*)getDiagFloatWithFloat:(float)a
                            withInt:(int)n
{
    NSRange leng;
    leng.location = 0;
    leng.length = n;
    
    NSMutableArray * B = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    for(NSInteger i = 0; i < n; i++)
    {
        B[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    }
    
    for (int i = 0; i < n; i++)
    {
        B[i][i] =[NSNumber numberWithFloat:(float)a];
    }
    return B;
}

+(NSMutableArray*)getDiagDoubleWithDouble:(double)a
                                withInt:(int)n
{
    NSRange leng;
    leng.location = 0;
    leng.length = n;
    
    NSMutableArray * B = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    for(NSInteger i = 0; i < n; i++)
    {
        B[i] = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    }
    
    for (int i = 0; i < n; i++)
    {
        B[i][i] =[NSNumber numberWithDouble:(double)a];
    }
    return B;
}

+(NSMutableArray*)copyRowArryIntWithInt:(int)row
                           withIntArray:(NSMutableArray*)A
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A[0] count];
    
    NSMutableArray * oneRow = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    
     for (int i = 0; i < [A[0] count]; i++)
     {
       oneRow[i] = A[(row - 1)][i];
     }
     return oneRow;
}

+(NSMutableArray*)copyRowArryFloatWithInt:(int)row
                           withFloatArray:(NSMutableArray*)A
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A[0] count];
    
    NSMutableArray * oneRow = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];

    for (int i = 0; i < [A[0] count]; i++)
    {
        oneRow[i] = [NSNumber numberWithFloat:[A[(row - 1)][i] floatValue]];
    }
    return oneRow;
}

+(NSMutableArray*)copyRowArryDoubleWithInt:(int)row
                           withDoubleArray:(NSMutableArray*)A
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A[0] count];
    
    NSMutableArray * oneRow = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];

    for (int i = 0; i < [A[0] count]; i++)
    {
        oneRow[i] = [NSNumber numberWithDouble:[A[(row - 1)][i] doubleValue]];
    }
    return oneRow;
}

+(NSMutableArray*)copyLineArryIntWithIntArray:(NSMutableArray*)A withInt:(int)line
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A count];
    
    NSMutableArray * oneRow = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    
    for (int i=0; i < [A count]; i++) {
        oneRow[i] = [NSNumber numberWithInt:[A[i][line-1] intValue]];
    }

    return oneRow;

}

+(NSMutableArray*)copyLineArryFloatWithFloatArray:(NSMutableArray*)A withInt:(int)line
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A count];
    
    NSMutableArray * oneRow = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];

    
    for (int i=0; i<[A count]; i++) {
        oneRow[i] = [NSNumber numberWithFloat:[A[i][line-1] floatValue]];
    }
    
    return oneRow;
    
}
+(NSMutableArray*)copyLineArryDoubleWithDoubleArray:(NSMutableArray*)A withInt:(int)line
{
    NSRange leng;
    leng.location = 0;
    leng.length = [A count];
    
    NSMutableArray * oneRow = [NSMutableArray arrayWithArray:[[StaticData arrayDefaultValue] subarrayWithRange:leng]];
    
    for (int i=0; i < [A count]; i++) {
        oneRow[i] = [NSNumber numberWithDouble:[A[i][line-1] doubleValue]];
    }
    
    return oneRow;
    
}






@end
