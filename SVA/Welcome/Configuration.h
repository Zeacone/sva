//
//  Configuration.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface Configuration : NSObject

+(void)seSVMTrain:(long)SVMTrain;
+(void)setOllaAjustF:(BOOL)OllaAjustF;
+(void)setMagFilterF:(BOOL)MagFilterF;
+(void)setresetWifiDistan:(float)resetWifiDistan;
+(void)setresetWifiTimeCnt:(int)resetWifiTimeCntn;
+(NSMutableArray*)setfingerprintTotal;
+(void)sescenario:(int)scenario;
+(void)seWifiAd:(int)WifiAd;
+(void)seinertia:(int)inertia;
+(void)serandom:(int)random;
+(void)seangleOf:(int)angleOf;
+(int)angleOfYAxis;
+(void)segridF:(int)gridF;
+(void)sesampling:(int)sampling;
+(void)semagAdj:(int)magAdj;
+(void)semagAdjPer:(int)magAdjPer;
+(void)sepaticleAdj:(int)paticleAdj;
+(void)seUT_TES:(int)UT_TES;
+(void)semagVT:(float)magVT;
+(void)semagFiT:(float)magFiT;
+(void)sewifiLocDi:(float)wifiLocDi;
+(void)seminDisOfMag:(float)minDisOfMag;
+(void)semaxDisOfMag:(float)maxDisOfMag;
+(void)sedisOfLast:(float)disOfLast;
+(void)seMeanVauleDistThn:(float)MeanVauleDistThn;
+(void)seMeanVauleDistTh1:(float)MeanVauleDistTh1;
+(void)seDistAjustF:(int)DistAjustF;
+(void)seLargeAreaRes:(int)LargeAreaRes;
+(void)seSmallAreaRes:(int)SmallAreaRes;
+(void)seSmallAreaRest:(int)SmallAreaRest;
+(void)seLargenumOfMagAdjR:(int)LargenumOfMagAdjR;
+(void)seSmallnumOfMagAdjRes:(int)SmallnumOfMagAdjRes;
+(void)seLargeminTimeMa:(int)LargeminTimeMa;
+(void)seSmallminTimeMag:(int) SmallminTimeMag;
+(void)seenhancedResampl:(int)enhancedResampl;
+(void)seonlyWifiF:(int)onlyWifiF;
+(void)sevFla:(int)vFla;
+(void)seresetWifiFla:(int)resetWifiFla;
+(void)seresetWifiTimeCn:(int)resetWifiTimeCn;
+(void)semagInsteadOfWifi:(int)magInsteadOfWifi;
+(void)seKNNNu:(int)KNNNu;
+(void)semagDistanceOfW:(float)magDistanceOfW;
+(void)seresetPdrByVf:(int)resetPdrByVf;
+(void)sefrozenFla:(int)frozenFla;
+(void)sefrozenTim:(int)frozenTim;
+(void)sewifiAlphaFilterF:(int)wifiAlphaFilterF;
+(void)sedeltaFiFla:(int)deltaFiFla;

+(void)seeKNNFla:(int)eKNNFla;
+(void)seLteWifiMod:(int)LteWifiMod;
+(void)sewifi_KNN_nu:(int)wifi_KNN_nu;
+(void)seresetPaticleRadiu:(double)resetPaticleRadiu;
+(void)sevalid_ap_nu:(int)valid_ap_nu;
+(void)selogFla:(int)logFla;
+(void)seknn_dis_t:(double)knn_dis_t;
+(void)sefloorJudgeThreshol:(int)floorJudgeThreshol;
+(void)sefloorJudgeByApNumFla:(int)floorJudgeByApNumFla;
+(void)setrianglelocateFla:(int)trianglelocateFla;
+(void)seenhanceBarycentreFla:(int)enhanceBarycentreFla;
+(void)sefixPointFla:(int)fixPointFla;
+(void)sefilterFla:(int)filterFla;
+(void)sefilter2Fla:(int)filter2Fla;
+(void)sechangeX:(int)changeX;
+(NSString*)semapNam;

+(int)LargeminTimeMagAdj;
+(int) samplingFrequency;
+(int)changeXY;
+(int)resetWifiFlag;
+(int)randomFlag;
+(double)resetPaticleRadius;
+(int)deltaFiFlag;
+(int)frozenTime;
+(float)resetWifiDistance;
+(int)resetWifiTimeCntLow;
+(int)resetWifiTimeCntHigh;
+(int)paticleAdjFlag;
+(int)inertiaFlag;
+(int)WifiAdjFlag;
+(int)eKNNFlag;
+(int)enhancedResamplingFlag;











@end
