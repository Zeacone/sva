//
//  StaticData.m
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <GameplayKit/GameplayKit.h>
#import <Foundation/Foundation.h>
#import "StaticData.h"
#import "GlobalConfig.h"
#import "Paticle.h"

@implementation StaticData

static float errorAngle = 90;
static int distanceThreshold = 8;
static int ErrorLocThreshold = 5;
static int ErrorLocCount = 0;
static int StayThreshold = 3;
static int StayCount = 0;
static float intialLocX = 0.0F;
static float intialLocY = 0.0F;

static float UP_GATE = 0.8F;
static float LOW_GATE = -0.3F;
static int RF_POINT_NUM = 154;
static int AP_NUM = 5;

static float DEFAULT_RSS = -90.0F;
static int UP_INTERVAL = 20;
static int RATE = 50;
static int MAX_INDEX = 49;
static int NO_PEAK = -1;
static float STEP_LEN = 0.68F;
static float H = 1.7F;

static BOOL firstSecond = true;
static BOOL twoSecond = false;
static bool moreSecond = false;
static bool isFirstHead = true;
static NSMutableArray *acc4Low;     //Float
static int stepNum = 0;
static int last_up = 0;
static int last_low = 0;
static float aMaxLast = 0.0F;
static float aMinLast = 0.0F;

static NSMutableArray *axFielt; //float
static NSMutableArray *ayFielt;  //float
static NSMutableArray *azFielt;  //float
static NSMutableArray *gxFielt;  //float
static NSMutableArray *gyFielt;  //float
static NSMutableArray *gzFielt;  //float
static NSMutableArray *mxFielt;  //float
static NSMutableArray *myFielt;   //float
static NSMutableArray *mzFielt;  //float

static NSMutableArray *ax;    //float
static NSMutableArray *ay;    //float
static NSMutableArray *az;    //float
static NSMutableArray *gx;    //float
static NSMutableArray *gy;    //float
static NSMutableArray *gz;    //float
static NSMutableArray *mx;    //float
static NSMutableArray *my;    //float
static NSMutableArray *mz;    //float

static NSMutableArray *axMean;  //float
static NSMutableArray *ayMean;  //float
static NSMutableArray *azMean;  //float
static NSMutableArray *gxMean;  //float
static NSMutableArray *gyMean;  //float
static NSMutableArray *gzMean;  //float
static NSMutableArray *mxMean;  //float
static NSMutableArray *myMean;  //float
static NSMutableArray *mzMean;  //float

static NSMutableArray *X;      //float
static NSMutableArray *Y;      //float
static NSMutableArray *yfielt;  //float
static NSMutableArray *velCache; //float
static NSMutableArray *headCache;  //float
static NSMutableArray *headPDR; //float

static float q[4] = {0.0f, 0.0f, 0.0f, 0.0f};
static float P[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f, 0.0f},{0.0f, 0.0f, 0.0f, 0.0f}};
static int flag = 1;
static int numberofstay = 0;
static int count = 0;
static float G = 0.0f;
static int flagFielt = 0;

static int t = 0;
static int t0 = 0;

static int iii_leo = 0;

static long preTime = 0L;
static NSMutableArray *preX; //float
static NSMutableArray *preY;  //float
static NSMutableArray *preFloorIndex;  //Integer
static NSMutableArray *preWifiFloorIndex;  //Integer
static int changeFloorFlag = 0;
static int validApNum[6][2] = {{0 ,0},{0 ,0},{0 ,0},{0 ,0},{0 ,0},{0 ,0}};
static float preV = 0.0f;
static float locationWifi[3] = { 0.0f, 0.0f, 0.0f};
static float locationWifi_global[3] = {0.0f, 0.0f, 0.0f};

static float WifiResult[50][2];

static NSMutableArray  *wifiForFilter;                        //Vector<float[]> wifiForFilter
static NSMutableArray  *wifiForFilter_global;       //Vector<float[]> wifiForFilter_global

static int wifi_pdr_DistanceCnt = 0;
static int wifi_wifi_DistanceCnt = 0;

static int quiescentCnt = 0;
static int floorNum = 5;
static int calcWifiFailFlag[];  //static int calcWifiFailFlag[floorNum];

static NSMutableArray *RSSI;    //Vector<float[]>[] RSSI = new Vector[floorNum];
//static char Bssid[][];       //String[][] Bssid = new String[floorNum][];
static NSMutableArray *FingureprintXY;  //Vector<Float[]>[] FingureprintXY = new Vector[floorNum];
static char mapName[5][15] = { "U4Lab", "U3", "U4", "SHIDAITIANJIE", "WANGFUJING" };
static float deltaFi = 45.0f;
static int resetWifiTimeCnt;
static int wifiResamplingCnt = 0;
//static Lock handleLock = new ReentrantLock();
static float WifiLocation_eKnn[2] = {0.0f, 0.0f};
static int stubCnt = 0;

static int MAX_LINE_NUM = 30;
//public static MagStr[] LineData = new MagStr[MAX_LINE_NUM];
static NSMutableArray *magDbXY; //Vector<Float[]> magDbXY = new Vector();

static int LineNum = 0;
static int LineCnt[];   //int[MAX_LINE_NUM];
static int WifiAvailableNum = 0;
static int WifiTotalNum = 0;

static int WifiNum = 0;

static int paticleNum = 200;
static  NSMutableArray<Paticle*> *paticle;
static float v = 0.0f;
static float fi = 0.0f;

/**
 * 地磁定位信息
 */
static int flagforDecide = 1;
static float dataMemsMean[10] = {0.0f, 0.0f,0.0f, 0.0f,0.0f, 0.0f,0.0f, 0.0f,0.0f, 0.0f };

static  NSMutableArray<NSNumber*> *rollforAttitude;  //float
static  NSMutableArray<NSNumber*> *pitchforAttitude;  //float
static  NSMutableArray<NSNumber*> *headforAttitude;  //float
static  NSMutableArray<NSNumber*> *MODE;  //float

static float flagofAttitude = 0.0f;
static float lastMode[2] = {0.0f, 0.0f};
static int flagforAttitude = 1;

static float modePrint[2] = {0.0f, 0.0f};

static int noReadingModeCount = 0;

static NSMutableArray<NSMutableArray*> *locationForFilter; //float[]

static NSMutableArray<NSMutableArray*> *Q_Vector;  //float[]
static NSMutableArray<NSMutableArray*> *Q_Vector_temp;  //float[]
static NSMutableArray *Mag_Init_Temp; //float[]
static NSMutableArray<NSMutableArray*> *Mag_Init;  //float[]
static bool Q_VectorExist = false;
static bool AjustSuccFlag = false;
static int AjustfaileCnt = 0;

static int numOfMagAdjResult = 3;
static int minTimeMagAdj = 5;
static NSMutableArray<NSString*> *LteRruid;
static NSMutableArray *LteRssi;

static float Xopt[4] = {0.0f, 0.0f, 0.0f, 0.0f};
static float Popt[4][4] = {{0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f}};
static int flagEkf = 1;

static NSMutableArray *wiFiDataCache;

static NSMutableArray *wiFiDataBssidCache;

static NSMutableArray *history_fi;   //foat
static int history_fi_index = 0;

static int lock = 1;

static int dB[];  //new int[AP_NUM];
static float mW[];  // new float[AP_NUM];


static int inputDataIndex = 0;

static float inputData[3600];
static NSArray *arrayDefaultValue;
static NSArray *arraydefaultIntValue;

+(NSArray*)arrayDefaultValue
{
    return arrayDefaultValue;
}
+(NSArray*)arraydefaultIntValue
{
    return arraydefaultIntValue;
}

+(int)floorNum
{
    return floorNum;
}



+ (void)setDefaultValue
{
    arrayDefaultValue = @[@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f),@(0.0f)];
    arraydefaultIntValue = @[@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0),@(0)];
    
    for(int i = 0; i < 6; i++){
        for(int j = 0; j < 2; j++){
            validApNum[i][j] = 0;
        }
    }
  
    for(int i = 0; i <50; i++){
        for(int j = 0; j < 2; j++){
            WifiResult[i][j] = 0.0f;
        }
    }
    
    for(int i = 0;  i < floorNum;  i++)
    {
        *(calcWifiFailFlag + i) = 0;
    }
    
    
    resetWifiTimeCnt = [[GlobalConfig moreMaxDeviate] intValue];
  
    //  public static Lock handleLock = new ReentrantLock();
   

    /* 132 */   //public static MagStr[] LineData = new MagStr[MAX_LINE_NUM]; ????
    for(int i = 0; i < MAX_LINE_NUM; i++)
        *(LineCnt + i) = 0 ;
    

    
    //  public static Vector<String> LteRruid = new Vector();

    for(int i = 0; i < AP_NUM; i++)
    {
        *(dB + i) = 0;
        *(mW + i) = 0.0f;
    }

    for(int i = 0; i < 3600; i++){
        inputData[i] = 0.0f;
    }
    
    //GKRandomDistribution *r = [[GKRandomDistribution alloc] init];

    
}



+(void)seAjustfaileCn:(int)AjustfaileCn
{
    AjustfaileCnt = AjustfaileCn;
    
    return ;
}

+(NSMutableArray*)seRSSI
{
    if(!RSSI)
    {
        RSSI = [NSMutableArray array];
    }
    return RSSI;
}

+(void)seAjustSuccFla:(bool)AjustSuccFla
{
    AjustSuccFlag = AjustSuccFla;
    
    return ;
}
+(bool)seQ_VectorExis:(bool)Q_VectorExis
{
    Q_VectorExist = Q_VectorExis;
    
    return Q_VectorExist;
}
+(BOOL) Q_VectorExist
{
    return Q_VectorExist;
}

+(void)sefi0:(float)fi0
{
    fi = fi0;
  //  NSLog(@"Fi changeto: %f", fi);
    
    return ;
    
    
}
+(float)fi
{
    return fi;
}

+(void)sev0:(float)v0
{
    v = v0;
    NSLog(@"v changeto: %f", v);
    
    return ;
    
    
}
+(float)v
{
    return v;
}

+(float)sepreV0:(float)preV0
{
    preV = preV0;
    
    return preV;
    
    
}
+(float)preV
{
    return preV;
}

+(void)seWifiNu:(int)WifiNu
{
    WifiNum = WifiNu;
    
    return;
    
    
}
+(void)sestubCnt0:(int)stubCnt0
{
    stubCnt = stubCnt0;
    
    return ;
    
    
}
+(void)seMAX_LINE_NUM0:(int)MAX_LINE_NUM0
{
    MAX_LINE_NUM = MAX_LINE_NUM0;
    
    return;
    
    
}









+(void)seLineNum0:(int)LineNum0
{
    LineNum = LineNum0;
    
    return;
    
    
}
+(void)sepaticleNu:(int)paticleNu
{
    paticleNum = paticleNu;
    
    return ;
    
    
}

+(int)paticleNum
{
    return paticleNum;
}

+(void)seInputDataIndex:(int)inputDataIndex
{
    inputDataIndex = inputDataIndex;
    
    return ;
}

+(void)seInputData:(float)value AtIndex:(int)index
{
    inputData[index] = value;
    return ;
}


+(void)seLineCn: (int)value AtIndex:(int)index
{
    LineCnt[index] = value;
    return ;
    
    
}


+(NSMutableArray*)seQ_Vector_te
{
    if(!Q_Vector_temp)
    {
         Q_Vector_temp = [NSMutableArray array];
    }
    return Q_Vector_temp;
}

+(void)Q_Vector_tempAddObject: (NSMutableArray*)arry
{
    if(!Q_Vector_temp)
    {
        Q_Vector_temp = [NSMutableArray array];
    }
    [Q_Vector_temp addObject:arry];
}
+(NSMutableArray*)Q_Vector_temp
{
    if(!Q_Vector_temp)
    {
        Q_Vector_temp = [NSMutableArray array];
    }
    return Q_Vector_temp;
}

+(void)seUPGATE:(float)UPGATE
{
    UP_GATE =UPGATE;
    return ;
}

+(float)UP_GATE
{
    return UP_GATE;
}

+(void)seLOW_GAT:(float)LOW_GAT
{
    LOW_GATE =LOW_GAT;
     return ;
}



+(void)seRFPOINTNUM:(int)RFPOINTNUM
{
    RF_POINT_NUM =RFPOINTNUM;
     return ;
}

+(void)seAP_NU:(int)AP_NU
{
    AP_NUM =AP_NU;
     return;
}
+(void)seDEFAULT_R:(float)DEFAULT_R
{
    DEFAULT_RSS =DEFAULT_R;
     return ;
}
+(void)seUP_INTERV:(int)UP_INTERV
{
    UP_INTERVAL =UP_INTERV;
     return ;
}

+(int) UP_INTERVAL
{
    return UP_INTERVAL;
}

+(void)seMAX_INDE:(int)MAX_INDE
{
    MAX_INDEX =MAX_INDE;
    return ;
}

+(int) MAX_INDEX
{
    return MAX_INDEX;
}

+(void)seNO_PEA:(int)NO_PEA
{
    NO_PEAK =NO_PEA;
     return;
}

+(int) NO_PEAK
{
    return NO_PEAK;
}

+(void)seSTEP_LE:(float)STEP_LE
{
    STEP_LEN =STEP_LE;
     return ;
}

+(void)seh:(float)h
{
    H =h;
     return;
}

+(float)H
{
    return H;
}
    
+(void)sefirstSeco:(BOOL)firstSeco
{
    firstSecond =firstSeco;
    
 return ;
    
}
+(BOOL) firstSecond
{
    return firstSecond;
}

+(void)setwoSeco:(BOOL)twoSeco
{
    twoSecond =twoSeco;
    
    return;

}

+(BOOL)twoSecond
{
    return twoSecond;
}

+(void)semoreSeco:(BOOL)moreSeco
{
    moreSecond =moreSeco;
    
 return;
    
}

+(BOOL)moreSecond
{
    return moreSecond;
}

+(void)seisFirstHe:(BOOL)isFirstHe
{
    isFirstHead =isFirstHe;
    return ;
}

+(BOOL)isFirstHead
{
    return isFirstHead;
}

+(NSMutableArray*)seacc4Lo
{
    if(!acc4Low)
    {
        acc4Low = [NSMutableArray array];
    }

    return acc4Low;
}
    
+(NSMutableArray*)acc4Low
{
    if(!acc4Low)
    {
        acc4Low = [NSMutableArray array];
    }
    
    return acc4Low;
}
    
+(void)sestepNu:(int)stepNu
{
    stepNum =stepNu;
    
 return ;
}
+(int) stepNum
{
    return stepNum;
}

+(void)selast_u:(int)last_u
{
    last_up =last_u;
     return ;
}

+(int)last_up
{
    return last_up;
}

+(void)selast_lo:(int)last_lo
{
    last_low =last_lo;
    
 return ;
    
}
+(void)seaMaxLas:(float)aMaxLas
{
    aMaxLast =aMaxLas;
     return ;
}

+(float)aMaxLast
{
    return aMaxLast;
}

+(void)seaMinLas:(float)aMinLas
{
    aMinLast =aMinLas;
    
 return ;
    
}

+(float)aMinLast
{
    return aMinLast;
}

+(NSMutableArray*)seaxFiel
{
    if(!axFielt)
    {
        axFielt = [NSMutableArray array];
    }
    return axFielt;
    
}

+(NSMutableArray*) axFielt
{
    return axFielt;
}

+(float)axFieltAtIndex:(int)index
{
    return [[axFielt objectAtIndex:index] floatValue];
}

+(void)axFielt_AddObject:(float)value
{
    if(!axFielt)
    {
        axFielt = [NSMutableArray array];
    }
    [axFielt addObject:[NSNumber numberWithFloat:value]];
}
+(NSMutableArray*)seayFiel
{
    if(!ayFielt)
    {
        ayFielt = [NSMutableArray array];
    }
    return ayFielt;
}

+(NSMutableArray*) ayFielt
{
    if(!ayFielt)
    {
        ayFielt = [NSMutableArray array];
    }
    return ayFielt;
}

+(void)ayFielt_AddObject:(float)value
{
    if(!ayFielt)
    {
        ayFielt = [NSMutableArray array];
    }
    [ayFielt addObject:[NSNumber numberWithFloat:value]];
}


+(float)ayFieltAtIndex:(int)index
{
    return [[ayFielt objectAtIndex:index] floatValue];
}
+(NSMutableArray*)seazFiel
{
    if(!azFielt)
    {
        azFielt = [NSMutableArray array];
    }
    return azFielt;
}

+(NSMutableArray*)azFielt
{
    if(!azFielt)
    {
        azFielt = [NSMutableArray array];
    }
    return azFielt;
}

+(float)azFieltAtIndex:(int)index
{
    return [[azFielt objectAtIndex:index] floatValue];
}

+(void)azFielt_AddObject:(float)value
{
    if(!azFielt)
    {
        azFielt = [NSMutableArray array];
    }
    [azFielt addObject:[NSNumber numberWithFloat:value]];
}

+(NSMutableArray*)segxFiel
{
    if(!gxFielt)
    {
        gxFielt = [NSMutableArray array];
    }

    return gxFielt;
}

+(NSMutableArray*)gxFielt
{
    if(!gxFielt)
    {
        gxFielt = [NSMutableArray array];
    }
    
    return gxFielt;
}

+(float)gxFieltAtIndex:(int)index
{
    return [[gxFielt objectAtIndex:index] floatValue];
}

+(void)gxFielt_AddObject:(float)value
{
    if(!gxFielt)
    {
        gxFielt = [NSMutableArray array];
    }
    [gxFielt addObject:[NSNumber numberWithFloat:value]];
}

+(NSMutableArray*)segyFiel
{
    if(!gyFielt)
    {
        gyFielt = [NSMutableArray array];
    }
    return gyFielt;
}

+(NSMutableArray*) gyFielt
{
    if(!gyFielt)
    {
        gyFielt = [NSMutableArray array];
    }
    return gyFielt;
}

+(float)gyFieltAtIndex:(int)index
{
    return [[gyFielt objectAtIndex:index] floatValue];
}

+(void)gyFielt_AddObject:(float)value
{
    if(!gyFielt)
    {
        gyFielt = [NSMutableArray array];
    }
    [gyFielt addObject:[NSNumber numberWithFloat:value]];
}


+(NSMutableArray*)segzFiel
{
    if(!gzFielt)
    {
        gzFielt = [NSMutableArray array];
    }
    return gzFielt;
}

+(NSMutableArray*) gzFielt
{
    if(!gzFielt)
    {
        gzFielt = [NSMutableArray array];
    }
    return gzFielt;
}

+(void)gzFielt_AddObject:(float)value
{
    if(!gzFielt)
    {
        gzFielt = [NSMutableArray array];
    }
    [gzFielt addObject:[NSNumber numberWithFloat:value]];
}

+(float)gzFieltAtIndex:(int)index
{
    return [[gzFielt objectAtIndex:index] floatValue];
}

+(NSMutableArray*)semxFiel
{
    if(!mxFielt)
    {
        mxFielt = [NSMutableArray array];
    }
    return mxFielt;
}

+(NSMutableArray*) mxFielt
{
    if(!mxFielt)
    {
        mxFielt = [NSMutableArray array];
    }
    return mxFielt;
}

+(float)mxFieltAtIndex:(int)index
{
    return [[mxFielt objectAtIndex:index] floatValue];
}

+(void)mxFielt_AddObject:(float)value
{
    if(!mxFielt)
    {
        mxFielt = [NSMutableArray array];
    }
    [mxFielt addObject:[NSNumber numberWithFloat:value]];
}

+(NSMutableArray*)semyFiel
{
    if(!myFielt)
    {
        myFielt = [NSMutableArray array];
    }
    return myFielt;
}

+(NSMutableArray*) myFielt
{
    if(!myFielt)
    {
        myFielt = [NSMutableArray array];
    }
    return myFielt;
}
+(float)myFieltAtIndex:(int)index
{
    return [[myFielt objectAtIndex:index] floatValue];
}

+(void)myFielt_AddObject:(float)value
{
    if(!myFielt)
    {
        myFielt = [NSMutableArray array];
    }
    [myFielt addObject:[NSNumber numberWithFloat:value]];
}


+(NSMutableArray*)semzFiel
{
    if(!mzFielt)
    {
        mzFielt = [NSMutableArray array];
    }
    return mzFielt;
}

+(float)mzFieltAtIndex:(int)index
{
    return [[mzFielt objectAtIndex:index] floatValue];
}

+(void)mzFielt_AddObject:(float)value
{
    if(!mzFielt)
    {
        mzFielt = [NSMutableArray array];
    }
    [mzFielt addObject:[NSNumber numberWithFloat:value]];
}


+(NSMutableArray*) mzFielt
{
    if(!mzFielt)
    {
        mzFielt = [NSMutableArray array];
    }
    return mzFielt;
}

+(NSMutableArray*)seax1
{
    if(!ax)
    {
        ax = [NSMutableArray array];
    }
    return ax;
}

+ (NSMutableArray*) ax1
{
    if(!ax)
    {
        ax = [NSMutableArray array];
    }
    return ax;
}

+(NSMutableArray*)seay1
{
    if(!ay)
    {
        ay = [NSMutableArray array];
    }
    return ay;
}

+(NSMutableArray*) ay1{
    if(!ay)
    {
        ay = [NSMutableArray array];
    }
    return ay;
}

+(NSMutableArray*)seaz
{
    if(!az)
    {
        az = [NSMutableArray array];
    }
    return az;
}

+(NSMutableArray*) az1{
    if(!az)
    {
        az = [NSMutableArray array];
    }
    return az;
}


+(NSMutableArray*) segx1
{
    if(!gx)
    {
        gx = [NSMutableArray array];
    }
    return gx;
}

+(NSMutableArray*) gx1{
    if(!gx)
    {
        gx = [NSMutableArray array];
    }
    return gx;
}

+(NSMutableArray*) segy1
{
    if(!gy)
    {
        gy = [NSMutableArray array];
    }
    return gy;
}

+(NSMutableArray*) gy1{
    if(!gy)
    {
        gy = [NSMutableArray array];
    }
    return gy;
}


+(NSMutableArray*)segz1
{
    if(!gz)
    {
        gz = [NSMutableArray array];
    }
    return gz;
}

+(NSMutableArray*) gz1{
    if(!gz)
    {
        gz = [NSMutableArray array];
    }
    return gz;
}

+(NSMutableArray*)semx1
{
    if(!mx)
    {
        mx = [NSMutableArray array];
    }
    return mx;
}

+(NSMutableArray*) mx1
{
    if(!mx)
    {
        mx = [NSMutableArray array];
    }
    return mx;
}

+(NSMutableArray*)semy1
{
    if(!my)
    {
        my = [NSMutableArray array];
    }
    return my;
}

+(NSMutableArray*) my1
{
    if(!my)
    {
        my = [NSMutableArray array];
    }
    return my;
}

+(NSMutableArray*)semz1
{
    if(!mz)
    {
        mz = [NSMutableArray array];
    }
    return mz;
}

+(NSMutableArray*) mz1{
    if(!mz)
    {
        mz = [NSMutableArray array];
    }
    return mz;
}

+(NSMutableArray*)seaxMean1
{
    if(!axMean)
    {
        axMean = [NSMutableArray array];
    }
    return axMean;
}

+(NSMutableArray*)axMean1
{
    if(!axMean)
    {
        axMean = [NSMutableArray array];
    }
    return axMean;
}

+(NSMutableArray*)seayMean1
{
    if(!ayMean)
    {
        ayMean = [NSMutableArray array];
    }
    return ayMean;
}
    
+(NSMutableArray*) ayMean1
{
    if(!ayMean)
    {
        ayMean = [NSMutableArray array];
    }
    return ayMean;
}
    
    
+(NSMutableArray*)seazMean1
{
    if(!azMean)
    {
        azMean = [NSMutableArray array];
    }
    return azMean;
}

+(NSMutableArray*) azMean1
{
    if(!azMean)
    {
        azMean = [NSMutableArray array];
    }
    return azMean;
}

+(NSMutableArray*)segxMean1
{
    if(!gxMean)
    {
        gxMean = [NSMutableArray array];
    }
    return gxMean;
}

+(NSMutableArray*) gxMean1{
    if(!gxMean)
    {
        gxMean = [NSMutableArray array];
    }
    return gxMean;
}

+(NSMutableArray*)segyMean1
{
    if(!gyMean)
    {
        gyMean = [NSMutableArray array];
    }
    return gyMean;
}

+(NSMutableArray*) gyMean1
{
    if(!gyMean)
    {
        gyMean = [NSMutableArray array];
    }
    return gyMean;
}

+(NSMutableArray*)segzMean1
{
    if(!gzMean)
    {
        gzMean = [NSMutableArray array];
    }
    return gzMean;
}

+(NSMutableArray*) gzMean1
{
    if(!gzMean)
    {
        gzMean = [NSMutableArray array];
    }
    return gzMean;
}

+(NSMutableArray*)semxMean1
{
    if(!mxMean)
    {
        mxMean = [NSMutableArray array];
    }
    return mxMean;
}

+(NSMutableArray*) mxMean1
{
    if(!mxMean)
    {
        mxMean = [NSMutableArray array];
    }
    return mxMean;
}

+(NSMutableArray*)semyMean1
{
    if(!myMean)
    {
        myMean = [NSMutableArray array];
    }
    return myMean;
}

+(NSMutableArray*) myMean1
{
    if(!myMean)
    {
        myMean = [NSMutableArray array];
    }
    return myMean;
}

+(NSMutableArray*)semzMean1
{
    if(!mzMean)
    {
        mzMean = [NSMutableArray array];
    }
    return mzMean;
}

+(NSMutableArray*) mzMean1
{
    if(!mzMean)
    {
        mzMean = [NSMutableArray array];
    }
    return mzMean;
}

+(NSMutableArray*)seX1
{
    if(!X)
    {
        X = [NSMutableArray array];
    }
    return X;
}

+(NSMutableArray*)seY1
{
    if(!Y)
    {
        Y = [NSMutableArray array];
    }
    return Y;
}

+(NSMutableArray*)seyfielt1
{
    if(!yfielt)
    {
        yfielt = [NSMutableArray array];
    }
    return yfielt;
}

+(NSMutableArray*)sevelCache1
{
    if(!velCache)
    {
        velCache = [NSMutableArray array];
    }
    return velCache;
}

+(NSMutableArray*)seheadCache1
{
    if(!headCache)
    {
        headCache = [NSMutableArray array];
    }
    return headCache;
}

+(NSMutableArray*)headCache1
{
    if(!headCache)
    {
        headCache = [NSMutableArray array];
    }
    return headCache;
}

+(NSMutableArray*)seheadPDR1{
    if(!headPDR)
    {
        headPDR = [NSMutableArray array];
    }
    return headPDR;
}

+(void)sefla:(int)fla
{
    flag =fla;
    
 return ;
    
}

+(void)senumberof:(int)numberof
{
    numberofstay =numberof;
     return ;
    
}

+(void)secoun:(int)coun
{
    count =coun;
    return ;
}

+(int) coun
{
    return count;
}

+(void)seg:(float)g
{
    G =g;
    
 return;
    
}
+(float) G
{
    return G;
}
+(void)seflagFie:(int)flagFie
{
    flagFielt =flagFie;
    
       return;
    
}
+(int) flagFielt
{
    return flagFielt;
}
+(void)seT:(int)T
{
    t =T;
    
 return;
    
}
+(void)seT0:(int)T0
{
    t0 =T0;
    
 return;
    
}
+(void)seiii_le:(int)iii_le
{
    iii_leo =iii_le;
    
 return ;
    
}
+(void)sepreTim:(long)preTim
{
    preTime =preTim;
     return ;
    
}
+(NSMutableArray*)sepreX
{
    if(!preX)
    {
        preX = [NSMutableArray array];
    }
    return preX;
}

+(NSMutableArray*) preX
{
    if(!preX)
    {
        preX = [NSMutableArray array];
    }
    return preX;
}

+(float) preXLastObject
{
    if(!preX)
        return [[preX lastObject] floatValue];
    return -1;
}

+(NSMutableArray*)sepreY1
{
    if(!preY)
    {
        preY = [NSMutableArray array];
    }
    return preY;
}

+(NSMutableArray*)preY
{
    if(!preY)
    {
        preY = [NSMutableArray array];
    }
    return preY;
}

+(float)preYLastObject
{
    if(!preY)
    {
        return [[preY lastObject] floatValue];
    }
    return -1;
}

+(NSMutableArray*)sepreFloorInde
{
    if(!preFloorIndex)
    {
        preFloorIndex = [NSMutableArray array];
    }
    return preFloorIndex;
}

+(NSMutableArray*)preFloorIndex
{
    if(!preFloorIndex)
    {
        preFloorIndex = [NSMutableArray array];
    }
    return preFloorIndex;
}

+(NSMutableArray*)sepreWifiFloorInde
{
    if(!preWifiFloorIndex)
    {
        preWifiFloorIndex = [NSMutableArray array];
    }
    return preWifiFloorIndex;
}

+(NSMutableArray*)preWifiFloorIndex
{
    if(!preWifiFloorIndex)
    {
        preWifiFloorIndex = [NSMutableArray array];
    }
    return preWifiFloorIndex;
}

+(void)sechangeFloorFla:(int)changeFloorFla
{
    changeFloorFlag =changeFloorFla;
     return ;
}

+(int)changeFloorFlag
{
    return changeFloorFlag;
}

+ (NSMutableArray*) serollforAttitude
{
    if(!rollforAttitude)
    {
        rollforAttitude = [NSMutableArray array];
    }
    return rollforAttitude;
}
+(void)rollforAttitude_AddObject:(float)value
{
    if(!rollforAttitude)
    {
        rollforAttitude = [NSMutableArray array];
    }
    [rollforAttitude  addObject:[NSNumber numberWithFloat:value]];
}

+(void)rollforAttitude_RemoveObjectAtIndex:(int)index
{
    if((!rollforAttitude)&&(index < [rollforAttitude count]))
    {
        [rollforAttitude removeObjectAtIndex: index];
    }
}
+(NSMutableArray*) sepitchforAttitude
{
    if(!pitchforAttitude)
    {
        pitchforAttitude = [NSMutableArray array];
    }
    return pitchforAttitude;
}
+(void)pitchforAttitude_AddObject:(float)value
{
    if(!pitchforAttitude)
    {
        pitchforAttitude = [NSMutableArray array];
    }
    [pitchforAttitude  addObject:[NSNumber numberWithFloat:value]];
}
+(void)pitchforAttitude_RemoveObjectAtIndex:(int)index
{
    if((!pitchforAttitude)&&(index < [pitchforAttitude count]))
    {
        [pitchforAttitude removeObjectAtIndex: index];
    }
}
+(NSMutableArray*) seheadforAttitude
{
    if(!headforAttitude)
    {
        headforAttitude = [NSMutableArray array];
    }
    return headforAttitude;
}
+(void)headforAttitude_AddObject:(float)value
{
    if(!headforAttitude)
    {
        headforAttitude = [NSMutableArray array];
    }
    [headforAttitude  addObject:[NSNumber numberWithFloat:value]];
}

+(void)headforAttitude_RemoveObjectAtIndex:(int)index
{
    if((!headforAttitude)&&(index < [headforAttitude count]))
    {
        [headforAttitude removeObjectAtIndex: index];
    }
}

+(NSMutableArray*) headforAttitude
{
    if(!headforAttitude)
    {
        headforAttitude = [NSMutableArray array];
    }
    return headforAttitude;
}

+(NSMutableArray*) pitchforAttitude
{
    if(!pitchforAttitude)
    {
        pitchforAttitude = [NSMutableArray array];
    }
    return pitchforAttitude;
}

+(NSMutableArray*) rollforAttitude
{
    if(!rollforAttitude)
    {
        rollforAttitude = [NSMutableArray array];
    }
    return rollforAttitude;
}

+(void) sehistory_fi_index: (int)newValue
{
    history_fi_index = newValue;
    return ;
}

+(int)history_fi_index
{
    return history_fi_index;
}

+ (void)clearField{
    if (gy!= nil) {
        [StaticData resetAll];
    }
}
+ (void)resetAll{
    [axFielt removeAllObjects];
    [ayFielt removeAllObjects];
    [azFielt removeAllObjects];
    
    [gxFielt removeAllObjects];
    [gyFielt removeAllObjects];
    [gzFielt removeAllObjects];
    
    [mxFielt removeAllObjects];
    [myFielt removeAllObjects];
    [mzFielt removeAllObjects];
    
    [ax removeAllObjects];
    [ay removeAllObjects];
    [az removeAllObjects];
    [gx removeAllObjects];
    [gy removeAllObjects];
    [gz removeAllObjects];
    [mx removeAllObjects];
    [my removeAllObjects];
    [mz removeAllObjects];
    
    [axMean removeAllObjects];
    [ayMean removeAllObjects];
    [azMean removeAllObjects];
    [gxMean removeAllObjects];
    [gyMean removeAllObjects];
    [gzMean removeAllObjects];
    [mxMean removeAllObjects];
    [myMean removeAllObjects];
    [mzMean removeAllObjects];
    
    [X removeAllObjects];
    [Y removeAllObjects];
    [yfielt removeAllObjects];
    [headCache removeAllObjects];
    [history_fi removeAllObjects];
    [wiFiDataCache removeAllObjects];
     for (int i = 0; i < 10; i++) {
         WifiResult[i][0] = 0.0;
         WifiResult[i][1] = 0.0;
     }
     wifi_pdr_DistanceCnt = 0;
     wifi_wifi_DistanceCnt = 0;
     quiescentCnt = 0;
     [wiFiDataBssidCache removeAllObjects];
     WifiAvailableNum = 0;
     WifiTotalNum = 0;
     history_fi_index = 0;

    [wifiForFilter removeAllObjects];
     [wifiForFilter_global removeAllObjects];
     deltaFi = 0.0;
     resetWifiTimeCnt = 5;
     wifiResamplingCnt = 0;
    WifiLocation_eKnn[0] = 0.0;
    WifiLocation_eKnn[1] = 0.0;
     changeFloorFlag = 0;

     for (int i = 0; i < floorNum; i++) {
         calcWifiFailFlag[i] = 0;
     }

     [locationForFilter removeAllObjects];
     [acc4Low removeAllObjects];

     flag = 1;
     numberofstay = 0;
     count = 0;
     G = 0.0F;
     flagFielt = 0;
     flagEkf = 1;
     lock = 1;
     t = 0;
     t0 = 0;
     stepNum = 0;
     last_up = 0;
     last_low = 0;
     aMaxLast = 0.0F;
     aMinLast = 0.0F;
     paticle=nil;
     firstSecond = true;
     twoSecond = false;
     moreSecond = false;
     isFirstHead = true;
    
    
    
     for (int i = 0; i < 4; i++) {
         q[i] =0.0;
         Xopt[i] =0.0;
       for (int j = 0; j < 4; j++) {
           P[i][j] =0.0;
           Popt[i][j] =0.0;
       }
     }
     for (int i = 0; i < AP_NUM; i++) {
         dB[i] = 0;
         mW[i] = 0.0f;
     }

     numOfMagAdjResult = 3;
     minTimeMagAdj = 5;
     flagforDecide = 1;
     [rollforAttitude removeAllObjects];
     [pitchforAttitude removeAllObjects];
     [headforAttitude removeAllObjects];
     [MODE removeAllObjects];
     flagofAttitude = 0.0;
     flagforAttitude = 1;
     for (int i = 0; i < 2; i++) {
         lastMode[i] = 0.0;
         
     }
    
     for (NSInteger i = 0; i < sizeof(dataMemsMean)/sizeof(dataMemsMean[0]); i++) {
         dataMemsMean[i] = 0.0;
     }
    modePrint[0] = 0.0;
    modePrint[1] = 0.0;
 
     noReadingModeCount = 0;
}

+ (void) seflagforDecide: (int)n
{
    flagforDecide = n;
    return;
}

+ (int) flagforDecide
{
    return flagforDecide;
}

+ (NSMutableArray*) seMODE
{
    if(!MODE)
    {
        MODE = [NSMutableArray array];
    }
   // NSLog(@"Mode count: %li",[MODE count]);
    return MODE;
}
+(void)MODE_AddObject:(float)value
{
    if(!MODE)
    {
        MODE = [NSMutableArray array];
    }
    [MODE addObject: [NSNumber numberWithFloat:value]];
    return;
}
+ (NSMutableArray*) MODE
{
    if(!MODE)
    {
        MODE = [NSMutableArray array];
    }
    return MODE;
}

+(void)sedataMemsMean: (float)value AtIndex: (int)index
{
    dataMemsMean[index] = value;
    return ;
}

+(float) dataMemsMeanAtIndex:(int)index
{
    return dataMemsMean[index];
}


+(void)seMag_Init_TempAddObject:newObject
{
    if(!Mag_Init_Temp)
    {
        [Mag_Init_Temp addObject:newObject];
    }
    return;
}

+(NSMutableArray*) seMag_Init_Temp
{
    if(!Mag_Init_Temp)
    {
        Mag_Init_Temp = [[NSMutableArray alloc] init];
    }
    return Mag_Init_Temp;
}
+(void)Mag_Init_Temp_AddObjectArray:(float*)arry WithElemCount:(int)lenOfArry
{
    if(!Mag_Init_Temp)
    {
        Mag_Init_Temp = [[NSMutableArray alloc] init];
    }
    NSMutableArray *temArry = [NSMutableArray arrayWithCapacity:lenOfArry];
    for(int i = 0; i < lenOfArry; i++)
    {
        [temArry addObject:[NSNumber numberWithFloat:arry[i]]];
    }
    [Mag_Init_Temp addObject:temArry];
}

+(NSMutableArray*) Mag_Init_Temp
{
    if(!Mag_Init_Temp)
    {
        Mag_Init_Temp = [NSMutableArray array];
    }
    return Mag_Init_Temp;
}

+(NSMutableArray*) seMag_Init
{
    if(!Mag_Init)
    {
        Mag_Init = [NSMutableArray array];
    }
    return Mag_Init;
}

+(NSMutableArray*) Mag_Init
{
    if(!Mag_Init)
    {
        Mag_Init = [NSMutableArray array];
    }
    return Mag_Init;
}


+(void) seq: (float)value AtIndex: (int)index
{
    q[index] = value;
}
+(int)q_count
{
    return sizeof(q)/sizeof(q[0]);
}

+(void)seq:(float*)newQ WithElemCount:(int)lenOfNewQ
{
    if( lenOfNewQ != 4)
    {
        return;
    }
    for(int i=0; i < 4; i++)
        q[i] = newQ[i];
}

+(float) qAtIndex:(int)index
{
    if (index < 4){
        return q[index];
    }
    else{
        return -1;
    }
}

+(float *) q
{
    return q;
}

+(float) flagofAttitude
{
    return flagofAttitude;
}

+(void) seflagofAttitude: (float) value
{
    flagofAttitude = value;
    return ;
}

+(void) selastMode: (float)value AtIndex: (int)index
{
    lastMode[index] = value;
    return;
}

+(float*) lastMode
{
    return lastMode;
}

+(void) semodePrint:(float) value AtIndex:(int)index
{
    modePrint[index] = value;
    return ;
}

+(float *) modePrint
{
    return modePrint;
}

+(void) seP:(float)newvalue AtRow:(int)row AtCol:(int)col
{
    P[row][col] = newvalue;
    return ;
}
+(int)P_RowCount
{
    return sizeof(P)/sizeof(P[0]);
}
+(int)P_ColCount
{
    return sizeof(P[0])/sizeof(P[0][0]);
}

+(float*) P
{
    return &P[0][0];
}

+(NSMutableArray*) seQ_Vector
{
    if(!Q_Vector)
    {
        Q_Vector = [NSMutableArray array];
    }
    return Q_Vector;
}

+(NSMutableArray*) Q_Vector
{
    if(!Q_Vector)
    {
        Q_Vector = [NSMutableArray array];
    }
    return Q_Vector;
}
+(int)Xopt_count
{
    return sizeof(Xopt)/sizeof(Xopt[0]);
}
+(float*)xopt
{
    return &Xopt[0];
}

+(void) seXopt: (float)value AtIndex: (int)index
{
    Xopt[index] = value;
    return ;
}

+(float) XoptAIndex:(int)index
{
    return Xopt[index];
}
+(void)setXopt:(float*)newArray withElemCount:(int)lenOfNewArray
{
    if(lenOfNewArray > sizeof(Xopt)/sizeof(Xopt[0]))
    {
        return;
    }
    for(int i = 0; i < lenOfNewArray; i++ )
    {
        Xopt[i] = newArray[i];
    }
}
+(NSMutableArray*) seyfielt
{
    if (!yfielt) {
        yfielt = [NSMutableArray array];
    }
    return yfielt;
}

+(NSMutableArray*) yfielt
{
    if (!yfielt) {
        yfielt = [NSMutableArray array];
    }
    return yfielt;
}

+(void) seRATE: (int) newRate
{
    RATE = newRate;
    return ;
}

+(int) RATE
{
    return RATE;
}

+(void) seflagEkf: (int)newValue
{
    flagEkf = newValue;
    return ;
}

+(int)flagEkf
{
    return flagEkf;
}

+(void)sequiescentCnt: (int) newValue
{
    quiescentCnt = newValue;
    return ;
}

+(int)quiescentCnt
{
    return quiescentCnt;
}

+(void) sePopt: (float)value AtRow: (int)row AtCol: (int)col
{
    Popt[row][col] = value;
    return;
}

+(float) PoptAtRow: (int)row AtCol:(int)col
{
    return Popt[row][col];
}
+(void)setPopt:(float*)newMatrix withRow:(int)row WithCol:(int)col
{
    for(int i = 0; i < row; i++ )
    {
        for(int j = 0; j < col; j++)
        Popt[i][j] = *(newMatrix + i*col +j);
    }
}

+(float*) Popt
{
    return &Popt[0][0];
}
+(NSMutableArray*)sepaticle
{
    if(!paticle)
    {
        paticle = [NSMutableArray array];
    }
    return paticle;
}

+(NSMutableArray*) paticle
{
    if(!paticle)
    {
        paticle = [NSMutableArray array];
    }
    return paticle;
}

+(void) selocationWifi:(float)value AtIndex: (int)index
{
    locationWifi[index] = value;
    return ;
}

+(float) locationWifiAtIndex:(int)index
{
    return locationWifi[index];
}

+(void) senoReadingModeCount: (int)newValue
{
    noReadingModeCount = newValue;
    return ;
}

+(int) noReadingModeCount
{
    return noReadingModeCount;
}

+(void)selocationWifi_global: (float)value AtIndex:(int)index
{
    locationWifi_global[index] = value;
    return ;
}

+(float)locationWifi_globalAtIndex:(int)index
{
    return locationWifi_global[index];
}

+(void)sedeltaFi: (float)newdeltaFi
{
    deltaFi = newdeltaFi;
    return ;
}

+(float)deltaFi
{
    return deltaFi;
}

+(NSMutableArray*)sehistory_fi
{
    if(!history_fi)
    {
        history_fi = [NSMutableArray array];
    }
    return history_fi;
}

+(NSMutableArray*)history_fi
{
    if(!history_fi)
    {
        history_fi = [NSMutableArray array];
    }
    return history_fi;
}

+(void) sewifi_wifi_DistanceCnt: (int)newValue
{
    wifi_wifi_DistanceCnt = newValue;
    return ;
}

+(int)wifi_wifi_DistanceCnt
{
    return wifi_wifi_DistanceCnt;
}

+(void) seresetWifiTimeCnt: (int)newValue
{
    resetWifiTimeCnt = newValue;
    return ;
}

+(int)resetWifiTimeCnt
{
    return resetWifiTimeCnt;
}

+(void) seWifiLocation_eKnn: (float)value AtIndex:(int) index
{
    WifiLocation_eKnn[index] = value;
    return ;
}

+(float)WifiLocation_eKnnAtIndex:(int)index
{
    return WifiLocation_eKnn[index];
}

+(void) seWifiAvailableNum: (int)newValue
{
    WifiAvailableNum = newValue;
    return ;
}

+(int)WifiAvailableNum
{
    return WifiAvailableNum;
}

+(void)seWifiTotalNum: (int)newvalue
{
    WifiTotalNum = newvalue;
    return ;
}

+(int)WifiTotalNum
{
    return WifiTotalNum;
}

+(void)sewifi_pdr_DistanceCnt: (int)newValue
{
    wifi_pdr_DistanceCnt = newValue;
    return ;
}

+(int)wifi_pdr_DistanceCnt
{
    return wifi_pdr_DistanceCnt;
}

//+(GKRandomDistribution*)ser
//{
//    if (!r)
//    {
//        r = [[GKRandomDistribution alloc] initWithRandomSource: [[GKARC4RandomSource alloc] init] lowestValue:0 highestValue:1];
//    }
//    return r;
//}


+(int)distanceThreshold
{
    return distanceThreshold;
}

+(int)ErrorLocThreshold
{
    return ErrorLocThreshold;
}

+(int)ErrorLocCount
{
    return ErrorLocCount;
}

+(void)setErrorLocCount:(int)newValue
{
    ErrorLocCount = newValue;
}

+(void)setStayCount:(int)newValue
{
    StayCount = newValue;
}

+(int)StayCount
{
    return StayCount;
}

+(void)setIntialLocX:(float)newValue
{
    intialLocX = newValue;
}

+(float)intialLocX
{
    return intialLocX;
}

+(void)setIntialLocY:(float)newValue
{
    intialLocY = newValue;
}

+(float)intialLocY
{
    return intialLocY;
}
+(int)StayThreshold
{
    return StayThreshold;
}
+(float)errorAngle
{
    return errorAngle;
}

+(void)setErrorAngle:(float)angle
{
    errorAngle = angle;
}

@end
