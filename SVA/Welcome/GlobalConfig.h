//
//  GlobalConfig.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GlobalConfig : NSObject

+(double)sexSp:(double)xSp;
+(double)seySp:(double)ySp;
+(NSString*)seThre;
+(NSString*)seServer;
+(NSString*)seIns;
+(NSString*)seRead;
+(NSString*)seAddsec;
+(double)sePropor:(double)propor;
+(NSString*)weights;
+(NSString*)seMaxDev;
+(NSString*)seMoreMax;
+(NSString*)seDirec;
+(NSString*)seIdentifi;
+(NSString*)sePor;
+(BOOL)seAutop:(BOOL)autop;
+(BOOL)seAnimation:(BOOL)animation;

+(NSString*)threshold;
+(double)proportion;
+(double)xSport;
+(double)ySport;
+(NSString*)moreMaxDeviate;



@end
