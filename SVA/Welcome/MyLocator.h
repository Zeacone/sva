//
//  MyLocator.h
//  svaDemo
//
//  Created by XuZongCheng on 16/1/12.
//  Copyright © 2016年 XuZongCheng. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MyLocator :NSObject


+(NSMutableArray*) pixelToMeterwithInt: (int)ix withInt: (int)iy;
+(void) paticleFilterwithFloat:(float)x withFloat:(float)y withInt: (NSString *)floorIndex withFloat: (float)fi withFloat: (float)v WithResultSavedInArray:(int*)ixy WithElemCount:(int)lenOfIxy;
+(void) meterToPixelwithFloat: (float)fx withFloat: (float)fy WithResultSavedInArray:(int*)ixy WithElemCount:(int)lenOfIxy;

+(void) init_last_loc;

@end
