//
//  SVALogger.h
//  SVA
//
//  Created by Zeacone on 16/1/27.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVALogger : NSObject

+ (instancetype)sharedLogger;
- (void)appendLog:(NSString *)log;
- (NSString *)readAllMessage;
- (void)clearAllData;

@end
