//
//  SVAPathParser.m
//  SVA
//
//  Created by 一样 on 16/2/18.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import "SVAPathParser.h"
#import "GDataXMLNode.h"

@implementation SVAPathParser

+ (NSMutableArray<NSMutableArray<NSNumber *> *> *)getMatrixFromXML:(NSString *)xmlPath {
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:xmlPath ofType:@"xml"]];
    if (!data) {
        return nil;
    }
    
    GDataXMLDocument *xmlDoc = [[GDataXMLDocument alloc] initWithData:data options:0 error:nil];
    NSError *error = nil;
    NSArray *lines = [xmlDoc nodesForXPath:@"//line" error:&error];
    
    // 初始化矩阵
    NSMutableArray<NSMutableArray<NSNumber *> *> *rows = [NSMutableArray array];
    for (NSInteger i = 0; i < lines.count; i++) {
        NSMutableArray *col = [NSMutableArray array];
        for (NSInteger j = 0; j < lines.count; j++) {
            [col addObject:@(-1)];
            if (i == j) {
                [col replaceObjectAtIndex:j withObject:@(0)];
            }
        }
        [rows addObject:col];
    }
    
    
    for (GDataXMLElement *element in lines) {
        NSString *xString = element.stringValue;
        NSString *yString = [element attributeForName:@"y"].stringValue;
        NSArray *array = [xString componentsSeparatedByString:@","];
        for (NSString *xPoint in array) {
            rows[yString.integerValue][xPoint.integerValue] = @(1);
        }
    }
    if ([xmlPath isEqualToString:@"huawei"]) {
        rows[22][23] = @(2);
        rows[23][22] = @(2);
    }
    return rows;
}

@end
