//
//  SVAFindPath.m
//  SVA
//
//  Created by 一样 on 16/2/18.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import "SVAFindPath.h"
#import "SVADijkstra.h"

@interface SVAFindPath ()

@end

@implementation SVAFindPath

+ (NSMutableArray<NSValue *> *)initializeHuaweiPoints {
    NSMutableArray<NSValue *> *points = [NSMutableArray array];
    
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(118, 205)]]; //0
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(170, 205)]]; //1
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(170, 180)]]; //2
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(170, 130)]]; //3
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(275, 180)]]; //4
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(275, 150)]]; //5
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(310, 180)]]; //6
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(310, 150)]]; //7
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 180)]]; //8
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 135)]]; //9
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(450, 180)]]; //10
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(450, 150)]]; //11
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(480, 180)]]; //12
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(480, 150)]];//13
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 215)]];//14
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(350, 225)]];//15
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(295, 255)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(350, 340)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(360, 295)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(308, 324)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 340)]]; //20
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(240, 305)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(240, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(205, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(290, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(290, 435)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(205, 460)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(205, 520)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(150, 555)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(128, 615)]]; //30
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(210, 635)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(260, 625)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(305, 575)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(345, 525)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 525)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 565)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(450, 565)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(460, 585)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 615)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(420, 485)]]; //40
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(380, 485)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(500, 485)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(540, 525)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(590, 505)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(500, 575)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(520, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(506, 435)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(530, 355)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(660, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(550, 215)]]; //50
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(602, 215)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(602, 183)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(660, 215)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(660, 183)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(550, 265)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(490, 265)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(600, 265)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(550, 305)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(660, 327)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(745, 280)]]; //60
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(745, 180)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(830, 180)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(830, 205)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(950, 180)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(950, 145)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(800, 353)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(750, 353)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(830, 315)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(765, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(765, 430)]]; //70
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(800, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(880, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(880, 465)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(950, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(990, 395)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(990, 455)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(660, 280)]]; //77
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(475, 220)]];
    
    return points;
}

+ (NSMutableArray<NSValue *> *)initializeVDFPoints {
    NSMutableArray<NSValue *> *points = [NSMutableArray array];
    
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(60, 70)]]; //0
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(60, 30)]]; //1
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(195, 30)]]; //2
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(195, 75)]]; //3
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(140, 75)]]; //4
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(195, 130)]]; //5
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(140, 130)]]; //6
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(230, 130)]]; //7
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(195, 165)]]; //8
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(60, 165)]]; //9
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(60, 130)]]; //10
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(195, 230)]]; //11
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(130, 230)]]; //12
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(240, 230)]];//13
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(195, 280)]];//14
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(200, 300)]];//15
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(200, 380)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(150, 380)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(260, 380)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(200, 465)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(200, 520)]]; //20
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(150, 520)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(260, 520)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 30)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(475, 30)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(540, 30)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(540, 60)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(595, 30)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 128)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(320, 128)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(410, 128)]]; //30
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 165)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(475, 165)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(540, 165)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(540, 128)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(595, 165)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 222)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(320, 222)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(410, 222)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(540, 222)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(595, 222)]]; //40
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(630, 222)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(475, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(475, 340)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(536, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(595, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(595, 340)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 430)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(536, 430)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 465)]]; //50
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(410, 465)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(365, 520)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(410, 520)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(500, 470)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(536, 470)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(575, 470)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(536, 520)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(500, 520)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(705, 165)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(820, 165)]]; //60
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(940, 165)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(705, 220)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(740, 220)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(790, 220)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(820, 220)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(840, 220)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(910, 220)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(940, 220)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(705, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(820, 300)]]; //70
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(940, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(785, 355)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(820, 355)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(880, 355)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(940, 355)]]; //75
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(960, 355)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(705, 430)]]; //77
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(820, 430)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(880, 430)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(940, 430)]]; //80
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(880, 460)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(705, 470)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(660, 470)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1015, 369)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1050, 369)]]; //85
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1050, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1100, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1100, 260)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1260, 200)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1220, 200)]]; //90
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1220, 256)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1185, 256)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1260, 256)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1220, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1185, 390)]]; //95
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1220, 390)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1260, 390)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1260, 330)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1260, 300)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1390, 300)]]; //100
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1390, 140)]];
    [points addObject:[NSValue valueWithCGPoint:CGPointMake(1440, 140)]];
    
    return points;
}

+ (CGPoint)getShortPointWithPoint:(CGPoint)point keyword:(NSString *)keyword {

    // 临时距离
    double tmp_distance = 0;
    // 最短距离
    double shortest_dis = 0;
    // 所找到的点
    CGPoint find_point;
    
    NSInteger x0 = point.x;
    NSInteger y0 = point.y;
    NSArray<NSValue *> *points;
    if ([keyword isEqualToString:@"huawei"]) {
        points = [self.class initializeHuaweiPoints];
    } else {
        points = [self.class initializeVDFPoints];
    }
    
    for (NSValue *pointValue in points) {
        tmp_distance = sqrt(pow(x0 - pointValue.CGPointValue.x, 2) + pow(y0 - pointValue.CGPointValue.y, 2));
        if (shortest_dis == 0 || shortest_dis > tmp_distance) {
            shortest_dis = tmp_distance;
            find_point = pointValue.CGPointValue;
        }
    }
    return find_point;
}

+ (NSArray<NSValue *> *)findPathWithStart:(CGPoint)start end:(CGPoint)end onModel:(SVAMapDataModel *)mapModel {
    
    CGPoint realStartPoint = [self.class getShortPointWithPoint:start keyword:mapModel.keyWord];
    CGPoint realEndPoint = [self.class getShortPointWithPoint:end keyword:mapModel.keyWord];
    
    NSArray<NSValue *> *points;
    if ([mapModel.keyWord isEqualToString:@"huawei"]) {
        points = [self.class initializeHuaweiPoints];
    } else {
        points = [self.class initializeVDFPoints];
    }
    
    NSInteger startNo = 0, endNo = 0;
    for (NSInteger i = 0; i < points.count; i++) {
        
        if (CGPointEqualToPoint(points[i].CGPointValue, realStartPoint)) {
            startNo = i;
        }
        if (CGPointEqualToPoint(points[i].CGPointValue, realEndPoint)) {
            endNo = i;
        }
    }
    
    NSString *path = [SVADijkstra calculatePathWithMatrix:[SVAPathParser getMatrixFromXML:mapModel.keyWord] start:startNo end:endNo];
    
    NSArray *pointNo = [path componentsSeparatedByString:@">"];
    NSMutableArray<NSValue *> *pointValue = [NSMutableArray array];
    for (NSString *no in pointNo) {
        [pointValue addObject:points[no.integerValue]];
    }
    return pointValue;
}

//+ (NSArray *)getpathPointWithMatrix:(NSMutableArray<NSMutableArray<NSNumber *> *> *)matrix start:(AOPathPoint *)start end:(AOPathPoint *)end {
//    NSMutableArray<NSValue *> *points = [self.class initializePoints];
//}

@end
