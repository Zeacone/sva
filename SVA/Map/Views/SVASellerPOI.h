//
//  SVASellerPOI.h
//  SVA
//
//  Created by 君若见故 on 16/2/24.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVASellerPOI : UIView

- (void)addSellerLayer:(NSArray<SVAPOIModel *> *)sellers;

- (void)removeSellersLayer:(NSArray<SVAPOIModel *> *)sellers;

- (void)addPushMessageLayers:(NSArray<SVALocationMessageModel *> *)pushMessages;

- (void)removePushMessageLayers:(NSArray<SVALocationMessageModel *> *)pushMessages;

- (void)addIdentifierIcon;

@end
