//
//  SVANewMap.h
//  SVA
//
//  Created by 一样 on 16/2/24.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVAMap.h"

@interface SVANewMap : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *parentView;

@property (nonatomic, strong) SVAMap *mapView;

@end
