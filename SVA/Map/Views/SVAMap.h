//
//  SVAMap.h
//  SVA
//
//  Created by Zeacone on 16/1/14.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXSVGView.h"
#import "SVASVGImage.h"

typedef NS_ENUM(NSUInteger, POIType) {
    POISeller,
    POIPush,
};

typedef void(^changeSize)();

@interface SVAMap : UIView

// Map related
@property (nonatomic, weak) CAShapeLayer *oldClickedlayer;
@property (nonatomic, strong) SVAMapDataModel *mapModel;
@property (nonatomic, strong) SVASVGImage *svgImage;
@property (nonatomic, strong) UIImageView *ImgMap;
@property (nonatomic, strong) NSMutableArray<NSValue *> *points;
@property (nonatomic,strong) changeSize chansize;

+ (instancetype)sharedMap;
+ (instancetype)sharedMap_new;
- (void)loadMapWithMapModel:(SVAMapDataModel *)mapModel;

@end
