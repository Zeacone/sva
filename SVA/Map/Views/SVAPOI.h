//
//  SVAPOI.h
//  SVA
//
//  Created by Zeacone on 15/12/17.
//  Copyright © 2015年 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <libkern/OSAtomic.h>
#import "SVAMapDataModel.h"
#import "FindPath.h"
#import "SVAPathFilter.h"
#import "SVAMap.h"
#import "NSFileHandle+readLine.h"
@class SVALocationDataModel;
@class SVALocationMessageModel;
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>
#import "SVAFindPath.h"
#import "ScaleStore.h"


//typedef NS_ENUM(NSUInteger, POIType) {
//    POISeller,
//    POIPush,
//};

@interface SVAPOI : UIView <UIGestureRecognizerDelegate, CLLocationManagerDelegate>
{
    BOOL hasStart, hasEnd, hasIdentifier;
    UIButton * deleteBT;
    CLLocationCoordinate2D currentLocation;
    CLLocationDirection currentHeading;
    CLLocationManager *locationManager;
    CGPoint filterPoint;
  
   
}
@property (nonatomic, strong) NSMutableArray       *result;
@property (nonatomic, strong) NSMutableArray       *poiModels;
@property (nonatomic, strong) NSMutableArray       *messageModels;
@property (nonatomic, strong) SVALocationDataModel *locationModel;
@property (nonatomic, strong) SVALocationMessageModel *messageModel;
@property (nonatomic, strong) SVAMapDataModel      *mapDataModel;

@property (nonatomic, strong) NSMutableArray       *historyLocation;
@property (nonatomic, strong) NSMutableArray       *sellerPoints;
@property (nonatomic, strong) NSMutableArray       *pushPoints;

@property (nonatomic, assign) CGFloat              scale;

@property (nonatomic, assign) CGPoint              touchPoint;


@property (nonatomic, assign) CGPoint              realCurrentPoint, realStartPoint, realEndPoint;
@property (nonatomic, assign) CGPoint              currentPoint;
@property (nonatomic, copy)   NSString             *currentMessage;
@property (nonatomic, copy)   NSString             *picturePath;

@property (nonatomic, assign) BOOL                 shouldShowSellerInfo;
@property (nonatomic, assign) BOOL                 shouldLocation;
//@property (nonatomic, assign) POIType              poiType;

@property (nonatomic, strong) CALayer              *startLayer;
@property (nonatomic, strong) CALayer              *endLayer;
@property (nonatomic, strong) CALayer              *identifier;
@property (nonatomic, strong) CALayer              *locateLayer;
@property (nonatomic, strong) CAShapeLayer     *pathLayer;

@property (nonatomic, assign) CGPoint startPoint, endPoint;

@property (nonatomic, strong) CLLocationManager    *locationManager;
@property (nonatomic, strong) ScaleStore *scaleStore;
@property (nonatomic,assign) BOOL isLocate;

+ (instancetype)sharedPOI;
- (void)addPopViewWithMessage:(NSString *)message logoPath:(NSString *)logoPath mapPoint:(CGPoint)mapPoint;
- (void)addPushMessage;
- (void)addAllLayer;
- (void)addLocation;
- (void)addHistoryLayer;
- (void)clearLocation;
- (void)clearAll;
- (void)rotateTextWithRotation:(CGFloat)rotation;

@end
