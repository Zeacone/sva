//
//  SVAMap.m
//  SVA
//
//  Created by Zeacone on 16/1/14.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import "SVAMap.h"

#import "SVAPointParser.h"
#import "SVARouteDataViewModel.h"


@implementation SVAMap

+ (instancetype)sharedMap {
    static SVAMap *map = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
//        map = [[SVAMap alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64)];
        map = [[SVAMap alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) mapType:@"svg"];
        
        [map addSubview:map.ImgMap];
        [map sendSubviewToBack:map.ImgMap];
        
        [map addSubview:map.svgImage];
        [map sendSubviewToBack:map.svgImage];
    });
    return map;
}

+ (instancetype)sharedMap_new {

    SVAMap *map = [[SVAMap alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height - 64) mapType:@"svg"];
    [map addSubview:map.ImgMap];
    [map sendSubviewToBack:map.ImgMap];
    
    [map addSubview:map.svgImage];
    [map sendSubviewToBack:map.svgImage];
    return map;
}

- (instancetype)initWithFrame:(CGRect)frame mapType:(NSString *)type {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    self.svgImage = [[SVASVGImage alloc] initWithSVG:@"bazhan"];
    self.ImgMap = [[UIImageView alloc] initWithFrame:self.frame];
    
    if ([type isEqualToString:@"svg"]) {
        [self loadSVGMap];
    } else {
        [self loadImgMap];
    }
    return self;
}

- (void)loadMapWithMapModel:(SVAMapDataModel *)mapModel {
    self.mapModel = mapModel;
    if (mapModel.svg) {
       // [self removeFromSuperview];
        [self loadSVGMap];
    } else {
        [self loadImgMap];
    }
    
    SVARouteDataViewModel *viewmodel = [SVARouteDataViewModel new];
    @weakify(self);
    [viewmodel getRouteData:^(NSMutableArray<SVARouteData *> *data) {
        for (SVARouteData *routeData in data) {
            if ([routeData.place isEqualToString:mapModel.place] && [routeData.floor isEqualToString:mapModel.floor]) {
                NSString *key = [mapModel.place stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSNumber *content = ((NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey:key]);
                
                NSInteger updatetime;
                if (!content) {
                    updatetime = 0;
                } else {
                    updatetime = content.longValue;
                }
                NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                                                      inDomain:NSUserDomainMask
                                                                             appropriateForURL:nil
                                                                                        create:NO
                                                                                         error:nil];
                if (!routeData.route) return;
                NSURL *xmlURL = [documentsDirectoryURL URLByAppendingPathComponent:routeData.route];
                self.points = [[SVAPointParser new] getPointsFromXML:[xmlURL absoluteString] withScale:self.mapModel.scale.doubleValue];
                
                if (routeData.updateTime != updatetime) {
                    if (!routeData.route) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            MBProgressHUD *hud = [[MBProgressHUD alloc] initWithWindow:KEY_WINDOW];
                            hud.detailsLabelText = @"路径滤波文件不存在！";
                            [hud hide:YES afterDelay:3.0];
                        });
                        return;
                    }
                    
                    [[NSFileManager defaultManager] removeItemAtURL:xmlURL error:nil];
                    [[SVANetworkResource sharedResource] downloadPathFilterWithFilename:routeData.route CompletionHandler:^(NSURL *filepath) {
                        @strongify(self);
                        self.points = [[SVAPointParser new] getPointsFromXML:[filepath absoluteString] withScale:self.mapModel.scale.doubleValue];
                    }];
                }
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithLong:routeData.updateTime] forKey:key];
            }
        }
    }];
}

- (void)loadSVGMap {
    if (!self.mapModel) {
        return;
    }
    self.ImgMap.hidden = YES;
    self.svgImage.hidden = NO;

    
    if ([self.mapModel.keyWord isEqualToString:@"huawei"] || [self.mapModel.keyWord isEqualToString:@"vdf"]) {
        [self.svgImage loadSVGMap:self.mapModel.keyWord];
//        if (self.chansize) {
//            self.chansize();
//        }
        return;
    }
    
    @weakify(self);
    [[SVANetworkResource sharedResource] downloadSVGFileWithFilename:self.mapModel.svg completionHandler:^(NSURL *filepath) {
        @strongify(self);
        if (!filepath) {
            [self loadImgMap];
            return;
        }
        [self.svgImage loadSVGMap:[filepath absoluteString]];
        self.svgImage.bounds = CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        CGSize size = [UIScreen mainScreen].bounds.size;
        self.frame = CGRectMake(0, 0, size.width, size.height-64);
        self.svgImage.center = self.center;

    }];
    
    // Set callback for click.
    [self.svgImage setClickHandler:^(NSString *identifier, CAShapeLayer *layer, CGPoint touchPoint) {
        @strongify(self);
        if (self.oldClickedlayer) {
            self.oldClickedlayer.zPosition = 0;
            self.oldClickedlayer.shadowOpacity = 0;
        }
        self.oldClickedlayer = layer;
        layer.zPosition = 5;
        layer.shadowOpacity = 0.5;
        layer.shadowColor = [UIColor redColor].CGColor;
        layer.fillColor = [UIColor redColor].CGColor;
        layer.shadowRadius = 1;
        layer.shadowOffset = CGSizeMake(3, 3);
    }];
}

- (void)loadImgMap {
    if (!self.mapModel) {
        return;
    }
//    self.ImgMap.hidden = NO;
    for (UIView *igv in self.subviews) {
        if ([igv isKindOfClass:[UIImageView class]]) {
            [igv removeFromSuperview];
            self.ImgMap = nil;
        }
    }
    self.ImgMap = [[UIImageView alloc] initWithFrame:self.frame];
    
//    self.svgImage.hidden = YES;
    
    self.ImgMap.center = CGPointMake(self.center.x, self.center.y);
    self.ImgMap.bounds = CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    self.ImgMap.contentMode = UIViewContentModeScaleAspectFit;
    [[SVANetworkResource sharedResource] loadImage:self.ImgMap WithPath:self.mapModel.path];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
//
//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
////    UIView *hitView = [super hitTest:point withEvent:event];
//    NSMutableArray *array = [NSMutableArray array];
//    [array addObjectsFromArray:[SVAPOI sharedPOI].sellerPoints];
//    [array addObjectsFromArray:[SVAPOI sharedPOI].pushPoints];
//    
//    for (NSValue *value in array) {
//        CGPoint originPoint = value.CGPointValue;
//        if (CGRectContainsPoint(CGRectMake(originPoint.x, originPoint.y, 20, 20), point) || ![self.subviews containsObject:self.svgImage]) {
//            return [SVAPOI sharedPOI];
//        }
//    }
//    return self.svgImage;
//}

@end
