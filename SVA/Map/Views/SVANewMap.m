//
//  SVANewMap.m
//  SVA
//
//  Created by 一样 on 16/2/24.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import "SVANewMap.h"

@implementation SVANewMap

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    return self;
}

- (void)setupView {
    self.mapView = ({
        SVAMap *map = [SVAMap new];
        map;
    });
    [self addSubview:self.mapView];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self);
        make.left.mas_equalTo(self);
        make.size.mas_equalTo(self);
    }];
    
    // 添加手势控制地图的移动、缩放以及旋转
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(translateWithPan:)];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(smoothTranslateWithSwipe:)];
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scaleWithPinch:)];
    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotateWithRotation:)];
    
    [self addGestureRecognizer:pan];
    [self addGestureRecognizer:swipe];
    [self addGestureRecognizer:pinch];
    [self addGestureRecognizer:rotate];
    
    
}

- (void)translateWithPan:(UIPanGestureRecognizer *)pan {
    
}

- (void)smoothTranslateWithSwipe:(UISwipeGestureRecognizer *)swipe {
    
}

- (void)scaleWithPinch:(UIPinchGestureRecognizer *)pinch {
    
}

- (void)rotateWithRotation:(UIRotationGestureRecognizer *)rotate {
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
