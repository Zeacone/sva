//
//  SVAPOI.m
//  SVA
//
//  Created by Zeacone on 15/12/17.
//  Copyright © 2015年 huawei. All rights reserved.
//

#import "SVAPOI.h"
#import <CoreLocation/CLHeading.h>
#import "math.h"
#import "MyLocator.h"
#import "StaticData.h"
#import "SVANavAngleAndSpeed.h"
#import "SJAvatarBrowser.h"
//static NSUserDefaults *userdefault;
static NSMutableDictionary * dic;
static int numCount;
static NSMutableArray * pushModels;
static NSMutableArray * pushPointArr;

@interface SVAPOI () {
    UIView *popupView;
    NSTimer * timer;
    NSString * pushMessage;
    //NSDictionary * dic;
    NSString * clickShopMessage;
    CGRect oldImgRect;
    BOOL isBigPicture;
    UIImageView * logImgView;
    CALayer * imgLayer;
    CALayer * stringLayer;
}

@end

@implementation SVAPOI

+ (instancetype)sharedPOI {
    static SVAPOI *poi = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        poi = [[SVAPOI alloc] init];
        poi.scale = 1;
        poi.identifier = [CALayer layer];
//        poi.locateLayer = [CALayer layer];
        [poi startGetHeadingCource];
        poi.scaleStore = [ScaleStore defaultScale];
        
    });
    return poi;
}

-(instancetype)init
{
    if(self = [super init])
    {
        dic = [[NSMutableDictionary alloc] init];
        pushModels = [[NSMutableArray alloc]init];
        pushPointArr = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (void)startGetHeadingCource {
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.headingFilter = 5;
   
    
    [self.locationManager startUpdatingHeading];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
//    CLLocation * loc = [[CLLocation alloc]init];
    
    if (self.mapDataModel) {
        NSLog(@"magneticHeading1=%f", newHeading.magneticHeading);
        self.locateLayer.transform = CATransform3DMakeRotation(M_PI * (newHeading.magneticHeading-55) / 180.0, 0, 0, 1);
    } else {
        self.locateLayer.transform = CATransform3DMakeRotation(M_PI * (newHeading.magneticHeading-55) / 180.0, 0, 0, 1);
    }
    NSLog(@"magneticHeading=%f2", newHeading.magneticHeading);
    //transform 定位点角度旋转和形变 固定其一 ？？？
    self.locateLayer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
    if (newHeading.headingAccuracy<0) {
        return;
    }
    
    CLLocationDirection theHeading = newHeading.magneticHeading;
    currentHeading = theHeading;
    
}

- (NSMutableArray *)poiModels {
    if (!_poiModels) {
        _poiModels = [NSMutableArray array];
    }
    return _poiModels;
}

- (NSMutableArray *)messageModels {
    if (!_messageModels) {
        _messageModels = [NSMutableArray array];
    }
    return _messageModels;
}

- (NSMutableArray *)historyLocation {
    if (!_historyLocation) {
        _historyLocation = [NSMutableArray array];
    }
    return _historyLocation;
}

- (NSMutableArray *)sellerPoints {
    if (!_sellerPoints) {
        _sellerPoints = [NSMutableArray array];
    }
    return _sellerPoints;
}

- (NSMutableArray *)pushPoints {
    if (!_pushPoints) {
        _pushPoints = [NSMutableArray array];
    }
    return _pushPoints;
}

- (SVAMapDataModel *)mapDataModel {
    if (!_mapDataModel) {
        _mapDataModel = [[SVAMapDataModel alloc] init];
    }
    return _mapDataModel;
}

- (void)removeSellers {
    [self.messageModels removeAllObjects];
    [self.historyLocation removeAllObjects];
    self.shouldLocation = NO;
}

- (void)clearLocation {
    self.locationModel = nil;
    hasStart = NO;
    hasEnd = NO;
    hasIdentifier = NO;
    
dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.historyLocation removeAllObjects];
        [self.startLayer removeFromSuperlayer];
        [self.endLayer removeFromSuperlayer];
        [self.identifier removeFromSuperlayer];
        [self.locateLayer removeFromSuperlayer];

     });   
    
//    self.locateLayer = nil;
//    self.startLayer = nil;
//    self.endLayer = nil;
//    self.historyLocation = nil;
//    self.identifier = nil;
    
    [self removePopView];
}

- (void)clearAll {

    self.locateLayer = nil;
    self.startLayer = nil;
    self.endLayer = nil;
    self.historyLocation = nil;
//    self.identifier = nil;
//
//    [self.layer.sublayers enumerateObjectsUsingBlock:^(CALayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        [obj removeFromSuperlayer];
//    }];
    
dispatch_async(dispatch_get_main_queue(), ^{
    
    NSArray<CALayer *> *array = [NSArray arrayWithArray:self.layer.sublayers];
    NSLock *lock = [NSLock new];
    [lock lock];
    for (CALayer *layer in array) {
        [layer removeFromSuperlayer];
    }
    [lock unlock];

    });


}

- (void)addAllLayer {
    // Clear all sublayers at first time.

    //初始化定位点layer,放在单例置空后无法再次初始化
    self.locateLayer = [[CALayer alloc] init];
//    self.identifier = [[CALayer alloc] init];
    
    NSLock *lock = [[NSLock alloc] init];
    [lock lock];
    NSArray *array = [NSArray arrayWithArray:self.layer.sublayers];
    for (CALayer *sublayer in array) {
        if ([sublayer isEqual:self.startLayer] || [sublayer isEqual:self.endLayer] || [sublayer isEqual:self.identifier]) {
            self.startLayer.bounds = CGRectMake(0, 0, 25, 34 );
            self.endLayer.bounds = CGRectMake(0, 0, 25 , 34 );
//            self.identifier.bounds = CGRectMake(0, 0, 24 , 30 );
            self.startLayer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
            self.endLayer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
            self.identifier.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
            continue;
        }
        

        [sublayer removeFromSuperlayer];
    }
    [lock unlock];
    
    [self.sellerPoints removeAllObjects];
//    [self.pushPoints removeAllObjects];
    [self addPOI];
    if (self.isLocate) {
//        [self addHistoryLayer];
        [self addPushMessage];
    }
//    [self addLocation];
    if (hasStart && hasEnd) {
        if ((self.startPoint.x != self.endPoint.x) && (self.startPoint.y != self.endPoint.y)) {
            [self drawPathWithStart:self.realStartPoint endPoint:self.realEndPoint];
        }
    }
//    if (hasIdentifier) {
//        [self.layer addSublayer:self.identifier];
//    }
//    if (![self.layer.sublayers containsObject:self.identifier]) {
//        
//        [self.layer addSublayer:self.identifier];
//    }
    if (hasStart) {
        [self.layer addSublayer:self.startLayer];
    }
    if (hasEnd) {
        [self.layer addSublayer:self.endLayer];
    }
    [self.layer addSublayer:self.locateLayer];

}

- (void)addPOI {
    // Enumerate poi array to seek draw points and related information.
    for (SVAPOIModel *model in self.poiModels) {
        // Caculate real coordinate.
        CGPoint point = [SVACoordinateConversion getPointWithXspot:model.xSpot Yspot:model.ySpot onMapMode:self.mapDataModel];
        // Store coordinates for touch recognize.
        
//        if (![self.sellerPoints containsObject:[NSValue valueWithCGPoint:point]]) {
            [self.sellerPoints addObject:[NSValue valueWithCGPoint:point]];
//        }
        
#warning 添加POI
        if (self.scaleStore.scale <= 1.5) {
            if ([self.sellerPoints indexOfObject:[NSValue valueWithCGPoint:point]] % 2 != 0) {
                continue;
            }
        }
        [self drawIcon:point AndText:model.info];
    }
}


- (void)addPushMessage {
    
    // Finish this if the switch is turning off.
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"generalSetting10000"]) return;
    // Enumerate push message array to seek draw points and related information.
    
   // userdefault = [NSUserDefaults standardUserDefaults];
    //画推送点
   
//    if(pushPointArr.count==0)
//    {
//        pushPointArr = self.messageModels;
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [pushPointArr removeAllObjects];
//        });
//        
//    }
    
    
//    for (SVALocationMessageModel *model in pushModels) {
//        
//        
//        CGPoint point = [SVACoordinateConversion getPointWithXspot:model.xSpot Yspot:model.ySpot onMapMode:self.mapDataModel];
//        // Store coordinates for touch recognize.
//        if (![self.pushPoints containsObject:[NSValue valueWithCGPoint:point]]) {
//            
//            [self.pushPoints addObject:[NSValue valueWithCGPoint:point]];
//        }
//        
//        [self drawIcon:point AndText:model.shopName];
//    }
    
    
    
        self.messageModel = self.messageModels.count == 0 ? nil : [self.messageModels lastObject];
        NSString *key = self.messageModel.shopName;
    
        if (!key) {
#warning 
//            [popupView removeFromSuperview];
//            [self.identifier removeFromSuperlayer];
            return;
        }
    
        NSTimeInterval timestamp = [[NSDate dateWithTimeIntervalSinceNow:0] timeIntervalSince1970];
        
//        NSTimeInterval timestamp_cmp = timestamp + model.timeInterval.doubleValue * 60;
    
        // NSString *timestampValue = [userdefault objectForKey:key];
        NSTimeInterval timestamp_tmp = [[dic objectForKey:key]doubleValue];
        
        //小于1分钟
        if ([dic objectForKey:key] && timestamp - timestamp_tmp < 60) {
            
            return;
            
        } else {
            
           // pushPointArr = self.pushPoints;
            
          //  pushModels = self.messageModels;
            self.picturePath = self.messageModel.pictruePath;
            
            clickShopMessage = [NSString stringWithFormat:@"%@",self.messageModel.message];

            //添加推送窗口
            [self addPushView];
            
            [dic setValue:@(timestamp) forKey:key];
            
            
        }
}


-(void)addPushView
{

    
    popupView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, SCREEN_SIZE.height)];
        view.backgroundColor = [UIColor colorWithRed:49/255.0 green:133/255.0 blue:255/255.0 alpha:1.0];
        view.tag = 1111;
        
       // 51.133.255
        
        // Add pan gesture.
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragView:)];
        panGesture.delegate = self;
        [view addGestureRecognizer:panGesture];
        
        // Add swipe gesture to show detail information.
        UISwipeGestureRecognizer *swipeShowGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToShowView:)];
        swipeShowGesture.delegate = self;
        swipeShowGesture.direction = UISwipeGestureRecognizerDirectionUp;
     //   [view addGestureRecognizer:swipeShowGesture];
        
        UISwipeGestureRecognizer *swipeCloseGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToCloseView:)];
        swipeCloseGesture.delegate = self;
        swipeCloseGesture.direction = UISwipeGestureRecognizerDirectionDown;
        [view addGestureRecognizer:swipeCloseGesture];
        
        view;
    });
    
    UIImageView *logo = ({
        UIImageView *imageview = [UIImageView new];
        
        [[SVANetworkResource sharedResource] loadLogo:imageview WithPath:self.picturePath];
        
        imageview.contentMode = UIViewContentModeScaleAspectFit;
        imageview.tag = 123;
        imageview.userInteractionEnabled = YES;
        UITapGestureRecognizer * picGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage)];
        [imageview addGestureRecognizer:picGesture];
        logImgView = imageview;
        imageview;
    });
    [popupView addSubview:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_SIZE.height / 6);
        make.height.mas_equalTo(SCREEN_SIZE.height / 6 - 10);
        make.left.mas_equalTo(popupView).offset(5);
        make.top.mas_equalTo(popupView).offset(5);
    }];
    
    UITextView *messageLabel = ({
        UITextView *label = [UITextView new];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentLeft;
        [label setFont:[UIFont systemFontOfSize:17]];
        label.text = clickShopMessage;
        label.editable = NO;
        label;
    });
    
    [popupView addSubview:messageLabel];

    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(logo.mas_right).offset(5);
        make.right.mas_equalTo(popupView).offset(-5);
        make.height.mas_equalTo(SCREEN_SIZE.height / 6 - 5);
        make.top.mas_equalTo(popupView).offset(0);
        
    }];
    
    
    [KEY_WINDOW addSubview:popupView];
    
    CGPoint point = [SVACoordinateConversion getPointWithXspot:self.messageModel.xSpot Yspot:self.messageModel.ySpot onMapMode:self.mapDataModel];
    
    self.identifier.zPosition = 1;
    self.identifier.anchorPoint = CGPointMake(0.5, 1.0);
    self.identifier.bounds = CGRectMake(0, 0, 24, 30 );
    self.identifier.position = point;
    self.identifier.contents = (__bridge id _Nullable)([UIImage imageNamed:@"end"].CGImage);
    
    if (![self.layer.sublayers containsObject:self.identifier]) {
        
        [self.layer addSublayer:self.identifier];
    }
    

    [UIView animateWithDuration:0.5
                          delay:0.0
         usingSpringWithDamping:0.4
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         popupView.transform = CGAffineTransformMakeTranslation(0, -SCREEN_SIZE.height / 6);
                     } completion:^(BOOL finished) {
                         
                     }];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [popupView removeFromSuperview];
        
        [self.identifier removeFromSuperlayer];
        
        [pushModels removeAllObjects];
        
        [pushPointArr removeAllObjects];
    
       // [self.pushPoints removeAllObjects];
        
    });
    
}

-(void)clickImage
{
    if (isBigPicture) {
        
        [[SVALocationViewModel sharedLocateViewModel] startLocating];
        isBigPicture = NO;
    }
    else
    {
        [[SVALocationViewModel sharedLocateViewModel] stopLocating];
        isBigPicture = YES;
    }
    
    [SJAvatarBrowser showImage:logImgView];//调用方法

}
- (void)drawIcon:(CGPoint)point AndText:(NSString *)text {
    // Get seller identity image.
    
    // Add sublayer to show push message.
    CALayer *layer = [CALayer layer];
//    layer.anchorPoint = CGPointMake(1.0, 0.5);
    NSLog(@"%@",NSStringFromCGPoint(point));
    layer.position = point;
    layer.contentsScale = [UIScreen mainScreen].scale;
    
//    layer.bounds = CGRectMake(0, 0, 15 / self.scale, 15 / self.scale);
    layer.bounds = CGRectMake(0, 0, 15, 15);
    layer.transform = CATransform3DMakeScale(1 / self.scaleStore.scale, 1 / self.scaleStore.scale,0);
    
    UIImage *image;
    if ([text isEqualToString:@"Vodafone"]) {
        image = [UIImage imageNamed:@"vdf_logo"];
        layer.bounds = CGRectMake(0, 0, 30, 30);
    } else {
        image = [UIImage imageNamed:@"business"];
    }
    
//    layer.frame = CGRectMake(point.x, point.y, 15, 15);
    layer.contents = (__bridge id _Nullable)(image.CGImage);
    
    imgLayer = layer;
    
    [self.layer addSublayer:layer];
    
    
    // Add text layer to show message.
    CATextLayer *textlayer = [CATextLayer layer];
    textlayer.string = text;
    textlayer.contentsScale = [UIScreen mainScreen].scale;
    textlayer.fontSize = 9;
    textlayer.foregroundColor = [UIColor lightGrayColor].CGColor;
    textlayer.font = CFBridgingRetain([UIFont systemFontOfSize:10].fontName);
    textlayer.foregroundColor = [UIColor blackColor].CGColor;
    textlayer.alignmentMode = @"center";
    CGRect stringRect = [text boundingRectWithSize:CGSizeMake(200, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:10]} context:nil];
    textlayer.anchorPoint = CGPointMake(0, 0.5);
    textlayer.position = point;
    textlayer.bounds = CGRectMake(0, 0, stringRect.size.width, stringRect.size.height);
    textlayer.transform = CATransform3DMakeScale(1 / self.scaleStore.scale, 1 / self.scaleStore.scale, 0);
    
    stringLayer = textlayer;
    
   [self.layer addSublayer:textlayer];
    
}

/**
 *  Add locate layer.
 */
- (void)addLocation {
    
  //  NSLog(@"newHeading.magneticHeading=%f",self.locationManager.   newHeading.magneticHeading);
   
    // Draw location point
    UIImage *locateImage = [UIImage imageNamed:@"daohang"];
    
    if (self.shouldLocation || self.locationModel)
    {
        CGPoint location;
//        CGPoint simulatePoint;
        // Path filter
        
        double final_x = self.locationModel.x/10;
        double final_y = self.locationModel.y/10;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"generalSetting10006"])
        {
            //惯导
            NSLock *lock = [[NSLock alloc] init];
            [lock lock];
            SVANavAngleAndSpeed *nav = [SVANavAngleAndSpeed defaultNav];
            [lock unlock];
            
//            if (currentHeading > 360-self.mapDataModel.angle.doubleValue) {
//                currentHeading = currentHeading-(360-self.mapDataModel.angle.doubleValue);
//            }
            
            int newlocation[3]={0,0,0};
           
            [MyLocator paticleFilterwithFloat:self.locationModel.x/10 withFloat:self.locationModel.y/10 withInt:self.mapDataModel.floor withFloat:((currentHeading + self.mapDataModel.angle.doubleValue)/180*M_PI)  withFloat:nav->nav_velocity WithResultSavedInArray:newlocation WithElemCount:3];
            
            final_x = newlocation[0];
            final_y = newlocation[1];
        }

        location = [SVACoordinateConversion getPointWithXspot:final_x Yspot:final_y onMapMode:self.mapDataModel];

        // Add halo circle
//        CALayer *haloLayer = [CALayer layer];
//        haloLayer.position = location;
//        haloLayer.bounds = CGRectMake(0, 0, 50, 50);
//        haloLayer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
//        haloLayer.backgroundColor = [UIColor colorWithRed:0.000 green:0.000 blue:1.000 alpha:0.1].CGColor;
//        haloLayer.cornerRadius = 25;
//        [self.layer addSublayer:haloLayer];
    
        // Add sublayer to show push message.
        self.locateLayer.position = location;
        self.locateLayer.bounds = CGRectMake(0, 0, 25, 25);
        //定位点缩放
        NSLog(@"storeScale:%f",self.scaleStore.scale);
        self.locateLayer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
        self.locateLayer.contents = (__bridge id _Nullable)(locateImage.CGImage);
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"generalSetting10005"]) {
            // Add histor location information.
            if (![self.historyLocation containsObject:[NSValue valueWithCGPoint:location]]) {
                [self.historyLocation addObject:[NSValue valueWithCGPoint:location]];
            }
        }
    }
}

/**
 *
 *  Draw histor location points.
 */
- (void)addHistoryLayer {
    
    // Finish this if the swith is turning off.
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"generalSetting10005"]) {
        [self.historyLocation removeAllObjects];
        return;
    }
    
    UIImage *history = [UIImage imageNamed:@"icon_zj_center"];
    
    NSLog(@"history point count = %@", @(self.historyLocation.count));
    if (self.historyLocation.count != 0) {
        for (NSValue *value in self.historyLocation) {
            CGPoint point = value.CGPointValue;
            
            CALayer *layer = [CALayer layer];
            layer.position = point;
            layer.bounds = CGRectMake(0, 0, 5, 5);
            //            layer.frame = CGRectMake(point.x + 12, point.y + 12, 5 / self.scale, 5 / self.scale);
            layer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
            layer.contents = (__bridge id _Nullable)(history.CGImage);
            [self.layer addSublayer:layer];
        }
    }
}

- (void)fetchSensorDataHandler:(void(^)(NSMutableArray *data))dataHandler {
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        CMMotionManager *motionManager = [[CMMotionManager alloc] init];
        
        if (motionManager.accelerometerAvailable) {
            motionManager.accelerometerUpdateInterval = 1.0 / 50;
            [motionManager startAccelerometerUpdates];
        } else {
          //  NSLog(@"Accelerometer is not available.");
        }
        if (motionManager.gyroAvailable) {
            motionManager.gyroUpdateInterval = 1.0 / 50;
            [motionManager startGyroUpdates];
        } else {
         //   NSLog(@"Gyro is not available.");
        }
        if (motionManager.magnetometerAvailable) {
            motionManager.magnetometerUpdateInterval = 1.0 / 50;
            [motionManager startMagnetometerUpdates];
        } else {
         //   NSLog(@"Magnetometer is not available.");
        }
        
        NSMutableArray *accelerationX = [NSMutableArray array];
        NSMutableArray *accelerationY = [NSMutableArray array];
        NSMutableArray *accelerationZ = [NSMutableArray array];
        NSMutableArray *GyroX = [NSMutableArray array];
        NSMutableArray *GyroY = [NSMutableArray array];
        NSMutableArray *GyroZ = [NSMutableArray array];
        NSMutableArray *MagnetometerX = [NSMutableArray array];
        NSMutableArray *MagnetometerY = [NSMutableArray array];
        NSMutableArray *MagnetometerZ = [NSMutableArray array];
        NSMutableArray *Pressure = [NSMutableArray array];
        
        for (NSInteger i = 0; i < 50; i++) {
            [accelerationX addObject:[NSNumber numberWithDouble:motionManager.accelerometerData.acceleration.x]];
            [accelerationY addObject:[NSNumber numberWithDouble:motionManager.accelerometerData.acceleration.y]];
            [accelerationZ addObject:[NSNumber numberWithDouble:motionManager.accelerometerData.acceleration.z]];
            [GyroX addObject:[NSNumber numberWithDouble:motionManager.gyroData.rotationRate.x]];
            [GyroY addObject:[NSNumber numberWithDouble:motionManager.gyroData.rotationRate.y]];
            [GyroZ addObject:[NSNumber numberWithDouble:motionManager.gyroData.rotationRate.z]];
            [MagnetometerX addObject:[NSNumber numberWithDouble:motionManager.magnetometerData.magneticField.x]];
            [MagnetometerY addObject:[NSNumber numberWithDouble:motionManager.magnetometerData.magneticField.y]];
            [MagnetometerZ addObject:[NSNumber numberWithDouble:motionManager.magnetometerData.magneticField.z]];
            [Pressure addObject:[NSNumber numberWithDouble:.0]];
            [NSThread sleepForTimeInterval:0.02];
        }
        
        // result
        _result = [NSMutableArray arrayWithObjects:accelerationX, accelerationY, accelerationZ, GyroX, GyroY, GyroZ, MagnetometerX, MagnetometerY, MagnetometerZ, Pressure, nil];
        if (_result && dataHandler) {
            dataHandler(_result);
        }
    });

}

- (void)rotateTextWithRotation:(CGFloat)rotation {

    for (CALayer *sublayer in self.layer.sublayers) {

        if ([sublayer isKindOfClass:CATextLayer.class]) {
            sublayer.anchorPoint = CGPointMake(0, 0);
//          sublayer.transform = //CATransform3DRotate(sublayer.transform, -rotation, 0, 0, 1);
            sublayer.transform = CATransform3DMakeScale(1 / self.scale, 1 / self.scale, 1 / self.scale);
        
        }
    }
    
    
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    // Initial config.
    self.shouldShowSellerInfo = NO;
    [self removePopView];
    
    //[pushModels removeAllObjects];
    //[pushPointArr removeAllObjects];
    
    

    
    
    // Current touch point.
    UITouch *touch = [touches anyObject];
    self.touchPoint = [touch locationInView:self];
    
    for (NSValue *value in self.sellerPoints) {
        CGPoint point = value.CGPointValue;
        CGRect rect = CGRectMake(point.x - 10, point.y - 10, 20, 20);
        if (CGRectContainsPoint(rect, self.touchPoint)) {
            self.touchPoint = point;
            [self shouldShowPopview:POISeller rect:rect value:value];
//            self.identifier = [CALayer layer];
            hasIdentifier = YES;

            self.identifier.bounds = CGRectMake(0, 0, 30, 30);
            self.identifier.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
            
            self.identifier.anchorPoint = CGPointMake(0.5, 1.0);
            self.identifier.position = point;
            self.identifier.contents = (__bridge id _Nullable)([UIImage imageNamed:@"start"].CGImage);
            if (![self.layer.sublayers containsObject:self.identifier]) {
                [self.layer addSublayer:self.identifier];
            }
            break;
        }
    }
    for (NSValue *value in self.pushPoints) {
        CGPoint point = value.CGPointValue;
        CGRect rect = CGRectMake(point.x - 10, point.y - 10, 20, 20);
        if (CGRectContainsPoint(rect, self.touchPoint)) {
            self.touchPoint = point;

            SVALocationMessageModel *model = (SVALocationMessageModel *)[self.messageModels objectAtIndex:[self.pushPoints indexOfObject:value]];
            self.picturePath = model.pictruePath;
          // [self shouldShowPopview:POIPush rect:rect value:value];
            clickShopMessage = [NSString stringWithFormat:@"%@",model.message];
            
           // pushModels = self.messageModels;
           // pushPointArr = self.pushPoints;
            
            //推送窗口
            [self addPushView];
            
//            self.identifier = [CALayer layer];
            hasIdentifier = YES;
            self.identifier.bounds = CGRectMake(0, 0, 24, 30);
            self.identifier.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
            self.identifier.position = point;
            self.identifier.contents = (__bridge id _Nullable)([UIImage imageNamed:@"end"].CGImage);
            if (![self.layer.sublayers containsObject:self.identifier]) {
                [self.layer addSublayer:self.identifier];
            }
            
            break;
        }
    }
}

- (void)shouldShowPopview:(POIType)type rect:(CGRect)rect value:(NSValue *)value {
    if (type == POISeller) {
        SVAPOIModel *model = (SVAPOIModel *)[self.poiModels objectAtIndex:[self.sellerPoints indexOfObject:value]];
        self.currentMessage = model.info;
        self.picturePath = model.pictruePath;
        
        // 转换点击的商家点为屏幕坐标点
        self.currentPoint = [SVACoordinateConversion getPointWithXspot:model.xSpot Yspot:model.ySpot onMapMode:self.mapDataModel];
        // 获取实际的地图上的点
        self.realCurrentPoint = CGPointMake(model.xSpot * self.mapDataModel.scale.doubleValue, self.mapDataModel.imgHeight - model.ySpot * self.mapDataModel.scale.doubleValue);
    } else {
        SVALocationMessageModel *model = (SVALocationMessageModel *)[self.messageModels objectAtIndex:[self.pushPoints indexOfObject:value]];
        self.currentMessage = model.message;
        self.picturePath = model.pictruePath;
    }
    self.shouldShowSellerInfo = YES;
//    self.poiType = type;
    [self addPopViewWithMessage:self.currentMessage logoPath:self.picturePath mapPoint:self.touchPoint];
    
}

- (void)addPopViewWithMessage:(NSString *)message logoPath:(NSString *)logoPath mapPoint:(CGPoint)mapPoint {
//    return;
//    self.currentPoint = mapPoint;
    
    popupView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, SCREEN_SIZE.height)];
        view.backgroundColor = [UIColor colorWithRed:49/255.0 green:133/255.0 blue:255/255.0 alpha:1.0];
        view.tag = 1111;
        
        // Add pan gesture.
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragView:)];
        panGesture.delegate = self;
        [view addGestureRecognizer:panGesture];
        
        // Add swipe gesture to show detail information.
        UISwipeGestureRecognizer *swipeShowGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToShowView:)];
        swipeShowGesture.delegate = self;
        swipeShowGesture.direction = UISwipeGestureRecognizerDirectionUp;
        [view addGestureRecognizer:swipeShowGesture];
        
        UISwipeGestureRecognizer *swipeCloseGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeToCloseView:)];
        swipeCloseGesture.delegate = self;
        swipeCloseGesture.direction = UISwipeGestureRecognizerDirectionDown;
        [view addGestureRecognizer:swipeCloseGesture];
        
        // Add this to
//        [panGesture requireGestureRecognizerToFail:swipeGesture];
        
        view;
    });
    
    UIImageView *logo = ({
        UIImageView *imageview = [UIImageView new];
        [[SVANetworkResource sharedResource] loadLogo:imageview WithPath:logoPath];
        imageview.contentMode = UIViewContentModeScaleAspectFit;
        imageview;
    });
    [popupView addSubview:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(SCREEN_SIZE.height / 6);
        make.height.mas_equalTo(SCREEN_SIZE.height / 6);
        make.left.mas_equalTo(popupView);
        make.top.mas_equalTo(popupView);
    }];
    
    UITextView *messageLabel = ({
        UITextView *label = [UITextView new];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont systemFontOfSize:14];
        label.editable = NO;
        label.text = message;
        label;
    });
    [popupView addSubview:messageLabel];
//    CGRect stringRect = [message boundingRectWithSize:CGSizeMake(SCREEN_SIZE.width * 5/6, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil];
    [messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(logo.mas_right);
        make.right.mas_equalTo(popupView);
        make.height.mas_equalTo(60);
        make.top.mas_equalTo(popupView).offset(0);
    }];
    
    //
    CGFloat buttonWidth = (SCREEN_SIZE.width - SCREEN_SIZE.height / 6 - 45) / 2.0;
    
    deleteBT =[[UIButton   alloc]init];
    [deleteBT addTarget:self action:@selector(deleteWay) forControlEvents:UIControlEventTouchUpInside];
    [deleteBT setImage:[UIImage imageNamed:@"deletepath.png"] forState:UIControlStateNormal];
    
    [popupView addSubview:deleteBT];
    
    [deleteBT   mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(logo.mas_bottom).with.offset(-10);
        make.right.equalTo(popupView).offset(-5);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(30);
    }];
    
    
    [popupView addSubview:deleteBT];
    UIView * showView = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_SIZE.height/6, SCREEN_SIZE.width, SCREEN_SIZE.height/3)];
    showView.backgroundColor = [UIColor whiteColor];
    [popupView addSubview:showView];
    
    
    UIImageView * showPicture = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.height/4, SCREEN_SIZE.height/4)];
    showPicture.contentMode = UIViewContentModeScaleAspectFill;
    showPicture.backgroundColor = [UIColor colorWithRed:34/255.0 green:104/255.0 blue:255/255.0 alpha:1];
    
    [[SVANetworkResource sharedResource] loadLogo:showPicture WithPath:logoPath];
    showPicture.center = showView.center;
    [popupView addSubview:showPicture];
    
    UIButton *from = ({
        UIButton *button = [UIButton new];
        //        button.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitle:CustomLocalizedString(@"起点",nil) forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"start_flag"] forState:UIControlStateNormal];
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, buttonWidth - 20);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -40, 0, 0);
        [button addTarget:self action:@selector(setStartLocation:) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    [popupView addSubview:from];
    [from mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(logo.mas_right).offset(5);
        make.bottom.mas_equalTo(logo.mas_bottom).with.offset(-10);
        make.width.mas_equalTo(buttonWidth);
        make.height.mas_equalTo(30);
    }];
    
    UIButton *to = ({
        UIButton *button = [UIButton new];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitle:CustomLocalizedString(@"终点",nil) forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"end_flag"] forState:UIControlStateNormal];
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, buttonWidth - 20);
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -40, 0, 0);
        [button addTarget:self action:@selector(setEndLocation:) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    [popupView addSubview:to];
    [to mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(deleteBT.mas_left).with.offset(-5);
        make.bottom.mas_equalTo(logo.mas_bottom).with.offset(-10);
        make.width.mas_equalTo(buttonWidth);
        make.height.mas_equalTo(30);
    }];
    
    
    [KEY_WINDOW addSubview:popupView];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
         usingSpringWithDamping:0.4
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         popupView.transform = CGAffineTransformMakeTranslation(0, -SCREEN_SIZE.height / 6);
                     } completion:^(BOOL finished) {
                         
                     }];
}
-(void)deleteWay
{
    [self.startLayer removeFromSuperlayer];
    [self.endLayer removeFromSuperlayer];
    [self.pathLayer removeFromSuperlayer];
   
    self.startLayer = nil;
    self.endLayer = nil;
    self.pathLayer = nil;
    
    hasStart = NO;
    hasEnd = NO;

   
}
- (void)setStartLocation:(UIButton *)button {
    [self addFlag:@"start_eng"];
}

- (void)setEndLocation:(UIButton *)button {
    [self addFlag:@"end_eng"];
   
}

- (void)addFlag:(NSString *)flag {
    
    if ([flag isEqualToString:@"start_eng"]) {
        if (!self.startLayer) {
            self.startLayer = [CALayer layer];
        }
        hasStart = YES;
        self.startLayer.anchorPoint = CGPointMake(0.5, 1.0);
        self.startLayer.position = self.touchPoint;
        
        self.startLayer.bounds = CGRectMake(0, 0, 25, 34 );
        self.startLayer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
//        self.startLayer.frame = CGRectMake(self.touchPoint.x - 20 / self.scale, self.touchPoint.y - 34 / self.scale, 25 / self.scale, 34 / self.scale);
        self.startLayer.contents = (__bridge id _Nullable)([UIImage imageNamed:flag].CGImage);
        
        self.startPoint = self.currentPoint;
        self.realStartPoint = self.realCurrentPoint;
        
        if (self.endLayer && CGPointEqualToPoint(self.startPoint, self.endPoint)) {
            [self.endLayer removeFromSuperlayer];
            hasEnd = NO;
        }
        [self.layer addSublayer:self.startLayer];
        
    } else {
        if (!self.endLayer) {
            self.endLayer = [CALayer layer];
        }
        hasEnd = YES;
        self.endLayer.anchorPoint = CGPointMake(0.5, 1.0);
        self.endLayer.position = self.touchPoint;
        self.endLayer.bounds = CGRectMake(0, 0, 25 , 34 );
        self.endLayer.transform = CATransform3DMakeScale(1/self.scaleStore.scale, 1/self.scaleStore.scale, 0);
//        self.endLayer.frame = CGRectMake(self.touchPoint.x - 20 / self.scale, self.touchPoint.y - 34 / self.scale, 25 / self.scale, 34 / self.scale);
        self.endLayer.contents = (__bridge id _Nullable)([UIImage imageNamed:flag].CGImage);
        self.endPoint = self.currentPoint;
        self.realEndPoint = self.realCurrentPoint;
        if (self.startLayer && CGPointEqualToPoint(self.startPoint, self.endPoint)) {
            [self.startLayer removeFromSuperlayer];
            hasStart = NO;
        }
        
        [self.layer addSublayer:self.endLayer];
    }
    
    if (hasStart && hasEnd) {
        if (!CGPointEqualToPoint(self.startPoint, self.endPoint)) {
            [self drawPathWithStart:self.realStartPoint endPoint:self.realEndPoint];
        }
    } else {
        [self.pathLayer removeFromSuperlayer];
    }
}

- (void)drawPathWithStart:(CGPoint)start endPoint:(CGPoint)end {
    
    NSLock *lock = [[NSLock alloc] init];
    [lock lock];
    NSArray *sublayers = [NSArray arrayWithArray:self.layer.sublayers];
    for (CALayer *sublayer in sublayers) {
        if ([sublayer isKindOfClass:[CAShapeLayer class]]) {
            [sublayer removeFromSuperlayer];
        }
    }
    [lock unlock];
    
//    FindPath *new = [[FindPath alloc] init];
    
    if (![self.mapDataModel.keyWord isEqualToString:@"huawei"] && ![self.mapDataModel.keyWord isEqualToString:@"vdf"]) {
        return;
    }
    
    NSArray *array = [SVAFindPath findPathWithStart:start end:end onModel:self.mapDataModel];
    
//    NSMutableArray *array = [new findPathStartX:start.x
//                                          statY:start.y
//                                           endX:end.x
//                                           endY:end.y
//                                       filePath:self.mapDataModel.keyWord];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:self.startPoint];
    
    for (NSValue *pointValue in array) {
        CGPoint pathPoint = [SVACoordinateConversion getPathPointWithXspot:pointValue.CGPointValue.x / self.mapDataModel.scale.doubleValue Yspot:pointValue.CGPointValue.y / self.mapDataModel.scale.doubleValue onMapMode:self.mapDataModel];
        [path addLineToPoint:pathPoint];
    }
    [path addLineToPoint:self.endPoint];
    
    self.pathLayer = [CAShapeLayer layer];
    self.pathLayer.path = path.CGPath;
    self.pathLayer.strokeColor = [UIColor greenColor].CGColor;
    self.pathLayer.fillColor = [UIColor clearColor].CGColor;
    [self.layer addSublayer:self.pathLayer];
}

- (void)removePopView {
    [self.identifier removeFromSuperlayer];
    hasIdentifier = NO;
    UIView *popview = [KEY_WINDOW viewWithTag:1111];
    [UIView animateWithDuration:0.5
                          delay:0.0
         usingSpringWithDamping:0.4
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         popview.transform = CGAffineTransformMakeTranslation(0, 0);
                         
                     } completion:^(BOOL finished) {
                         [popview removeFromSuperview];
                     }];
    //退出push窗口
    [UIView animateWithDuration:0.5
                          delay:0.0
         usingSpringWithDamping:0.4
          initialSpringVelocity:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         popupView.transform = CGAffineTransformMakeTranslation(0,0);
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void)dragView:(UIPanGestureRecognizer *)pan {
//    CGPoint offset = [pan translationInView:self];
//    if (pan.view) {
//        CGRect original = pan.view.bounds;
//        pan.view.transform = CGAffineTransformMakeTranslation(0, offset.y - original.size.height / 2);
//    }
}

- (void)swipeToShowView:(UISwipeGestureRecognizer *)show {
    
    [UIView animateWithDuration:.5
                          delay:.0
         usingSpringWithDamping:.4
          initialSpringVelocity:.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         show.view.transform = CGAffineTransformMakeTranslation(0, -SCREEN_SIZE.height / 2);
                     
                     }completion:^(BOOL finished) {
                     }];
}

- (void)swipeToCloseView:(UISwipeGestureRecognizer *)close {
    [UIView animateWithDuration:.5
                          delay:.0
         usingSpringWithDamping:.4
          initialSpringVelocity:.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         close.view.transform = CGAffineTransformMakeTranslation(0, 0);}
                     completion:^(BOOL finished) {
                         [close.view removeFromSuperview];
                         [self.identifier removeFromSuperlayer];
                         hasIdentifier = NO;
                     }];
    
    [pushModels removeAllObjects];
    
    [pushPointArr removeAllObjects];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

@end
