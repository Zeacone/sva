//
//  SVAInteractivePOI.h
//  SVA
//
//  Created by 一样 on 16/2/24.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SVAInteractivePOI : UIView

- (void)configStartPoint;

- (void)configDestination;

@end
