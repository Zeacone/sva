//
//  SVAPathFilter.h
//  SVA
//
//  Created by Zeacone on 16/1/25.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVAPathFilter : NSObject

@property (nonatomic, strong) NSMutableArray<NSValue *> *fingerPrintXY;

+ (instancetype)sharedPathFilter;
- (CGPoint)calculatePathFilterPoint:(CGPoint)locate;

@end
