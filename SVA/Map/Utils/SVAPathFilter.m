//
//  SVAPathFilter.m
//  SVA
//
//  Created by Zeacone on 16/1/25.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import "SVAPathFilter.h"

@interface SVAPathFilter ()

@property (nonatomic, strong) NSMutableArray<NSValue *> *resultPoints;
@property (nonatomic, strong) NSMutableArray<NSValue *> *globalResultPoints;
@property (nonatomic, assign) NSInteger                 distanceCount;
@property (nonatomic, assign) NSInteger                 filter2Flag;

@end

@implementation SVAPathFilter

+ (instancetype)sharedPathFilter {
    static SVAPathFilter *filter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        filter = [SVAPathFilter new];
    });
    return filter;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.distanceCount = 0;
        self.filter2Flag = 0;
    }
    return self;
}

- (NSMutableArray<NSValue *> *)resultPoints {
    if (!_resultPoints) {
        _resultPoints = [NSMutableArray array];
    }
    return _resultPoints;
}

- (NSMutableArray<NSValue *> *)globalResultPoints {
    if (!_globalResultPoints) {
        _globalResultPoints = [NSMutableArray array];
    }
    return _globalResultPoints;
}

- (NSMutableArray<NSValue *> *)fingerPrintXY {
    if (!_fingerPrintXY) {
        _fingerPrintXY = [NSMutableArray array];
    }
    return _fingerPrintXY;
}

- (CGPoint)calculatePathFilterPoint:(CGPoint)locate {
    NSValue *locateValue = [NSValue valueWithCGPoint:locate];
    
    CGPoint outval, outval_global;
    
    // 滤波窗长
    NSInteger filterWindowLength = 2;
    // 局部搜索算法距离门限，单位分米
    CGFloat dis_th = 50.0;
    // 局部搜索算法次数门限
    NSInteger count_th = 3;
    
    // 全局搜索
    [self.globalResultPoints addObject:locateValue];
    if (self.globalResultPoints.count > filterWindowLength) {
        [self.globalResultPoints removeObjectAtIndex:0];
    } else if (self.globalResultPoints.count < filterWindowLength) {
        return locate;
    }
    
    CGFloat sumX = 0;
    CGFloat sumY = 0;
    for (NSInteger i = 0; i < filterWindowLength; i++) {
        sumX += [self.globalResultPoints objectAtIndex:i].CGPointValue.x;
        sumY += [self.globalResultPoints objectAtIndex:i].CGPointValue.y;
    }
    outval_global = CGPointMake(sumX / filterWindowLength, sumY / filterWindowLength);
    
    // 局部搜索
    [self.resultPoints addObject:locateValue];
    if (self.resultPoints.count > filterWindowLength) {
        [self.resultPoints removeObjectAtIndex:0];
    } else if (self.resultPoints.count < filterWindowLength) {
        return locate;
    }
    sumX = 0;
    sumY = 0;
    for (NSInteger i = 0; i < filterWindowLength; i++) {
        sumX += [self.resultPoints objectAtIndex:i].CGPointValue.x;
        sumY += [self.resultPoints objectAtIndex:i].CGPointValue.y;
    }
    outval = CGPointMake(sumX / filterWindowLength, sumY / filterWindowLength);
    
    outval = [self pathScope:outval filterWindowLength:filterWindowLength];
    outval_global = [self pathAdapt:outval_global filterWindowLength:filterWindowLength];
    
    // 替换最后一个点
    NSValue *outval_global_Value = [NSValue valueWithCGPoint:outval_global];
    [self.globalResultPoints replaceObjectAtIndex:filterWindowLength - 1 withObject:outval_global_Value];
    
    CGFloat dis = sqrt(pow(outval.x - outval_global.x, 2) + pow(outval.y - outval_global.y, 2));
    if (dis > dis_th) {
        self.distanceCount++;
    } else {
        self.distanceCount = 0;
    }
    
    if (self.distanceCount >= count_th) {
        // 清掉历史值
        outval = outval_global;
    }
    
    if (self.filter2Flag == 1) {
        sumX = 0;
        sumY = 0;
        for (NSInteger i = 0; i < filterWindowLength; i++) {
            sumX += [self.resultPoints objectAtIndex:i].CGPointValue.x;
            sumY += [self.resultPoints objectAtIndex:i].CGPointValue.y;
        }
        outval = CGPointMake(sumX / filterWindowLength, sumY / filterWindowLength);
        NSValue *outvalValue = [NSValue valueWithCGPoint:outval];
        [self.resultPoints replaceObjectAtIndex:filterWindowLength - 1 withObject:outvalValue];
    }
    
    NSValue *outvalValue = [NSValue valueWithCGPoint:outval];
    
    [self.resultPoints replaceObjectAtIndex:filterWindowLength - 1 withObject:outvalValue];
    return outval;
}

- (CGPoint)pathScope:(CGPoint)scope filterWindowLength:(NSInteger)length {
  //  NSLog(@"局部搜索");
    // 单位是分米
    CGFloat scope_th = 90;
    if (self.fingerPrintXY.count == 0) {
        return scope;
    }
    
    CGPoint lastPoint = [self.resultPoints objectAtIndex:length - 2].CGPointValue;
    NSInteger dbNum = self.fingerPrintXY.count;
    NSMutableArray<NSValue *> *distance = [NSMutableArray array];
    CGPoint dbData;
    // 查找中心点9米范围内的所有滤波点
    for (NSInteger i = 0; i < dbNum; i++) {
        dbData = [self.fingerPrintXY objectAtIndex:i].CGPointValue;
        CGFloat distanceNum = sqrt(pow(lastPoint.x - dbData.x, 2) + pow(lastPoint.y - dbData.y, 2));
        if (distanceNum <= scope_th) {
            [distance addObject:[NSValue valueWithCGPoint:dbData]];
        }
    }
    // 如果9米范围内不存在滤波点则返回中心点
    NSInteger scopeNum = distance.count;
    if (scopeNum == 0) {
        return scope;
    }
    
    // 9米范围内存在滤波点，计算中心点到所有滤波点的距离
    NSMutableArray<NSNumber *> *distanceScope = [NSMutableArray array];
    NSMutableArray<NSNumber *> *index = [NSMutableArray array];
    for (NSInteger i = 0; i < scopeNum; i++) {
        dbData = [distance objectAtIndex:i].CGPointValue;
        [distanceScope addObject:[NSNumber numberWithDouble:sqrt(pow(scope.x - dbData.x, 2) + pow(scope.y - dbData.y, 2))]];
        [index addObject:[NSNumber numberWithInteger:i]];
    }
    
    // 按照中心点到滤波点的距离远近降序排列（远->近）
    for (NSInteger i = 0; i < distanceScope.count; i++) {
        for (NSInteger j = i + 1; j < distanceScope.count; j++) {
            if (distanceScope[i] > distanceScope[j]) {
                CGFloat temp = 0;
                temp = distanceScope[i].doubleValue;
                distanceScope[i] = distanceScope[j];
                distanceScope[j] = [NSNumber numberWithDouble:temp];
                NSInteger tempIndex = 0;
                tempIndex = index[i].integerValue;
                index[i] = index[j];
                index[j] = [NSNumber numberWithInteger:tempIndex];
            }
        }
    }
    
    // 如果中心点周围滤波点个数小于10，则返回中心点
    NSInteger s_data_num = 10;
    if (distance.count < s_data_num) {
        return scope;
    }
    
    // 计算前一次定位点与周围滤波点的距离
    NSMutableArray<NSNumber *> *preDataDis = [NSMutableArray array];
    NSMutableArray<NSNumber *> *preDataIndex = [NSMutableArray array];
    CGPoint preData = self.resultPoints[length - 2].CGPointValue;
    
    for (NSInteger i = 0; i < s_data_num; i++) {
        dbData = [distance objectAtIndex:index[i].integerValue].CGPointValue;
        [preDataDis addObject:[NSNumber numberWithDouble:sqrt(pow(preData.x - dbData.x, 2) + pow(preData.y - dbData.y, 2))]];
        [preDataIndex addObject:[NSNumber numberWithInteger:index[i].integerValue]];
    }
    
    // 按照上一次定位点到滤波点的距离远近降序排列（远->近）
    for (NSInteger i = 0; i < preDataDis.count - 1; i++) {
        for (NSInteger j= i + 1; j < preDataDis.count; j++) {
            if (preDataDis[i].doubleValue > preDataDis[j].doubleValue) {
                CGFloat temp = 0;
                temp = preDataDis[i].doubleValue;
                preDataDis[i] = preDataDis[j];
                preDataDis[j] = [NSNumber numberWithDouble:temp];
                NSInteger tempIndex = 0;
                tempIndex = preDataIndex[i].integerValue;
                preDataIndex[i] = preDataIndex[j];
                preDataIndex[j] = [NSNumber numberWithInteger:tempIndex];
            }
        }
    }
    CGPoint outval = distance[index[0].integerValue].CGPointValue;
    return outval;
}

- (CGPoint)pathAdapt:(CGPoint)adapt filterWindowLength:(NSInteger)length {
   // NSLog(@"全局搜索");
    NSInteger dbNum = self.fingerPrintXY.count;
    NSMutableArray<NSNumber *> *distance = [NSMutableArray array];
    NSMutableArray<NSNumber *> *index = [NSMutableArray array];
    CGPoint dbData;
    for (NSInteger i = 0; i < dbNum; i++) {
        dbData = self.fingerPrintXY[i].CGPointValue;
        [distance addObject:[NSNumber numberWithDouble:sqrt(pow(adapt.x - dbData.x, 2) + pow(adapt.y - dbData.y, 2))]];
        [index addObject:[NSNumber numberWithInteger:i]];
    }
    
    // 先排序
    for (NSInteger i = 0; i < distance.count - 1; i++) {
        for (NSInteger j = i + 1; j < distance.count; j++) {
            if (distance[i].doubleValue > distance[j].doubleValue) {
                CGFloat temp = 0;
                temp = distance[i].doubleValue;
                distance[i] = distance[j];
                distance[j] = [NSNumber numberWithDouble:temp];
                
                NSInteger tempIndex;
                tempIndex = index[i].integerValue;
                index[i] = index[j];
                index[j] = [NSNumber numberWithInteger:tempIndex];
            }
        }
    }
    
    NSInteger s_data_num = 15;
    NSMutableArray<NSNumber *> *preDataDis = [NSMutableArray array];
    NSMutableArray<NSNumber *> *preDataIndex = [NSMutableArray array];
    CGPoint preData = self.resultPoints[length - 2].CGPointValue;

    for (NSInteger i = 0; i < s_data_num; i++) {
        dbData = self.fingerPrintXY[index[i].integerValue].CGPointValue;
        [preDataDis addObject:[NSNumber numberWithDouble:sqrt(pow(preData.x - dbData.x, 2) + pow(preData.y - dbData.y, 2))]];
//        preDataIndex[i] = index[i];
        [preDataIndex addObject:index[i]];
    }
    
    // 先排序
    for (NSInteger i = 0; i < preDataDis.count - 1; i++) {
        for (NSInteger j = i + 1; j < preDataDis.count; j++) {
            if (preDataDis[i].doubleValue > preDataDis[j].doubleValue) {
                CGFloat temp = 0;
                
                temp = preDataDis[i].doubleValue;
                preDataDis[i] = preDataDis[j];
                preDataDis[j] = [NSNumber numberWithDouble:temp];
                
                NSInteger tempIndex;
                tempIndex = preDataIndex[i].integerValue;
                preDataIndex[i] = preDataIndex[j];
                preDataIndex[j] = [NSNumber numberWithInteger:tempIndex];
            }
        }
    }
    
    CGFloat x = (self.fingerPrintXY[index[0].integerValue].CGPointValue.x + self.fingerPrintXY[preDataIndex[0].integerValue].CGPointValue.x) / 2;
    CGFloat y = (self.fingerPrintXY[index[0].integerValue].CGPointValue.y + self.fingerPrintXY[preDataIndex[0].integerValue].CGPointValue.y) / 2;
    
    CGPoint outVal = CGPointMake(x, y);
    return outVal;
}

@end
