//
//  SVALocate.h
//  SVA
//
//  Created by Zeacone on 15/12/21.
//  Copyright © 2015年 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SVALocationModel.h"
#import "svaNavAngleAndSpeed.h"
#import "MyLocator.h"

@interface SVALocationViewModel : SMActivityBaseModel

@property (nonatomic, strong) SVALocationModel        *location;
@property (nonatomic, strong) SVALocationDataModel    *locationData;
@property (nonatomic, strong) SVALocationMessageModel *locationMessage;
@property (nonatomic, strong) NSTimer                 *timer;

/**
 *  Create locating singleton.
 *
 *  @return A singleton of current class.
 */
+ (instancetype)sharedLocateViewModel;

/**
 *  Start locating every 2 seconds on a runloop.
 */
- (void)startLocating;

/**
 *  Invalid source timer to stop locate operation.
 */
- (void)stopLocating;

- (void)startRunloop;

- (void)startTimer;

@end
