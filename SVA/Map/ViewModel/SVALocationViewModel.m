//
//  SVALocate.m
//  SVA
//
//  Created by Zeacone on 15/12/21.
//  Copyright © 2015年 huawei. All rights reserved.
//

#import "SVALocationViewModel.h"
#import "SVAMapView.h"
#import "SVANavAngleAndSpeed.h"

@class SVAMapView;
@class SVAPOI;

#define GET_LOCATION @"/sva/api/getData"

static dispatch_queue_t get_location_queue() {
    static dispatch_queue_t get_location_queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        get_location_queue = dispatch_queue_create("server.connection.queue", DISPATCH_QUEUE_CONCURRENT);
    });
    return get_location_queue;
}

@implementation SVALocationViewModel

+ (instancetype)sharedLocateViewModel {
    static SVALocationViewModel *locateViewModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        locateViewModel = [[SVALocationViewModel alloc] init];
    });
    return locateViewModel;
}

- (void)stopLocating {
    [self.timer invalidate];
    [SVAPOI sharedPOI].isLocate = NO;
    [SVAMapView sharedMap].locate.selected = NO;
    //判断网络请求
    [ScaleStore defaultScale].isHttpRequest = NO;
    
    [MyLocator init_last_loc];
}

- (void)startTimer {
    [self.timer fire];
}

- (void)startLocating {
    /// Test method.
//    [self startRunloop];
//    [[SVANavAngleAndSpeed defaultNav] threadStart];
//    return;
    [ScaleStore defaultScale].isHttpRequest = YES;
    NSDictionary *parameter = @{@"ip": /*@"10.65.98.156"*/(NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"IP"],
                                @"isPush": @"2"};
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST"
                                                                                 URLString:[BASE_IP stringByAppendingString:GET_LOCATION]
                                                                                parameters:parameter
                                                                                     error:nil];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    @weakify(self);
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                  //  NSLog(@"%@ %@", response, responseObject);
                                                    [[SVALogger sharedLogger] appendLog:((NSHTTPURLResponse *)response).description];
                                                    [[SVALogger sharedLogger] appendLog:((NSDictionary *)responseObject).description];
    
        if (responseObject) {
            @strongify(self);
            // 获取定位数据
            NSDictionary *data = [responseObject objectForKey:@"data"];
            if (!data || [data isEqual:[NSNull null]]) return;
            
            SVALocationDataModel *locationModel = [[SVALocationDataModel alloc] initModelWithDictionaryAndExclude:data];
            
            // 加载定位所在的地图
            SVAMapDataModel *mapDataModel = [[SVAMapDataViewModel sharedMapViewModel] getMapDataByFloorNo:locationModel.z];
            
            [[SVAPOI sharedPOI] clearLocation];
            [[SVAPOI sharedPOI] clearAll];
            //初始化各个控件
            ///////////////////////////////////////
            [SVAMapView sharedMap].contentScrollView.contentSize = CGSizeMake(SCREEN_SIZE.width * 1.5, SCREEN_SIZE.height * 1.2);
            [SVAMapView sharedMap].contentScrollView.contentOffset = CGPointMake(0, 0);
            [SVAMapView sharedMap].mapView.center = CGPointMake(SCREEN_SIZE.width / 2, SCREEN_SIZE.height / 2);
            [SVAMapView sharedMap].mapView.transform = CGAffineTransformIdentity;
            [SVAMapView sharedMap].lastScale = 1;
            [ScaleStore defaultScale].scale = 1;
            //////////////////////////////////////////
            
            [[SVAMapView sharedMap].mapView loadMapWithMapModel:mapDataModel];
            
            [[SVAPOIViewModel sharedPOIViewModel] getPOIsWithMapDataModel:mapDataModel];
            [SVAMapView sharedMap].floors = [[SVAMapDataViewModel sharedMapViewModel] getFloorsByPlace:mapDataModel.place];
            [[SVAMapView sharedMap].floorSelection reloadData];
            if ([SVAMapView sharedMap].floors.count != 0) {
                
            }
            
            [[SVAPOI sharedPOI] clearLocation];
            [SVAPOI sharedPOI].isLocate = YES;
            /**
             *  此处写死了，应该根据商场名字和楼层去匹配
             */
            
            for (NSInteger i = 0; i < [SVAMapView sharedMap].floors.count; i++) {
                NSInteger floorNumber = ((SVAMapDataModel *)([SVAMapView sharedMap].floors[i])).floorNo;
                if (mapDataModel.floorNo == floorNumber) {
                    [[SVAMapView sharedMap].floorSelection selectRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
                }
            }
            
            /**
             *  判断定位后地图名字和第几个
             */
            NSInteger select = 0;
            for (int i = 0; i < [SVAPopupView sharedPopup].places.count; i++) {
                if ([mapDataModel.place isEqualToString:[SVAPopupView sharedPopup].places[i]]) {
                    select = i;
                    break;
                }
            }
            
            [[SVAPopupView sharedPopup].storeTableview selectRowAtIndexPath:[NSIndexPath indexPathForItem:select inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
            
            [SVAMapView sharedMap].getMarketTitleHandler(mapDataModel.place);
            
            
            [self startRunloop];
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                [[SVANavAngleAndSpeed defaultNav] threadStart];
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SVAPOI sharedPOI] addAllLayer];
            });
        }
    }];
    [dataTask resume];
}

- (void)startRunloop {
    dispatch_async(get_location_queue(), ^{
        self.timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(getLocation) userInfo:nil repeats:YES];
        NSRunLoop *runloop = [NSRunLoop currentRunLoop];
        [runloop addPort:[NSMachPort port] forMode:NSDefaultRunLoopMode];
        [runloop addTimer:self.timer forMode:NSRunLoopCommonModes];
        [runloop run];
    });
}

- (void)getLocation {
    NSDictionary *parameter = @{@"ip": /*@"10.65.98.156"*/(NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"IP"],
                                @"isPush": @"2"};
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST"
                                                                                 URLString:[BASE_IP stringByAppendingString:GET_LOCATION]
                                                                                parameters:parameter
                                                                                     error:nil];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    @weakify(self);
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                completionHandler:^(NSURLResponse * _Nonnull response,
                                                                    id  _Nullable responseObject,
                                                                    NSError * _Nullable error) {
                                                    [[SVALogger sharedLogger] appendLog:((NSHTTPURLResponse *)response).description];
                                                    [[SVALogger sharedLogger] appendLog:((NSDictionary *)responseObject).description];
                                                    if ([ScaleStore defaultScale].isHttpRequest) {
                                                        
                                                    
        if (responseObject) {
            @strongify(self);
            self.locationData = nil;
            
          //  NSLog(@"%@", responseObject);
            
            self.location = [[SVALocationModel alloc] initModelWithDictionaryAndExclude:responseObject];
        
        //   [[NSUserDefaults standardUserDefaults] setObject:pushM forKey:@"pushMessage"];
            
        
            // 获取定位数据
            NSDictionary *data = [responseObject objectForKey:@"data"];
            
            /// test data.
//            NSDictionary *data = @{@"idType": @"12", @"timestamp": @"3243472161", @"dataType": @"seller", @"x" : @(arc4random() % 200 + 100), @"y": @(arc4random() % 200 + 100), @"z": @10001, @"userID": @"3442", @"path": @"imagePath", @"xo": @"0", @"yo": @"0", @"scale": @"23.4"};
            if (!data || [data isEqual:[NSNull null]]) return;
            self.locationData = [[SVALocationDataModel alloc] initModelWithDictionaryAndExclude:data];
            
            // 加载定位所在的地图
            
            SVAMapDataModel *mapDataModel = [[SVAMapDataViewModel sharedMapViewModel] getMapDataByFloorNo:self.locationData.z];
            
            // 获取推送信息
            // test message data
//            NSDictionary *message1 = @{@"place": @"oceanstore", @"placeId": @"3243472161", @"shopName": @"KFC", @"timeInterval" : @"1", @"xSpot" : @(arc4random() % 20 + 10), @"ySpot": @(arc4random() % 20 + 10), @"floor": @10001, @"rangeSpot": @"34", @"pictruePath": @"imagePath", @"moviePath": @"moviePath", @"id": @"100", @"floorNo": @"10001", @"message": @"it's a message.", @"isEnable": @"1"};
//            NSDictionary *message2 = @{@"place": @"oceanstore", @"placeId": @"3243472161", @"shopName": @"KFC", @"timeInterval" : @"1", @"xSpot" : @(arc4random() % 20 + 10), @"ySpot": @(arc4random() % 20 + 10), @"floor": @10001, @"rangeSpot": @"34", @"pictruePath": @"imagePath", @"moviePath": @"moviePath", @"id": @"200", @"floorNo": @"10001", @"message": @"it's a message.", @"isEnable": @"1"};
//            NSDictionary *message3 = @{@"place": @"oceanstore", @"placeId": @"3243472161", @"shopName": @"KFC", @"timeInterval" : @"1", @"xSpot" : @(arc4random() % 20 + 10), @"ySpot": @(arc4random() % 20 + 10), @"floor": @10001, @"rangeSpot": @"34", @"pictruePath": @"imagePath", @"moviePath": @"moviePath", @"id": @"300", @"floorNo": @"10001", @"message": @"it's a message.", @"isEnable": @"1"};
//            NSArray<NSDictionary *> *message = [NSArray arrayWithObjects:message1, message2, message3, nil];
            NSArray *message = [responseObject objectForKey:@"message"];
            NSMutableArray *tempMessage = [NSMutableArray array];
            for (NSDictionary *dic in message) {
                SVALocationMessageModel *model = [[SVALocationMessageModel alloc] initModelWithDictionaryAndExclude:dic];
                [tempMessage addObject:model];
            }
            [SVAPOI sharedPOI].messageModels = tempMessage;
            [SVAPOI sharedPOI].mapDataModel = mapDataModel;
            [SVAPOI sharedPOI].locationModel = self.locationData;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SVAPOI sharedPOI] addAllLayer];
                [[SVAPOI sharedPOI] addHistoryLayer];
                [[SVAPOI sharedPOI] addLocation];
            });
            
//            CGPoint point = [SVACoordinateConversion getPointWithXspot:self.locationData.x Yspot:self.locationData.y onMapMode:mapDataModel];
//            [SVAPOI sharedPOI].point = point;
//            [SVAMapView sharedMap].locationPoint.center = CGPointMake(point.x / 10, point.y / 10);
        }}
    }];
    [dataTask resume];
    
}

@end
