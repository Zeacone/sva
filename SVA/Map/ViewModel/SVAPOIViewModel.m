//
//  SVAPOIViewModel.m
//  SVA
//
//  Created by Zeacone on 15/12/23.
//  Copyright © 2015年 huawei. All rights reserved.
//

#import "SVAPOIViewModel.h"

#define GET_POI @"/sva/api/getSellerInfo?floorNo="

@implementation SVAPOIViewModel

+ (instancetype)sharedPOIViewModel {
    static SVAPOIViewModel *poiViewModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        poiViewModel = [[SVAPOIViewModel alloc] init];
    });
    return poiViewModel;
}

- (NSMutableArray   *)POIPoints {
    if (!self.POIModels) return nil;
    if (!_POIPoints) {
        _POIPoints = [NSMutableArray array];
    }
    for (NSDictionary *dictionary in self.POIModels) {
        SVAPOIModel *model = [[SVAPOIModel alloc] initModelWithDictionaryAndExclude:dictionary];
        CGPoint point = [SVACoordinateConversion getPointWithXspot:model.xSpot Yspot:model.ySpot onMapMode:self.mapModel];
        NSValue *value = [NSValue valueWithCGPoint:point];
        if (![_POIPoints containsObject:value]) {
            [_POIPoints addObject:value];
        }
    }
    return _POIPoints;
}

- (void)getPOIsWithMapDataModel:(SVAMapDataModel *)model {
    self.mapModel = model;
    NSString *middleAddress = [BASE_IP stringByAppendingString:GET_POI];
    NSDictionary *parameter = @{@"ip": (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"IP"],
                                @"isPush": @"2"};
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST"
                                                                                 URLString:[middleAddress stringByAppendingFormat:@"%@", @(self.mapModel.floorNo)]
                                                                                parameters:parameter
                                                                                     error:nil];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                completionHandler:^(NSURLResponse * _Nonnull response,
                                                                    id  _Nullable responseObject,
                                                                    NSError * _Nullable error) {
        NSArray *poiData = [responseObject objectForKey:@"data"];
        NSMutableArray *tempArray = [NSMutableArray array];
        
        for (NSDictionary *dictionary in poiData) {
            SVAPOIModel *model = [[SVAPOIModel alloc] initModelWithDictionaryAndExclude:dictionary];
            [tempArray addObject:model];
        }
        [SVAPOI sharedPOI].poiModels    = tempArray;
        [SVAPOI sharedPOI].mapDataModel = model;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SVAPOI sharedPOI] addAllLayer];
        });
    }];
    [dataTask resume];
}

@end
