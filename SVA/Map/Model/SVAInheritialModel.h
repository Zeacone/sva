//
//  SVAInheritialModel.h
//  SVA
//
//  Created by XuZongCheng on 16/2/21.
//  Copyright © 2016年 huawei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVAInheritialModel : SMActivityBaseModel

@property (nonatomic, copy) NSString *banThreshold;
@property (nonatomic, assign) NSInteger biasSpeed;
@property (nonatomic, assign) NSInteger errorAngle;
@property (nonatomic, assign) NSInteger exceedMaxDeviation;
@property (nonatomic, assign) NSInteger filterNumber;
@property (nonatomic, assign) NSInteger gaussVariance;
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, assign) NSInteger isEnable;
@property (nonatomic, assign) NSInteger maxDeviation;
@property (nonatomic, assign) NSInteger positioningError;
@property (nonatomic, assign) NSInteger spr;
@property (nonatomic, assign) NSInteger updateTime;
@property (nonatomic, assign) NSInteger weight;

@end
